#实现步骤
## 一、删除目录项
##### 1、思路
(1)在文件所在的目录中擦除该文件的目录项
(2)考虑是否回收目录项所在的那个块
(3)目录inode的i_size减去一个目录项的大小，并同步到磁盘

##### 2、具体操作
参考：dir.c中的dir_entry_delete()
(1)收集目录占用的块到all_blocks
(2)遍历所有块，寻找目录项
(3)开始处理目录项，并考虑是否释放目录所占的块
(4)更新目录inode到磁盘

## 二、回收inode
##### 1、涉及到的资源：
磁盘：
(1) inode_table
(2) inode的i_sectors[0~11]中的直接块和一级间接索引块表i_sectors[12]中的间接块
(3) 一级间接索引块表本身的扇区地址
(4) inode位图

内存：
(5) 内存中的inode自身

##### 2、具体操作
参考：inode.c中的inode_release()

(1)收集inode占用的块到all_blocks
(2)设置空闲块位图，回收直接块和间接块
(3)回收一级间接块表占用的块
(4)回收inode位图中inode自身
(5)将inode_table上的对应inode清空
(6)释放内存中inode自身

## 三、测试代码
参考init/main.c
```c
#include <printk.h>
#include <interrupt.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <bitmap.h>
#include <memory.h>
#include <thread.h>
#include <list.h>
#include <mutex.h>
#include <kfifo.h>
#include <tss.h>
#include <process.h>
#include <syscall.h>
#include <syscall_kernel.h>
#include <printf.h>
#include <bitmap.h>
#include <disk.h>
#include <fs.h>
#include <file.h>

void test_a(void * args);
void test_b(void * args);
void process_entry_a(void);
void process_entry_b(void);

int  main()
{   
   console_init();
   interrupt_init();
   time_init();
   memory_init();
   main_thread_init();
   keyboard_init();
   tss_init();
   syscall_init();
           
   process_start("pro_a",process_entry_a);
   process_start("pro_b",process_entry_b);

   thread_start("thread_a",1,test_a,"arg_a");
   thread_start("thread_b",1,test_b,"arg_b");
   asm volatile("sti");
   disk_init();
   fs_init();

   /***********写测试**************/
   // int32_t fd= sys_open("/file1", O_RDWR);
   // printk("fd=%d \r\n",fd);
   // sys_write(fd,"hello,world\r\n",13);
   // int ret=sys_close(fd);
   // printk("after close fd =%d  ret=%d\r\n",fd,ret);


   /***********读测试**************/
   // int32_t fd= sys_open("/file1", O_RDWR);
   // printk("open file :fd=%d \r\n",fd);

   // char buf[64]={0};
   // int read_bytes=sys_read(fd,buf,20);
   // printk("1 read %d bytes:\r\n %s \r\n",read_bytes,buf);

   // memset(buf,0,sizeof(buf));
   // read_bytes=sys_read(fd,buf,20);
   // printk("2 read %d bytes:\r\n %s \r\n",read_bytes,buf); 

   // printk("Read to end of file ,read again maybe return -1 \r\n");
   // memset(buf,0,sizeof(buf));
   // read_bytes=sys_read(fd,buf,20);
   // printk("3 read %d bytes: %s \r\n",read_bytes,buf); 

   // printk(" close file and reopen ----------\r\n");
   // int ret=sys_close(fd);
   // fd = sys_open("/file1", O_RDWR);

   // printk(" after seek_set ----------\r\n");
   // sys_lseek(fd,0,SEEK_SET);//把读写位置指针重置为文件开头
   // memset(buf,0,sizeof(buf));
   // read_bytes=sys_read(fd,buf,13);
   // printk("4 read %d bytes:\r\n %s \r\n",read_bytes,buf); 

   // printk(" after seek_cur ----------\r\n");
   // sys_lseek(fd,2,SEEK_CUR);//把读写位置指针重置为文件开头
   // memset(buf,0,sizeof(buf));
   // read_bytes=sys_read(fd,buf,13);
   // printk("5 read %d bytes:\r\n %s \r\n",read_bytes,buf); 

   // printk(" after seek_end ----------\r\n");
   // sys_lseek(fd,-4,SEEK_END);//把读写位置指针重置为文件开头
   // memset(buf,0,sizeof(buf));
   // read_bytes=sys_read(fd,buf,13);
   // printk("6 read %d bytes:\r\n %s \r\n",read_bytes,buf); 
   // sys_close(fd);

   /***********删除文件测试**************/
   int ret =sys_unlink("/file1");
   printk("unlink /file1  ret= %d \r\n",ret);

    while(1){
    //    printk("main  ...........\r\n");
       };
    return 0;
}


void test_a(void *args)
{
   //  void* addr1 = kmalloc(256);
   // void* addr2 = kmalloc(255);
   // void* addr3 = kmalloc(254);
   // printk(" thread_a malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   // int cpu_delay = 100000;
   // while(cpu_delay-- > 0);
   // kfree(addr1);
   // kfree(addr2);
   // kfree(addr3);
   while(1){};
}

void test_b(void *args)
{
//   void* addr1 = kmalloc(256);
//    void* addr2 = kmalloc(255);
//    void* addr3 = kmalloc(254);
//    printk(" thread_b malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

//    int cpu_delay = 100000;
//    while(cpu_delay-- > 0);
//    kfree(addr1);
//    kfree(addr2);
//    kfree(addr3);
   while(1){};
}

void process_entry_a(void)
{
void* addr1 = malloc(256);
   void* addr2 = malloc(255);
   void* addr3 = malloc(254);
   printf(" pro_a malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   int cpu_delay = 100000;
   while(cpu_delay-- > 0);
   free(addr1);
   free(addr2);
   free(addr3);
   while(1);
}

void process_entry_b(void)
{
 void* addr1 = malloc(256);
   void* addr2 = malloc(255);
   void* addr3 = malloc(254);
   printf(" pro_b malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   int cpu_delay = 100000;
   while(cpu_delay-- > 0);
   free(addr1);
   free(addr2);
   free(addr3);
   while(1);
}
```

## 四、测试结果

![](picture/删除文件后打印.png)
显示ret为0，说明删除成功

![](picture/删除文件前磁盘信息.png)
删除文件前，空闲块位图为3，说明有两个块被分配了，一个是给根目录的i_sectors[0]，一个是给file1的i_sectors[0];inode位图也为3，说明有两个inode被分配了，一个是给根目录，一个是给file1；inode_table有两个inode;file1的目录项是存在的。

==注意，目录和普通文件都占用一个inode，然后目录存的是目录项，存放在目录inode的i_sectors对应的扇区中；普通文件存的是数据，存放在文件inode的i_sectors对应的扇区中==

![](picture/删除文件后磁盘信息.png)
删除file1后，空闲块位图为1，说明只有一个块被分配，是给根目录的i_sectors[0]的，说明file1的i_sectors[0]所占的块被回收了;inode位图也为1，说明只有一个inode被分配了，是给根目录的，说明file1所占的inode被回收了；inode_table有一个inode;file1的目录项已经不存在了。
