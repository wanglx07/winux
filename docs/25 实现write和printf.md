# 一、实现write
==本质就是通过系统调用，调用内核的printk==

## 1、新增功能号
unistd.h
```c
#ifndef _UNISTD_H_
#define _UNISTD_H_

#define __NR_getpid 0
#define __NR_write 1 

#endif 
```

## 2、内核的系统调用表sys_call_table添加和功能号对应的处理函数
==lib/usr/syscall-kernel.c==
```c
uint32_t write_sys(char * str)
{
    printk("%s",str);
    return strlen(str);
}

void syscall_init()
{
    sys_call_table[__NR_getpid]=getpid_sys;//将处理函数注册进系统调用表
    sys_call_table[__NR_write] =write_sys;
}
```

## 3、 封装系统调用接口给应用层使用
==lib/usr/syscall.c==
```c
uint32_t write(char * str)
{
    return _syscall1(write,str);
}
```

lib/usr/syscall.c 
```c
uint32_t write(char * str);
```
## 4、应用层开始使用系统调用接口

# 二、实现printf
==本质就是通过vsprintf得到格式化字符串，调用write来请求内核帮忙打印==

printf.c
```c
static char buf[1024];

extern int vsprintf (char *buf, const char *fmt, va_list args);


int printf(const char *fmt,...)
{
	va_list args;			// va_list 实际上是一个字符指针类型。
	int i;

	va_start (args, fmt);		// 参数处理开始函数。在（include/stdarg.h）
	vsprintf (buf, fmt, args);	// 使用格式串fmt 将参数列表args 输出到buf 中,返回值i 等于输出字符串的长度

	va_end (args);		// 参数处理结束函数。

    i=write(buf); //调用write系统调用进行打印

	return i;			// 返回字符串长度。
}
```
# 三、测试及结果
```c
int  main()
{   
 ......
}
void process_entry_a(void)
{
    printf("%s =%d \r\n","pro_a_pid",getpid());
    while(1);
}

void process_entry_b(void)
{
    printf("%s =%d \r\n","pro_b_pid",getpid());
    while(1);
}
```
![](picture/printf测试结果.png)




