# 实现步骤
## 一、实现copy_process
参考fork.c的copy_process()

==注意1：系统调用走的是handler.asm中的asm_syscall_handler，其它中断走的是handler_entry_%1==。不要误以为先走handler_entry_%1，intr_stack压入handler_entry_%1中保存的中断上下文，然后在c_handler_table中找到中断处理程序，然后再到asm_syscall_handler，最后到sys_fork()。

==注意2：重点注意==《操作系统真相还原》中build_child_stack()使用的是intr_stack，这是因为它Kernel.s中的syscall_handler刚好模仿了intr_stack的压栈顺序，所以它可以令struct intr_stack* intr_0_stack = (struct intr_stack*)((uint32_t)child_thread + PG_SIZE - sizeof(struct intr_stack));然后退出的时候使用*ret_addr_in_thread_stack = (uint32_t)intr_exit;
而我的handler.asm中的asm_syscall_handler是自己定义的操作顺序，要使用    struct syscall_stack * syscall_stack=(struct syscall_stack *)((uint32_t)child_task + PG_SIZE - sizeof(struct syscall_stack));，退出的时候使用 *eip = syscall_exit；

```c
《操作系统真相还原》fork.c:

static int32_t build_child_stack(struct task_struct* child_thread) {
/* a 使子进程pid返回值为0 */
   /* 获取子进程0级栈栈顶 */
   struct intr_stack* intr_0_stack = (struct intr_stack*)((uint32_t)child_thread + PG_SIZE - sizeof(struct intr_stack));
   /* 修改子进程的返回值为0 */
   intr_0_stack->eax = 0;
    ......
   /* switch_to的返回地址更新为intr_exit,直接从中断返回 */
   *ret_addr_in_thread_stack = (uint32_t)intr_exit;
    ......

}

-------------------------------------
我的操作：

void build_child_stack(struct task_struct * child_task)
{
    //修改子进程的系统调用栈syscall_stack（在thread.h中），使得返回的pid为0。我们在copy_process()中已经拷贝了父进程的pcb，
    //包含了系统调用栈,注意这里使用的不是intr_stack，intr_stack是其它中断使用的
    struct syscall_stack * syscall_stack=(struct syscall_stack *)((uint32_t)child_task + PG_SIZE - sizeof(struct syscall_stack));
    syscall_stack->eax=0;
    ......
    *eip = syscall_exit; //设置返回地址，跳到child_process_entry直接操作中断栈
    ......
}
```

## 二、增加系统调用
参考fork.c的sys_fork()
参考syscall.c和syscall_kernel.c的fork()
sys_fork()和fork()

==本质还是调用copy_process()==

## 三、实现init进程
用户进程，里面实际上就是调用fork()产生一个子进程

## 测试结果
![](picture/fork和init测试结果.png)

## 五、注意
## 1、
为了方便，指针是随时可以转换的，如intr_stack的转换
```c
fork.c中：

int32_t build_child_stack(struct task_struct * child_task)
{
    //修改子进程的中断栈，使得返回的pid为0。我们在copy_process()中已经拷贝了父进程的pcb，包含了中断栈
    struct interrupt_stack * intr_stack=(struct interrupt_stack *)((uint32_t)child_task + PG_SIZE - sizeof(struct interrupt_stack));
    intr_stack->eax=0;

    //到时候子进程是由switch_to()进行调度的，所以要构造返回的环境（注意不是构造thread_stack）。
    //将其构建在紧临intr_stack之下的空间，而且不需要设置参数，因为我们设置它的返回地址是退出中断，不需要参数
    
    uint32_t * eip=(uint32_t *) intr_stack-1;

}
```


