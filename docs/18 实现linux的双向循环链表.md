==《操作系统真相还原》用的是双向链表，我这里用的是双向循环链==
# 一、了解typeof()、offset_of()、container_of()
C语言高级用法---typeof()关键字_sweird的博客-CSDN博客_typeof()  https://blog.csdn.net/rosetta/article/details/90741468

C语言高级用法---typeof( ((type *)0)->member )和offset_of()_sweird的博客-CSDN博客_((type *)0)->member  https://blog.csdn.net/rosetta/article/details/90746936

C语言高级用法---container_of()_sweird的博客-CSDN博客_containerof  https://blog.csdn.net/rosetta/article/details/90751028#comments_20357943

```c
举例：

#include <stdio.h>

#define offset_of(type,member) ((unsigned int)&((type *)0)->member)

#define contain_of(ptr,type,mem) ({ \
    typeof(((type *)0)->mem) * mptr=(ptr);\
    (type *)((char *)mptr-offset_of(type,mem));\
})

struct list_head{
    struct list_head *prev;
    struct list_head *next;
};

struct ipstore{
    unsigned int time ;
    struct list_head list;
};

int main(int argc, char *argv[])
{
   struct ipstore ip1;
   struct ipstore *ip2;

   printf("ip1:  %0x\n",&ip1);
    ip2=contain_of(&ip1.list,struct ipstore,list);
   printf("ip2: %0x\n",ip2);
   return 0;
}

```
# 二、实现链表
Linux内核中链表的实现_sweird的博客-CSDN博客  https://blog.csdn.net/rosetta/article/details/90754660?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522165899238016781647580447%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=165899238016781647580447&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-2-90754660-null-null.nonecase&utm_term=list_entry&spm=1018.2226.3001.4450

Linux内核中链表的使用_sweird的博客-CSDN博客  https://blog.csdn.net/rosetta/article/details/7834545?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522165899238016781647580447%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=165899238016781647580447&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-3-7834545-null-null.nonecase&utm_term=list_entry&spm=1018.2226.3001.4450

Linux内核双向链表_weixin_45814428的博客-CSDN博客_linux 内核双向链表  https://blog.csdn.net/weixin_45814428/article/details/124758887

### 1、定义链表头
在使用链表时，需要定义一个链表头，方便索引整个链表,有三种方式：
==方式一和方式二三的区别是：方式一是以某个list_head为头节点。方式二三是以某个宿主数据结构中的list_head为头节点。方式二和方式三的区别是一个传入指针，一个传入名字==
```c
#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
    struct list_head name = LIST_HEAD_INIT(name)
    
//如果已知是一个struct list_head变量，则使用如下函数初始化
void INIT_LIST_HEAD(struct list_head *list)
{
    list->next = list;
    list->prev = list;
}

//方式一：由LIST_HEAD()实现，给定一个入参name，即链表头变量，把它定义为struct list_head，再把它的prev和next都指向自己
LIST_HEAD(addr_list); 

//方式二：首先看下动态创建的方法,参考：Linux内核之双向循环链表 - 疾速瓜牛 - 博客园  https://www.cnblogs.com/Arnold-Zhang/p/15267002.html
struct fox* red_fox = kmalloc(sizeof(*red_fox), GFP_KERNEL);
red_fox->weight = 10;
red_fox->tail_length = 20;
INIT_LIST_HEAD(&(red_fox->list));  //这个传入的是地址

//方式三：对于静态的创建方法：
struct fox red_fox = {
  .weight = 20,
  .tail_length = 10,
  .list = LIST_HEAD_INIT(red_fox.list), // 这个传入的是名字
}

```

### 2、对链表进行增删改等操作

### 3、测试
```c
#include <print.h>
#include <printk.h>
#include <interrupt.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <bitmap.h>
#include <memory.h>
#include <thread.h>
#include <list.h>

void k_thread_a(void* arg);

typedef struct stu{
    int num;
    struct list_head list;
}stu;

   int func(struct list_head *elem ,void * arg);

int  main()
{   
    clear_screen();
    interrupt_init();
    time_init();
    memory_init();

    // thread_start("k_thread_a", 31, k_thread_a, "argA ");

    //双向循环链表测试
    LIST_HEAD(head);

    stu ming;
    ming.num=1;

    stu hong;
    hong.num=2;

    stu wang;
    wang.num=3;

    list_push(&ming.list,&head);
    list_push(&hong.list,&head);
    list_push(&wang.list,&head);
    
    printk("len = %d \n\r",list_len(&head));

    struct list_head * find;
    stu *s;
    list_for_each(find,&head)
    {
        s=container_of(find,stu,list);//注意这里传入的是find，而不是&head->next或者&head

        printk("num = %d \r\n",s->num);
    }

    printk("-----after insert ---\r\n");
    struct stu *p;
    stu chen;
    chen.num=5;
    list_insert(&chen.list,&wang.list,&hong.list);

    list_for_each_entry(p,&head,list)
    {
        printk(" num = %d \r\n",p->num);
    }

    printk("-----after pop ---\r\n");

    list_pop(&head);

   
    list_for_each_entry(p,&head,list)
    {
        printk(" num = %d \r\n",p->num);
    }

    printk("-----after append ---\r\n");
    stu li;
    li.num=4;
    list_append(&li.list,&head);
    list_for_each_entry(p,&head,list)
    {
        printk(" num = %d \r\n",p->num);
    }
    
    printk("-----after del ---\r\n");
    list_del(&ming.list);
    list_for_each_entry(p,&head,list)
    {
        printk(" num = %d \r\n",p->num);
    }

    printk("-----test exist ---\r\n");
    if (list_exist(&head,&ming.list))
    {
        printk("ming exist \r\n");

    }else
    {
        printk("ming is not exist \r\n");

    }

    printk("-----test find ---\r\n");

    struct list_head * result = list_find(&head,func,6);
    if (result!=NULL)
    {
        stu * people=container_of(result,stu,list);

        printk("find num=%d \r\n",people->num);

    }else{
        printk("not found \r\n");
    }

    // asm volatile("sti");
    while(1);
    return 0;
}

/* 在线程中运行的函数 */
void k_thread_a(void* arg) {     
/* 用void*来通用表示参数,被调用的函数知道自己需要什么类型的参数,自己转换再用 */
   char* para = arg;
   while(1) { //这里必须要有while循环，因为现在线程栈中的返回地址我们设置成void (*reserverd);没有确切值
        printk("%s \r\n",arg);
   }

}
int func(struct list_head *elem ,void * arg)
{   
    // uint32_t num=(u32)arg[0];
    uint32_t num=arg;
    if (num==5)
    {
        return 1;
    }
    printk("num=============%d\r\n",num);
    return 0;
}

```
测试结果：
![](picture/双向循环链表测试结果.png)

# 三、注意点：
##1、强制类型转换, 指针变量的 地址数据 不变, 影响的只是指针的寻址(偏移)
指针 专题（ (int *)强制转换成(char *)，大小端）_deepwater_zone的博客-CSDN博客  https://blog.csdn.net/Hongwei_1990/article/details/106043155

```c
1、如何避免野指针
将指针初始化为NULL，用完后也将其赋值为NULL。


2、什么是大小端模式？请用C语言写出判断函数。
大端模式：内存低地址存放 数据的高字节；
小端模式：内存低地址存放 数据的低字节。
比如：将一个32位的整数0x12345678存放到一个整型变量（int）中，
大端：12-34-56-78； 小端：78-56-34-12

#include <stdio.h>
void main()
{
	int x = 1;
	char *p = (char *)&x;	//强制类型转换, 指针变量的 地址数据 不变, 影响的只是指针的寻址(偏移)
	if (*p)	printf("Little-endian \n");	//X86 或者 STM32 运行时打印
	else 	printf("Big-endian \n");
}


3、(int *)强制转换成(char *)（注意：X86，STM32是小端模式）
#include <stdio.h>
int main()
{
	unsigned int a = 0x11223344;		//x86是 小端模式，按照 内存地址递增 存储数据: 44-33-22-11
	unsigned char i = (unsigned char)a;

	char *b = (char *)&a;			//强制类型转换, 指针变量的 地址数据 不变, 影响的只是指针的寻址(偏移)
	unsigned int *c = &a;

	printf("i= %x, *b = %x\n\n", i, *b);	//0x44, 0x44
	printf("&a= %08x\n\nb = %08x\n", &a, b);//00affe30, 00affe30(跟&a相等)
	b++;
	printf("b = %08x\n", b);	//00affe31(内存地址+1, 相对&a增加1)
	printf("*b = %x\n\n", *b);	//0x33

	printf("c = %08x\n", c);	//00affe30(跟&a相等)
	c++;
	printf("c = %08x\n", c);	//00affe34(内存地址+4)
}
```
##2、void*的参数，如果要取它的值，直接取就好了
```c

struct list_head * result = list_find(&head,func,5);

int func(struct list_head *elem ,void * arg)
{   
    uint32_t num=arg;//比如说像这里，我打算arg传5进来的，如果使用uint32_t num=*((uint32_t *)arg)是不对的，数字才这么做
    // uint32_t num=(u32)arg[0]; 这样也是可以的哦
    if (num==5)
    {
        return 1;
    }
    printk("num=============%d\r\n",num);
    return 0;
}

类似的情况还有thread.c/thread_create()
thread_start("k_thread_a", 31, k_thread_a, "argA ");
void thread_create(struct task_struct* task, thread_func func ,void * func_arg)
{
    task->sp =task->sp - sizeof(struct intr_stack);//给中断栈留出空间
    task->sp =task->sp -sizeof(struct thread_stack); //给线程栈留出空间

    struct thread_stack* stack= (struct thread_stack*)task->sp;

    stack->eip = asm_call_c;

    stack->func_arg = func_arg;//像这里也是直接赋值
    stack->func = func;
    
    stack->ebp=0;//初始值为0
    stack->ebx=0;
    stack->edi=0;
    stack->esi=0;    
}


```