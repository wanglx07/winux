#一、几个实验
##实验1：使用BIOS中断打印字符串
```
org 0x7c00

jmp near start 
nop 

stackBase equ 0x7c00

bootMessage: db "Booting...."


start: 
    mov ax,cs
    mov ds,ax
    mov ss,ax 
    mov es,ax
    mov sp,stackBase

    ;清屏
    mov ax, 0x600 ;AH 功能号=0x06  AL＝上卷的行数（如果为 ，表示全部）
    mov bx, 0x700 ;BH ＝上卷行属性,07为黑底白字
    mov cx,0;左上角： (0, 0)
    mov dx, 0x184f ;右下角：（80,25），0x18=24, 0x4f=79
    int 0x10 ; 

    ;调用BIOS的0x10号中断的0x13号功能，打印字符串“Booting...”
    mov al,1;打印的模式
    mov bh,0;页号
    mov bl,0x07 ;黑底白字
    mov cx,11; 字符串中字符的个数
    mov dh,0 ;从第0行开始
    mov dl,0 ;从第0列开始
    mov bp,bootMessage ;字符串起始地址
    mov ah,0x13 
    int 0x10

    jmp $


times 510-($-$$) db 0
    dw 0xaa55
```
##实验2：参考了《x86汇编语言》实验5-1，打印字符串
```
mov ax,0xb800 ;指向文本模式的显示缓冲区
mov es,ax


;清屏
mov ax, 0x600 ;AH 功能号=0x06  AL＝上卷的行数（如果为 ，表示全部）
mov bx, 0x700 ;BH ＝上卷行属性,07为黑底白字
mov cx,0;左上角： (0, 0)
mov dx, 0x184f ;右下角：（80,25），0x18=24, 0x4f=79
int 0x10 ; 

;以下显示字符串“Booting...”
mov byte [es:0x00],'B'
mov byte [es:0x01],0x07
mov byte [es:0x02],'o'
mov byte [es:0x03],0x07 
mov byte [es:0x04],'o'
mov byte [es:0x05],0x07 
mov byte [es:0x06],'t'
mov byte [es:0x07],0x07 
mov byte [es:0x08],'i'
mov byte [es:0x09],0x07 
mov byte [es:0x0A],'n'
mov byte [es:0x0B],0x07 
mov byte [es:0x0c],'g'
mov byte [es:0x0d],0x07 
mov byte [es:0x0e],'.'
mov byte [es:0x0f],0x07 
mov byte [es:0x10],'.'
mov byte [es:0x11],0x07
mov byte [es:0x12],'.'
mov byte [es:0x13],0x07

infi :jmp near infi

times 510-($-$$) db 0
dw 0xaa55
```

##实验3：参考了《x86汇编语言》实验6-1，打印字符串
```
jmp near start 

boot_text db 'B',0x07,'o',0x07,'o',0x07,'t',0x07,'i',0x07,'n',0x07,'g',0x07,\
        '.',0x07,'.',0x07,'.',0x07

start:
    mov ax,0x7c0
    mov ds,ax 

    mov ax,0xb800
    mov es,ax 

    cld 
    mov si,boot_text
    mov di,0 
    mov cx,(start-boot_text)/2 ;实际上等于5
    rep movsw
    jmp near $


times 510-($-$$) db 0
    db 0x55,0xaa
```
##实验4：参考了《x86汇编语言》实验7-1,打印字符串
```

jmp near start 

boot_text db 'Booting...'

start:

    mov ax,3
    int 0x10

    mov ax,0x7c0
    mov ds,ax 

    mov ax,0xB800
    mov es,ax

    mov si,boot_text
    mov di,0 
    mov cx,start-boot_text

display:
    mov al,[si]
    mov [es:di],al
    inc di 
    mov byte [es:di],0x07 
    inc di 
    inc si 
    loop display 

    jmp $

times 510-($-$$) db 0
    db 0x55,0xaa
```

# 二、mbr.asm 读取内核加载器
- loader存放在逻辑扇区号为0x2扇区，则使用dd命令时seek=2
==注意== 如果一开始加载一个扇区，后面因为loader的增大，想一次加载多几个扇区，注意要修改makefile的dd命令的count值