# 实现步骤
## 一、shell的雏形
参考：shell/shell.c 的readline() 、open_shell()

主要就是通过open_shell()不停的打印输入提示和通过readline()读取输入的内容

![](picture/shell的简单实现结果.png)

==关键是环形缓冲区kfifo需要有生产者和消费者的功能，不然没内容的时候进程读不到只能return，open_shell()里面不停的循环==


## 二、解析键入的字符
参考：shell/shell.c的cmd_parse()

操作系统真象还原实验记录之实验三十一：实现简单的shell_mxy990811的博客-CSDN博客  https://blog.csdn.net/mxy990811/article/details/122792025?spm=1001.2014.3001.5502

函数接受命令字符串cmd_str,以及分隔符token（）就是’ ‘即空格字符。功能是将cmd_str的命令字依次放入argv数组中。
ASCLL中空格字符值是32，c语言中字符串结尾标志’\0’值是0.
argv是char*数组，也就是字符串数组。

模拟函数流程
假设命令字符串是“ is dir2 ”
那么这个函数会把“is”，“dir2”放入argv数组。
首先，一个while先把is前面的分隔符空格去除，然后将is放入argv[0]，如果is后面是个分隔符空格，那么就把‘\0’放入is后面作为argv[0]字符串的结束符。接着遍历dir2。
同样的逻辑，清除dir2前面的空格，将dir2放入argv[1]，遇到cmd_str的‘\0’，循环会结束，返回argv。
如果dir2后面有2个及以上的空格，那么会把dir2后的空格替换成’\0’，同时开始第三次大循环遍历不断跳过空格，当next指向’\0’后，则需要if来break退出循环。

```c
测试代码：shell.c

void open_shell()
{
    cwd_cache[0]='/';
    while(1)
    {
        input_prompt();
        memset(cmd_line,0,MAX_CMD_LENGTH);
        readline(cmd_line,MAX_CMD_LENGTH);
        if (cmd_line[0]==0)//如果只是输入回车，继续
        {
            continue;
        }

        //解析读到的cmd字符串
        argc= cmd_parse(cmd_line,argv,' ');
        if (argc == -1)
        {
            printf("The maximum number of parameters is %d \r\n",MAX_ARG_NUM);
            continue;
        }

        int32_t i=0;
        while(i<argc)
        {
            printf("%s  \r\n",argv[i]);
            i++;
        }
    }
}
```

![](picture/命令解析函数测试.png)


## 三、添加文件系统的一些系统调用
```c
#define __NR_getpid 0
#define __NR_write 1 
#define __NR_malloc 2 
#define __NR_free 3
#define __NR_fork 4 
#define __NR_read 5
#define __NR_putchar 6
#define __NR_clear 7
#define __NR_getcwd 8
#define __NR_open 9 
#define __NR_close 10 
#define __NR_lseek 11
#define __NR_unlink 12 
#define __NR_mkdir 13
#define __NR_opendir 14
#define __NR_closedir 15
#define __NR_chdir 16
#define __NR_rmdir 17 
#define __NR_readdir 18 
#define __NR_rewinddir 19
#define __NR_stat 20 
#define __NR_ps 21
```
## 四、路径解析
参考：/shell/buildin.c的get_abs_path()
==总思路是：==
- shell中输入的路径，可能是相对路径比如../a/b/c ./dir1，也可能是绝对路径如/a/b/c，还有一种可能是绝对路径里面包含'.'或'..'如/a/b/../c。
  
- 如果是相对路径，即第一个字符不是'/'的，则先获取当前工作路径，然后加在相对路径前面，如在/a/b目录下执行 cd ../dir1，因为../dir1是相对路径，所以获取当前工作路径放到../dir1前面即/a/b/../dir1，然后就解析这个路径
  
- 解析的思路是：
  - 如果解析得到的子路径既不是'.'，也不是'..'，那么就拼接到new_path；
  - 如果解析得到的子路径是'..'，那么就找到new_path中最后一个'/'，如果它不是最左边的'/'，如new_path为“/a/b”,".."之后则变为“/a”，如果它是最左边的'/'，如new_path为“/a”,".."之后则变为“/”。
  - 如果解析得到的子路径是'.'，则不对new_path进行任何操作

```c
测试代码：shell.c

void open_shell()
{
    cwd_cache[0]='/';
    while(1)
    {
        input_prompt();
        memset(cmd_line,0,MAX_CMD_LENGTH);
        readline(cmd_line,MAX_CMD_LENGTH);
        if (cmd_line[0]==0)//如果只是输入回车，继续
        {
            continue;
        }

        //解析读到的cmd字符串
        argc= cmd_parse(cmd_line,argv,' ');
        if (argc == -1)
        {
            printf("The maximum number of parameters is %d \r\n",MAX_ARG_NUM);
            continue;
        }

        int32_t i=0;
        char abs_path_buf[MAX_PATH_LENGTH]={0};
        while(i<argc)
        {
            get_abs_path(argv[i],abs_path_buf);//路径解析测试
            printf("%s --------->%s \r\n",argv[i],abs_path_buf);
            i++;
        }
    }
}
```
![](picture/路径解析测试结果.png)



## 五、实现常用shell命令
参考shell/buildin.c和shell/shell.c

==思路:==
```c
void open_shell()
{
    memset(cwd_cache,0,MAX_PATH_LENGTH);
    cwd_cache[0]='/';
    while(1)
    {
        //1 输入提示
        input_prompt();

        //2 读取键盘的输入保存到cmd_line
        memset(cmd_line,0,MAX_PATH_LENGTH);
        readline(cmd_line,MAX_PATH_LENGTH);
        if (cmd_line[0]==0)//如果只是输入回车，继续
        {
            continue;
        }

        //3 以空格为分隔符截取cmd_line字符串到argv数组中
        argc= cmd_parse(cmd_line,argv,' ');
        if (argc == -1)
        {
            printf("The maximum number of parameters is %d \r\n",MAX_ARG_NUM);
            continue;
        }

        //4、开始根据argv数组中的数据调用不同的命令进行处理
        if(!strcmp("cd",argv[0])){
            if (buildin_cd(argc,argv)==0)
            {
                memset(cwd_cache,0,MAX_PATH_LENGTH);
                strcpy(cwd_cache,abs_path);
            }
        }else if(!strcmp("pwd",argv[0])){
            buildin_pwd(argc,argv);
        }else if(!strcmp("mkdir",argv[0])){
            buildin_mkdir(argc,argv);
        }else if(!strcmp("rmdir",argv[0])){
            buildin_rmdir(argc,argv);
        }else if(!strcmp("rm",argv[0])){
            buildin_rm(argc,argv);
        }else if(!strcmp("ps",argv[0])){
            buildin_ps(argc,argv);
        }else if(!strcmp("clear",argv[0])){
            buildin_clear(argc,argv);
        }else if(!strcmp("ls",argv[0])){
            buildin_ls(argc,argv);
        }else{
            printf("external command\r\n");
        }
    }
```
测试结果：

![](picture/shell常用命令测试.png)

![](picture/shell常用命令ps测试.png)

从上面ps命令的结果来看，每运行一次ps，init_child进程的已花费时间ticks+1，这是因为当键盘没有输入的时候，环形缓冲区为空，init_child进程就阻塞，直到再次键入命令，唤醒init_child进程，才使ticks+1
## 六、注意
### 1、shell中滚不了屏，报缺页异常的解决办法
```c
lib/kernel/print.asm
 .roll_screen:
    cmp bx,2000 ;光标超出屏幕？滚屏
    jl .set_cursor

    cld
    ; mov esi, 0xb80a0 ;小心！32位模式下movsb/w/d
    ; mov edi, 0xb8000 ;使用的是esi/edi/ecx
    ;这里esi，和edi要使用高端地址，否则在用户进程中打开shell
    ;需要滚屏的时候会报缺页异常
    mov esi, 0xc00b80a0	; 第1行行首
    mov edi, 0xc00b8000	; 第0行行首
    mov ecx,960
```



