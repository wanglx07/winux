==这一节开始涉及这三种线程状态：  TASK_WAITING(父进程),TASK_HANGING(子进程),TASK_DIED(子进程最后状态)==
# 一、一些概念

- exit : 使进程“主动”退出，结束运行,让系统重新拿回处理器控制权的机会。可以是主动调用exit ，也可以是运行库调用的exit。们调用 exit 时必须要==提供个返回值==，会被内核放在 pcb 中。另外，进程在调用 exit 时就表示进程生命周期结束了，其占用的资
源可以被回收了，因此进程在调用 exit 后，内核会把该进程占用的大部分资源都回收，比如内存、页表等，但肯定不能将进程的 pcb 所占的内存回收，原因是里面存储着子进程的遗言，必须要交付给父进程

- wait : 一是使父进程阻塞，二是==获得子进程的返回值==，也就是获得子进程的"退出状态"。通过阻塞父进程，可以解决父子进程同步的问题,也就是协调父子进程某些代码的执行次序。==子进程 pcb 的回收是发生在父进程调用wait 之后==



- 孤儿进程：
![](picture/孤儿进程的定义.png)

- 僵尸进程
![](picture/僵尸进程的定义.png)

![](picture/exit_wait总结.png)


# 二、实现步骤

## 1、调整pid分配策略，使用位图进行管理
参考thread.c的alloc_pid()和free_pid()
==提交：e586ffd 调整pid分配策略，使用位图进行管理==

将pid的总个数固定为1024个

## 2、打开硬盘中断，使其能够唤醒进程，不然当你输入ls命令之后，由于main进程退出后，后面没有进程处于ready状态，系统就会运行idle_thread，如果没有硬盘中断唤醒shell进程使其重新ready，系统就一直idle
参考：提交 ==0e3d145 (HEAD -> master) 增加硬盘驱动的阻塞和唤醒功能，不然后面cat实验的时候，使用ls系统会没反应，因为没有线程处于ready，一直都是idle_thread运行，所以需要硬盘 中断来唤醒刚才ls的进程(ls会读硬盘),使其重新处于ready，系统才能继续往下运行==

==注意== 
==channel_ata0.want_intr=true一定要放在命令的前面，而不是放在down的前面就行==
```c
    channel_ata0.want_intr=true;//一定要放在命令前面
    outb(CMD_STATUS_PORT,CMD_IDENTIFY);//发送命令
    down(&channel_ata0.disk_sema);
```

==disk_write()中的down的位置==
disk_write disk_read的逻辑是相同的，区别是 disk_write 是将缓冲区 buf 中的数据写到硬盘，
阻塞的时机也有所不同 对于读硬盘来说，驱动程序阻塞自己是在硬盘开始读扇区之后，对于写硬盘来说，
驱动程序阻塞自己是在硬盘开始写扇区之后
```c

 参考disk_write():
        channel_ata0.want_intr=true;//这个一定要放在命令的前面
        outb(CMD_STATUS_PORT,CMD_WRITE);

        //4、检测硬盘状态是否已经准备好
        ...

        //5、开始将缓冲区的数据写入磁盘
        ...
        outsw(DATA_PORT,buf,bytes/2);
        down(&channel_ata0.disk_sema);//这句不能放在outb(CMD_STATUS_PORT,CMD_WRITE);后面
        done+=each;
    }
```

## 3、实现wait和exit

参考：wait_exit.c 

因为有了虚拟地址，需要有页目录表，才能找到物理地址
所以一个用户进程需要释放的东西有：
虚拟内存池位图---task.vir_pool_user.bmap
页目录表---task.pdt_base_vir
页---保存在页表项中
打开的文件
进程还有个pcb

==注意：exit回收的是内存，wait回收的是pcd和页表，wait调用的是thread_exit，只回收了主线程pcd。所以主线程的内核内存并不能回收。子进程调用exit，父进程调用wait。==

## 4、测试结果
![](picture/wait_exit_cat测试结果.png)

图中可以看出通过cat成功查看了file1的内容，而且main进程已经不存在，执行cat命令的进程pid为2（就是main进程释放的），返回值为66，说明成功执行了cat

## 总结
本次实验核心增加了exit、wait、cat。

exit和wait是为了父子进程执行顺序，也就是fork。
当父进程需要实现某功能时，他会调用fork来开一个子进程，
根据fork返回值pid，父进程走if，子进程走else。
然后子进程实现功能后，一直使用的是while(1)死循环，资源得不到释放，同时父进程的代码逻辑在if，父进程需要子进程告知功能是否完成，没完成需要等子进程，因此增加了exit与wait。

调用wait会返回两个值，一个是已结束子进程的child_pid，一个是该子进程的status。status由子进程调用exit提供，一般来说功能执行失败那么子进程将调用exit(-1)。依靠这两个值可以知道哪个子进程执行的功能是否成功，从而编写父进程代码。

现在来模拟一下所有涉及fork的代码执行顺序：

首先main函数，创造了init线程、主线程、idle线程。
主线程执行main函数，init线程执行init函数。
init函数中调用了fork后，父进程死循环调用wait，目的是为了回收所有孤儿进程的pid以及页表。
当孤儿进程执行exit后，返回它的status与pid，我们的操作系统编写的init进程只会打印他们。
再来看init进程fork出来的子进程，调用了open_shell()，open_shell是个死循环，不断接受并解析用户输入的命令，永远不会结束，故没有调用exit。

open_shell()不断打印命令行并readline接受用户输入命令，然后cmd_parse配合if-else逻辑解析此命令，决定执行哪一个系统调用。
我们以这次试验./cat file1举例分析：
因为./cat是外部命令，cat是一个提前存储在文件系统的用户程序，所以，open_shell调用fork开了一个子进程专门来执行这个外部命令，而与之对应的父进程调用wait用来接收子进程返回的child_pid与status，即使子进程功能失败，依然只是打印他们，但是如果child_pid为-1表示没有子进程，就要panic报错了。
接下来看专门用于执行外部命令的子进程，
get_abs_path可以把./cat这样带点的绝对路径洗成不带点的绝对路径，argv[0]=/cat，argv[1]=file1。检查了一下cat在不在文件系统，然后调用execv(argv[0], argv)，
execv调用load(argv[0])，
load解析cat的elf头，elf头里面是有每个可加载段应该被加载的内存地址，利用此信息，把cat每一个可加载段加载到相应内存地址中，返回
cat的入口地址即_start。
随后execv强制把当前进程修改成cat，使cpu执行cat，从start.s里的_start处执行，这样可以将argv传给cat的main，然后call main执行cat.c的功能。
cat.c的main函数自然知道argv[1]就是file1，这是个相对路径，故利用getcwd获取当前工作目录，拼成绝对路径，最后依靠open打开file1返回fd，read将file1以1024字节单位全部多次读入内存；write打印在显存。从而完成查看file1。

