# 实现步骤
参考：fs.c的sys_getcwd()
思路是由当前任务的cwd_i_num找到任务当前工作目录inode，再由这个inode的'..'找到父目录，遍历父目录的目录项，找到和当前工作目录inode对应的目录项，得到其目录项名字。再找到父目录的父目录，再循环找下去，直到找到根目录。

# 测试结果
测试代码：
```c

/********文件工作目录测试*************/
   char cwd_buf[32] ={0};
   sys_getcwd(cwd_buf,32);
   printk("current work directory = %s \r\n",cwd_buf);
   sys_chdir("/dir1");
   sys_getcwd(cwd_buf,32);
   printk("after chdir ,current work directory = %s \r\n",cwd_buf);

```
![](picture/任务工作目录显示和修改测试结果.png)


# 注意
1、main.c中调用fs.c的sys_getcwd() ，在main中打印的结果为空。因为cwd_buf和buf都指向同一块内存，但是在sys_getcwd()中将buf指向了字符串常量"/"，这个并没有改变cwd_buf中的内容，要理解指针的原理。
```c
fs.c
char * sys_getcwd(char * buf,uint32_t size)
{
    //获取线程当前工作目录
    struct task_struct * cur_task =get_current_thread();
    uint32_t cwd_i_num= cur_task->cwd_i_num;
    //如果是根目录，直接返回'/'
    if (cwd_i_num == 0)
    {
        
        buf="/";
        printk("buf = %s \r\n",buf);
        return buf;
    }
}


main.c 
   char cwd_buf[32] ={0};
   sys_getcwd(cwd_buf,32);
   printk("current work directory = %s \r\n",cwd_buf);//这里为空
```

如果要在sys_getcwd()修改main中的cwd_buf，可以这样改：
```c
char * sys_getcwd(char * buf,uint32_t size)
{
    //获取线程当前工作目录
    struct task_struct * cur_task =get_current_thread();
    uint32_t cwd_i_num= cur_task->cwd_i_num;
    //如果是根目录，直接返回'/'
    if (cwd_i_num == 0)
    {
        
        buf[0]='/';
        buf[1]=0;
        //或者*buf="/";
        printk("buf = %s \r\n",buf);
        return buf;
    }
}
```