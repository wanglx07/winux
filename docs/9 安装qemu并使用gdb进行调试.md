#一、安装qemu
参考 docs/相关软件使用笔记/qemu安装使用笔记

#二、需要调试可执行文件的话编译时添加 -g
##1、对于x86汇编文件：
    nasm -f elf -g $< -o $@  //记得加上-g
    ld -m elf_i386 -g -Ttext $(KERNEL_ENTRY_POINT)  $< -o $@   //记得加上-g，否则找不到符号信息

##2、对于c文件：
    gcc -m32 -g -c -o $(BUILD)/kernel/main.o $<  //记得加上-g
    ld -m elf_i386 $(BUILD)/kernel/main.o -Ttext $(KERNEL_ENTRY_POINT) -e main -o $@  //这里可以不用加上-g



#三、qemu和gdb联调
第1步：qemu-system-i386 -s -S -hda boot.img -monitor stdio  // boot.img需要加上路径，我的是在当前目录下所以省略掉了'./'

第2步：新开一个shell窗口

第3步：在新开的窗口中依次输入以下命令：

```
//./kernel.bin是想调试的那个文件，它必须已经写入boot.img
gdb ./kernel.bin  

//使用这个命令连接到qemu
target remote 127.0.0.1:1234 

//gdb由于不知道任何符号信息，并且也没有下断点，是不能进行源码级的调试的。为了让gdb获知符号信息，使用这个命令
    file ./kernel.bin  
```


随后就可以按照平时使用gdb的方法设置断点等进行调试了

==使用gdb配置文件==
在上面可以看到，为了进行源码级调试，需要输入较多的东西，很麻烦。为了方便，可以将这些命令存在脚本中，并让gdb在启动的时候自动载入。

以lab1为例，在lab1/tools目录下，执行完make后，我们可以创建文件gdbinit，并输入下面的内容：

    target remote 127.0.0.1:1234
    file bin/kernel
为了让gdb在启动时执行这些命令，使用下面的命令启动gdb：

    $ gdb -x tools/gdbinit
如果觉得这个命令太长，可以将这个命令存入一个文件中，当作脚本来执行。

参考：
 [用gdb调试nasm汇编程序 - 在于思考 - 博客园  ](https://www.cnblogs.com/chengxuyuancc/archive/2013/04/23/3038088.html)
 [使用gdb配置文件 · ucore_os_docs  ](https://chyyuu.gitbooks.io/ucore_os_docs/content/lab0/lab0_2_4_4_4_gdb_config_file.html)