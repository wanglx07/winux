#实现步骤：
##一、了解变长参数的原理
```c
//返回类型 n 占用的字节数，并且返回值为 sizeof(int) 的整数倍。换句话说，设空间最小粒度为 sizeof(int)
#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1)) 

//存储参数的类型信息，32位和64位实现不一样
typedef char * va_list;

/*
参数：
ap: 可变参数列表地址 
v: 确定的参数
功能：初始化可变参数列表，会把v之后的参数放入ap中
*/
#define va_start(ap,v) ( ap = (va_list)&v + _INTSIZEOF(v) )
 

/*功能：返回下一个参数的值
将ap移动_INTSIZEOF(t)个字节后，然后再取出ap未移动前所指向位置对应的数据,注意，该宏的第二个参数为类型
*/
#define va_arg(ap,t) (*(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)))

//功能：将0强转为va_list类型,并赋值给ap,使其置空,完成清理工作
#define va_end(ap) ( ap = (va_list)0 )

```
可变参数函数实现的步骤如下:

１.在函数中创建一个va_list类型变量
２.使用va_start对其进行初始化
３.使用va_arg访问参数值
４.使用va_end完成清理工作

实例：
```c
/*要使用变长参数的宏，需要包含下面的头文件*/
#include <stdarg.h>
/*
 * getSum：用于计算一组整数的和
 * num：整数的数量
 *
 * */
int getSum(int num,...)
{
    va_list ap;//定义参数列表变量
    int sum = 0;
    int loop = 0;
    va_start(ap,num);
    /*遍历参数值*/
    for(;loop < num ; loop++)
    {
        /*取出并加上下一个参数值*/
        sum += va_arg(ap,int);
    }
    va_end(ap);
    return sum;
}
int main(int argc,char *argv[])
{
    int sum = 0;
    sum = getSum(5,1,2,3,4,5);
    printf("%d\n",sum);
    return 0;
}
```
可得结果为：15

很好的文章：
[每天都在用printf，你知道变长参数是怎么实现的吗 - 腾讯云开发者社区-腾讯云](  https://cloud.tencent.com/developer/article/1523781)
[printf可变参数原理_mw_nice的博客-CSDN博客_printf可变参数原理](https://blog.csdn.net/mw_nice/article/details/82899575)
[可变参数模拟printf()函数实现一个my_print()函数以及调用可变参数需注意的陷阱 - tp_16b - 博客园  ](https://www.cnblogs.com/tp-16b/p/7868960.html)

##二、实现printk
主要是使用vsprintf.c