# 一、相关知识
![](picture/管道重定向原理.png)

# 二、实现步骤
## 1、shell中增加管道的操作

![](picture/重定向操作原理.png)

==注意这里的管道不是用于进程间通信的，而是用在一个进程中即shell进程中==

```c
参考shell.c的open_shell()
    while(1)
    {
        //1 输入提示
        input_prompt();
       
        //2 读取键盘的输入保存到cmd_line
        memset(cmd_line,0,MAX_PATH_LENGTH);
        readline(cmd_line,MAX_PATH_LENGTH);
        
        if (cmd_line[0]==0)//如果只是输入回车，继续
        {
            continue;
        }

        char * symbol_pos = strchr(cmd_line,'|');
        //3 先看cmd_line里面是否有'|'，有则要用到管道
        if (symbol_pos)
        {
            // 3.1、生成管道
            int32_t pipe_fd[2]={-1}; //fd[0]用于输入,fd[1]用于输出
            pipe(pipe_fd); //注意后面并没有fork()，说明不是用于父子进程间通信的，现在是在一个进程中进行

            //将标准输出重定向到fd[1]，使每条命令输出到屏幕的信息重定向到环形缓存区（管道）中
            //为什么会这样？因为当fd=STD_OUT的时候，sys_write()里面会检查，fd对应的文件结构类型是否是pipe
            //我们这里将pipe_fd[1]填进去了，所以肯定是pipe类型，会调用到pipe_write()
            fd_redirect(STD_OUT,pipe_fd[1]);

            // 3.2 第一条命令
            char * cmd_pos =cmd_line;
            symbol_pos = strchr(cmd_pos,'|');//找到第一个'|'位置，注意这里填的是cmd_pos
            * symbol_pos =0;//类似于cmd_parse()里面的pos=0,比如"ls | cat| cat "就变成 "ls 0 cat | cat"，这样就可截取ls 出来

            argc =cmd_parse(cmd_pos,argv,' ');
            cmd_hanler(argc,argv);

            cmd_pos= symbol_pos+1; //cmd_pos跨过'|'，指向下一条命令

            //将标准输入重定向到fd[0]，这样如果某一条命令是从标准输入拿数据的就会变成从管道拿数据
            //这是因为在sys_read()从会判断，fd对应的类型是否是pipe，我们这里将pipe_fd[0]填进去了，所以肯定是pipe类型
            //会调用到pipe_read()从管道中拿数据
            fd_redirect(STD_IN,pipe_fd[0]);

            // 3.3 中间的命令，命令的输入和输出都指向管道
            while(symbol_pos=strchr(cmd_pos,'|'))
            {
                * symbol_pos =0;
                argc =cmd_parse(cmd_pos,argv,' ');
                cmd_hanler(argc,argv);

                cmd_pos= symbol_pos+1; //cmd_pos跨过'|'，指向下一条命令
            }

            // 3.4 最后一个命令，比如aaa| bbb | ccc |ddd ,那么剩下就是ddd
            //将标准输出恢复屏幕,这样最后一个命令可以输出到屏幕
            //因为sys_write()中判断STD_OUT对应的文件结构不是pipe类型，所以会直接输出到屏幕
            fd_redirect(STD_OUT,STD_OUT);

            //处理最后一个命令
            argc =cmd_parse(cmd_pos,argv,' ');
            cmd_hanler(argc,argv);

            //将标准输入恢复为键盘,这样shell才能继续响应键盘的输入
            fd_redirect(STD_IN,STD_IN);

            //3.5 关闭管道
            close(pipe_fd[0]);
            close(pipe_fd[1]);

        }else{//否则按普通命令处理
            //以空格为分隔符截取cmd_line字符串到argv数组中
            argc= cmd_parse(cmd_line,argv,' ');
            if (argc == -1)
            {
                printf("The maximum number of parameters is %d \r\n",MAX_ARG_NUM);
                continue;
            }

            cmd_hanler(argc,argv);
        }
    }
```

## 2、增加一个从标准输入获取数据的命令
```c
参考 cat.c  

int main(int argc ,char **argv)
{
    if ( argc >2)
    {
        printf(" cat : argc error \r\n");
        exit(-2);
    }

    if (argc==1)
    {
        char buf[256]={0};
        read(0,buf,256);
        printf("%s \r\n",buf);
        exit(0);
    }
    ...
}

```

这里修改cat，无参时从标准输入获取数据，我们将标准输入重定向到管道，这样当使用无参的cat 时==如果它前面有管道，如ls -l | cat== 它就会从管道中拿数据，==如果它前面没管道，如仅仅cat==，那么就会从标准输入即键盘拿数据

# 三、测试结果
![](picture/shell支持多管道测试.png)

由上图看出，第一次执行"ls -l | cat"，输出一次"it's status is : 0"，status为0说明执行了执行了一次cat 

第二次执行"ls -l | cat |cat"，输出了两次"it's status is : 0"，status为0说明执行了两次无参cat。ls -l 将输出放入到管道中，第一个无参cat从标准输入读取，但是标准输入被重定向到管道了，所以实际是读取管道，再输出到管道，第二个cat从管道中读取，输出到屏幕

