#步骤
##1、将可执行文件中的程序段挪到相应的虚拟地址上
==需要熟悉elf文件的格式==：elf中包含了elf头、程序头表、节表、程序段
==我们的内核就是这些程序段，需要将其挪到相应的虚拟地址上==

```
;--------------------------------------------
;提取内核并进入内核运行
;--------------------------------------------
    mov ebx,[elf_base+28] ;elf偏移量28字节处是e_phoff,表示第一个program_header在elf文件中的偏移量
    add ebx,elf_base ;得到第一个program_header的起始虚拟地址
    mov ax,[elf_base+42] ;e_phentsize ，表示每个program_header的大小,注意取4个字节就够了，这里不能用eax
    mov cx,[elf_base+44] ;e_phnum,program_header的个数,注意取4个字节就够了，这里不能用ecx

    cmp byte [ebx+0],0 ;program_header偏移量为0字节处是type，这里判断是否为PT_NULL(也就是0)
    jz .next_program
.mov_program:
    ;为函数 mem_cpy 压入参数，参数是从右往左依然压入
    ;函数原型类似于 mem_cpy ( dst, src, size)
    push dword [ebx+16] ;program_header偏移量为16字节处是p_filesz,指明本段在文件中的大小,作为mem_cpy的第三个参数size
    mov edx,[ebx+4] ;program_header偏移量为4字节处是p_offset,指明本段在文件内的起始偏移字节
    add edx,elf_base ;得到本段当前所在的位置
    push edx ;作为mem_cpy的第二个参数src
    push dword [ebx+8] ;作为mem_cpy的第一个参数dst,program_header偏移量为8字节处是p_vaddr，用来指明本段在内存中的起始虚拟地址,程序需要被挪到这里，从这里开始执行

    call mem_cpy
    add esp,12 ;清空栈
    
.next_program:
    add ebx,eax ;类型为PT_NULL则跳过，处理下一个program_header
    loop .mov_program

```
- 首先，由elf_header得到program_header的偏移e_phoff（elf的加载位置+e_phoff可得==program_header的起始存放位置==）、大小e_phentsize、个数e_phnum
- 然后，遍历所有的program_header，这些program_header是挨在一起放的，知道程序段的当前偏移位置p_offset（==elf的加载位置==+p_offset可得==程序段的起始存放位置==，后面挪动的时候会用到）、需要挪动到的虚拟地址p_vaddr
- 最后，将程序段挪动到相应的虚拟地址即可
 
总而言之就是由elf_header得到各个program_header的位置、大小和数量-->由各个program_header得到各个程序段当前在内存中的位置及要挪到的目的虚拟地址-->挪动各个程序段
但是，注意挪完之后，程序段的虚拟地址不一定是程序的入口，比如程序段被挪动到起始虚拟地址p_vaddr=0xc0001000,程序的入口虚拟地址却是0xc0101500

结果：
==mem_cpy前内存的状况：==
![](picture/内核mem_cpy前.png)
![](picture/内核mem_cpy前内存情况.png)

==mem_cpy后内存的状况：==
![](picture/内核mem_cpy后.png)
![](picture/内核mem_cpy后内存情况.png)

elf相关网站：
    [ELF分析（1）_灰机_神的博客-CSDN博客_elf分析 ]( https://blog.csdn.net/m0_38017538/article/details/81199207)
    [ELF文件格式分析 | CN-SEC 中文网  ](http://cn-sec.com/archives/1035764.html)
    [ELF文件格式解析_乱世半仙的博客-CSDN博客_elf文件格式详解 ](https://blog.csdn.net/zj82448191/article/details/108441447)
    [UEFI第12话 为什么我被坑惨？字节级解释ELF格式_哔哩哔哩_bilibili  ](https://www.bilibili.com/video/BV1NY411p77L?spm_id_from=333.1007.top_right_bar_window_custom_collection.content.click)
##2、跳转到内核执行
```
jmp KENRNEL_ENTRY_POINT ;跳转到内核运行
```
结果：
![](picture/进入内核.png)
显示了字符'K',说明已经进入了内核，用main.c测试也是可以正常运行的