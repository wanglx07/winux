#include <printk.h>
#include <interrupt.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <bitmap.h>
#include <memory.h>
#include <thread.h>
#include <list.h>
#include <mutex.h>
#include <kfifo.h>
#include <tss.h>
#include <process.h>
#include <syscall.h>
#include <syscall_kernel.h>
#include <printf.h>
#include <bitmap.h>
#include <disk.h>
#include <fs.h>
#include <file.h>
#include <dir.h>
#include <shell.h>
#include <pipe.h>
#include <rbtree.h>

void init(void);
void test_a(void * args);
void test_b(void * args);
void process_entry_a(void);
void process_entry_b(void);

extern struct ata_channel channel_ata0; //通道ata0 
int  main()
{   
   console_init();
   interrupt_init();
   time_init();
   memory_init();
   main_thread_init();
   keyboard_init();
   tss_init();
   syscall_init();
           
   // process_start("pro_a",process_entry_a);
   // process_start("pro_b",process_entry_b);

   // thread_start("thread_a",1,test_a,"arg_a");
   // thread_start("thread_b",1,test_b,"arg_b");
   asm volatile("sti");
   disk_init();
   fs_init();

   clear();
   //前面main_thread_init()里面的init进程会fork出一个子进程来open_shell(),里面会调用到input_prompt()，但是我们又调用了clear()清屏，
   //所以这里得再调用一次input_prompt()
   input_prompt();

   /****************测试搜索二叉树***********/
   int arr[]= {1,5,4,3,2,6};

   BSTree root=NULL;
   printf("\r\nadd new bstree node:\r\n");
   int i;
   for ( i = 0; i < 6; i++)
   {
      printf("%d ",arr[i]);
      root = bstree_add(root,arr[i]);
   }

   printf("\r\npreorder:");
   preorder_bstree(root);

   printf("\r\ninorder:");
   inorder_bstree(root);

   printf("\r\npostorder:");
   postorder_bstree(root);

   printf("\r\n== min: %d\r\n", bstree_min(root)->key);
   printf("\r\n== max: %d\r\n", bstree_max(root)->key);
   printf("\r\n== tree's infomation: \r\n");
   bstree_print(root, root->key, 0);

   printf("\r\n== delete the root node: %d", arr[3]);
   root = bstree_delete(root, arr[3]);

   printf("\r\n== inorder: ");
   inorder_bstree(root);
   printf("\r\n");

   // 销毁二叉树
   bstree_destroy(root);


   // while(1){
   // //    printk("main  ...........\r\n");
   //    };
   thread_exit(get_current_thread(),true);
   return 0;
}


void test_a(void *args)
{
   //  void* addr1 = kmalloc(256);
   // void* addr2 = kmalloc(255);
   // void* addr3 = kmalloc(254);
   // printk(" thread_a malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   // int cpu_delay = 100000;
   // while(cpu_delay-- > 0);
   // kfree(addr1);
   // kfree(addr2);
   // kfree(addr3);
   while(1){};
}

void test_b(void *args)
{
//   void* addr1 = kmalloc(256);
//    void* addr2 = kmalloc(255);
//    void* addr3 = kmalloc(254);
//    printk(" thread_b malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

//    int cpu_delay = 100000;
//    while(cpu_delay-- > 0);
//    kfree(addr1);
//    kfree(addr2);
//    kfree(addr3);
   while(1){};
}

void init()
{
   uint32_t ret = fork();
  
   if (ret)
   {
      // while(1);
      int32_t status;
      int32_t child_pid;
      while(1)
      {
         child_pid=wait(&status);
         printf("I am init ,I receive a child ,it's pid is %d ,status is %d \r\n",child_pid,status);
      }
   }else{
      open_shell();
   }
}

void process_entry_a(void)
{
   printf("process_entry_a-----------\r\n");
   // void* addr1 = malloc(256);
   // void* addr2 = malloc(255);
   // void* addr3 = malloc(254);
   // printf(" pro_a malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   // int cpu_delay = 100000;
   // while(cpu_delay-- > 0);
   // free(addr1);
   // free(addr2);
   // free(addr3);
   while(1);
}

void process_entry_b(void)
{
 void* addr1 = malloc(256);
   void* addr2 = malloc(255);
   void* addr3 = malloc(254);
   printf(" pro_b malloc addr:0x %d  %d %d\r\n",(int)addr1,(int)addr2,(int)addr3);

   int cpu_delay = 100000;
   while(cpu_delay-- > 0);
   free(addr1);
   free(addr2);
   free(addr3);
   while(1);
}