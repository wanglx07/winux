#include <printf.h>
#include <syscall.h>
#include <string.h>

int main(int argc, char** argv) {

   int32_t fd[2] = {-1};
   pipe(fd);
   int32_t pid = fork();

   if(pid) {	  // 父进程
      close(fd[0]);  //只写不读，故关闭读描述符
      write(fd[1], "Hey, my son, daddy love you!", 29);
      printf("\r\nI'm father, my pid is %d\r\n", getpid());
      return 5;
   } else {
      close(fd[1]);  // 只读不写，故关闭写描述符
      char buf[32] = {0};
      int32_t i=900000;
      while(i--){}
      read(fd[0], buf, 29);
      printf("\r\nI'm child, my pid is %d\r\n", getpid());
      printf("I'm child, my father said to me: \"%s\"\r\n", buf);

      return 6;
   }
}
