#include <printf.h>
#include <syscall.h>
#include <string.h>
#include <file.h>

int main(int argc ,char **argv)
{
    if ( argc >2)
    {
        printf(" cat : argc error \r\n");
        exit(-2);
    }

    if (argc==1)
    {
        char buf[256]={0};
        read(0,buf,256);
        printf("%s \r\n",buf);
        exit(0);
    }

    int bufsize=1024;
    char abs_path[512]={0};
    void * buf=malloc(bufsize);

    if (buf==NULL)
    {
        printf("cat : malloc error \r\n");
        return -1;
    }

    if (argv[1][0] != '/') {
        getcwd(abs_path, 512);
        strcat(abs_path, "/");
        strcat(abs_path, argv[1]);
    } else {
        strcpy(abs_path, argv[1]); 
    }

    int fd=open(abs_path,O_RDONLY);
    if (fd == -1)
    {
        printf("cat : open error \r\n");
    }

    int count=0;
    while(1)
    {
        count = read(fd,buf,bufsize);
        if (count==-1)
        {
            break;
        }
        write(STD_OUT,buf,count);
    }
    free(buf);
    close(fd);
    return 66;

}