#include <printf.h>
#include <syscall.h>
#include <string.h>

int main(int argc,char **argv)
{
    int i =0;
    //打印参数
    while(i<argc)
    {
        printf("argv[%d] is %s \r\n",i,argv[i]);
        i++;
    }

    int pid = fork();
    if (pid)
    {
        int delay=900000;
        while(delay--);
        printf("I am father ,my pid is %d ,I will show process list \r\n",getpid());
        ps();
    }else{
        printf("I am child ,my pid is %d ,I will exec %s right now  \r\n",getpid(),argv[1]);
        char abs_path[512] = {0};
        if (argv[1][0] != '/') {
            getcwd(abs_path, 512);
            strcat(abs_path, "/");
            strcat(abs_path, argv[1]);
            execv(abs_path, argv);
        } else {
            execv(argv[1], argv);	 
        }
    }

    while(1);
    return 0;
}