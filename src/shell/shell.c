#include <fs.h>
#include <syscall.h>
#include <types.h>
#include <shell.h>
#include <string.h>
#include <buildin.h>
#include <exec.h>
#include <assert.h>

#define MAX_ARG_NUM 16	   // 最多支持15个参数


static char cmd_line[MAX_PATH_LENGTH] = {0};// 存储输入的命令 

char cwd_cache[MAX_PATH_LENGTH] = {0};// 用来记录当前目录,是当前目录的缓存,每次执行cd命令时会更新此内容 

extern char abs_path[MAX_PATH_LENGTH] ;//用来存储绝对路径，定义在buildin.c中,《操作系统真相还原》定义的是final_path，不够优雅

char* argv[MAX_ARG_NUM];  // 参数数组，argv必须为全局变量，以后exec的程序要用到 
int32_t argc = -1; //参数个数

//从键盘缓冲区读取最多max_length个字符到buf中
//主要就是做两件事，将字符保存进buf，输出到屏幕
void readline(char * buf,uint32_t max_length)
{
    char * position = buf;
    while(read(STD_IN,position,1) != -1 && (position-buf)< max_length)
    {
        switch(* position)
        {
			case '\r'://这才是回车键
                *position=0;//输入回车键意思是输入完毕，所以这里给字符串添加结束符0
                putchar('\r');//回到最前面
                putchar('\n');//下一行
                return;//这句千万不能少
            break;

            case '\b':
                if (position -buf !=0)//只能删除本次输入,没有这句的话会将前面readline输出到屏幕上的信息都删了
                {
                    position--;
                    putchar('\b');
                }
            break;

            default: //正常字符则输出
                putchar(*position);
                position++;
        }
    }
    //字符串最大长度不能超过128，并且应该按下回车键结束输入
    printf("The maximum length of the string cannot exceed 128, and the Enter key should be pressed to end the input \r\n");
}

//输入提示
void input_prompt()
{
    printf("wlx@host-007:%s$ ",cwd_cache);
}

//以token为分隔符截取cmd_str中的单词填入到argv中，最后返回截取到的单词个数
int32_t cmd_parse(char * cmd_str,char ** argv,char token)
{
    int32_t i=0;
    while(i<MAX_ARG_NUM)
    {
        argv[i]=NULL;
        i++;
    }

    char * pos= cmd_str;
    int32_t argc=0;
    while(* pos)
    {
        while(* pos == token)//跳过参数之前的空格，如"  aaa",则跳过aaa之前的空格
        {
             pos++;   
        }   

        if (* pos==0)//跳过了空格，但是最后没有字符，而是结束符0，如“   ”或者“abc  ”
        {
            break;//跳出，不执行下面的语句
        }

        argv[argc]= pos;//字符串首地址保存起来

        while(*pos && *pos !=token)//循环看字符串是否被空格分隔
        {
            pos++;
        }

        if(*pos)//如果跳出了上面的while，还没到cmd_str的末尾（末尾是结束符0），说明被空格分隔了
        {
            *pos=0; //人为添加结束字符0，使数组argv[]中的每个字符串都有边界

        }

        if (argc>MAX_ARG_NUM)
        {
            return -1;//如果参数过多，返回-1
        }

        pos++;//接着继续判断后面的字符
        argc++;
    }
    return argc;
}

void cmd_hanler(int32_t argc,char** argv)
{
    //开始根据argv数组中的数据调用不同的命令进行处理
    if(!strcmp("cd",argv[0])){
        if (buildin_cd(argc,argv)==0)
        {
            memset(cwd_cache,0,MAX_PATH_LENGTH);
            strcpy(cwd_cache,abs_path);
        }
    }else if(!strcmp("pwd",argv[0])){
        buildin_pwd(argc,argv);
    }else if(!strcmp("mkdir",argv[0])){
        buildin_mkdir(argc,argv);
    }else if(!strcmp("rmdir",argv[0])){
        buildin_rmdir(argc,argv);
    }else if(!strcmp("rm",argv[0])){
        buildin_rm(argc,argv);
    }else if(!strcmp("ps",argv[0])){
        buildin_ps(argc,argv);
    }else if(!strcmp("clear",argv[0])){
        buildin_clear(argc,argv);
    }else if(!strcmp("ls",argv[0])){
        buildin_ls(argc,argv);
    }else{
        int32_t pid = fork(); 
        if (pid)//父进程，啥也不做
        {
            // while(1);//没有这句的话子进程获取到的argv[0]始终为空，实在搞不懂
            int32_t status;
            int32_t child_pid = wait(&status);
            if (child_pid == -1)
            {
                panic("open_shell: wait error \r\n");
            }
            printf("child's pid is %d,it's status is:%d\r\n",child_pid,status);
        }else{//子进程
            //获取文件的绝对路径，注意存在argv[0]
            get_abs_path(argv[0],abs_path);
            argv[0] = abs_path;
            //判断文件是否存在
            struct stat file_stat;
            memset(&file_stat,0,sizeof(struct stat));
            if(stat(abs_path,&file_stat)==-1)//获取文件的属性
            {
                printf("open_shell: no such file or directory %s\r\n",abs_path);
                exit(-1);
            }else{
                execv(argv[0],argv);
            }
        }  
    }
}

void open_shell()
{
    memset(cwd_cache,0,MAX_PATH_LENGTH);
    cwd_cache[0]='/';
    while(1)
    {
        //1 输入提示
        input_prompt();
       
        //2 读取键盘的输入保存到cmd_line
        memset(cmd_line,0,MAX_PATH_LENGTH);
        readline(cmd_line,MAX_PATH_LENGTH);
        
        if (cmd_line[0]==0)//如果只是输入回车，继续
        {
            continue;
        }

        char * symbol_pos = strchr(cmd_line,'|');
        //3 先看cmd_line里面是否有'|'，有则要用到管道
        if (symbol_pos)
        {
            // 3.1、生成管道
            int32_t pipe_fd[2]={-1}; //fd[0]用于输入,fd[1]用于输出
            pipe(pipe_fd); //注意后面并没有fork()，说明不是用于父子进程间通信的，现在是在一个进程中进行

            //将标准输出重定向到fd[1]，使每条命令输出到屏幕的信息重定向到环形缓存区（管道）中
            //为什么会这样？因为当fd=STD_OUT的时候，sys_write()里面会检查，fd对应的文件结构类型是否是pipe
            //我们这里将pipe_fd[1]填进去了，所以肯定是pipe类型，会调用到pipe_write()
            fd_redirect(STD_OUT,pipe_fd[1]);

            // 3.2 第一条命令
            char * cmd_pos =cmd_line;
            symbol_pos = strchr(cmd_pos,'|');//找到第一个'|'位置，注意这里填的是cmd_pos
            * symbol_pos =0;//类似于cmd_parse()里面的pos=0,比如"ls | cat| cat "就变成 "ls 0 cat | cat"，这样就可截取ls 出来

            argc =cmd_parse(cmd_pos,argv,' ');
            cmd_hanler(argc,argv);

            cmd_pos= symbol_pos+1; //cmd_pos跨过'|'，指向下一条命令

            //将标准输入重定向到fd[0]，这样如果某一条命令是从标准输入拿数据的就会变成从管道拿数据
            //这是因为在sys_read()从会判断，fd对应的类型是否是pipe，我们这里将pipe_fd[0]填进去了，所以肯定是pipe类型
            //会调用到pipe_read()从管道中拿数据
            fd_redirect(STD_IN,pipe_fd[0]);

            // 3.3 中间的命令，命令的输入和输出都指向管道
            while(symbol_pos=strchr(cmd_pos,'|'))
            {
                * symbol_pos =0;
                argc =cmd_parse(cmd_pos,argv,' ');
                cmd_hanler(argc,argv);

                cmd_pos= symbol_pos+1; //cmd_pos跨过'|'，指向下一条命令
            }

            // 3.4 最后一个命令，比如aaa| bbb | ccc |ddd ,那么剩下就是ddd
            //将标准输出恢复屏幕,这样最后一个命令可以输出到屏幕
            //因为sys_write()中判断STD_OUT对应的文件结构不是pipe类型，所以会直接输出到屏幕
            fd_redirect(STD_OUT,STD_OUT);

            //处理最后一个命令
            argc =cmd_parse(cmd_pos,argv,' ');
            cmd_hanler(argc,argv);

            //将标准输入恢复为键盘,这样shell才能继续响应键盘的输入
            fd_redirect(STD_IN,STD_IN);

            //3.5 关闭管道
            close(pipe_fd[0]);
            close(pipe_fd[1]);

        }else{//否则按普通命令处理
            //以空格为分隔符截取cmd_line字符串到argv数组中
            argc= cmd_parse(cmd_line,argv,' ');
            if (argc == -1)
            {
                printf("The maximum number of parameters is %d \r\n",MAX_ARG_NUM);
                continue;
            }

            cmd_hanler(argc,argv);
        }
    }
}