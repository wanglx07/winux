#include <fs.h>
#include <string.h>
#include <buildin.h>
#include <syscall.h>
#include <dir.h>

char abs_path[MAX_PATH_LENGTH] ;//用来存储绝对路径

//old_path是包含'. '或'..'的绝对路径，比如在/a/b目录下执行cd ../dir1，实际上相当于cd /a/b/../dir1，
//也就是在../dir1前面加上了当前工作目录/a/b，old_abs_path就是/a/b/../dir1
//new_path是经过处理之后的绝对路径，处理之后cd /a/b/../dir1相当于cd /a/dir1,new_abs_path就是 /a/dir1
void path_handler(char * old_path,char * new_path)
{
    char name_buf[MAX_FILE_NAME_LENGTH] ={0};
    char * sub_path = old_path;
    sub_path = path_parse(old_path,name_buf);

    if (name_buf[0]==0)//说明old_path为一个或以上的“/”，没有往name_buf存东西，则将“/”存入new_path后返回
    {
        new_path[0]='/';
        new_path[1]=0;
        return;
    }

    new_path[0]=0;	// 避免传给new_path的缓冲区不干净，必须要有这步，这样下面strcat才能将/放在第0个位置
    strcat(new_path,"/");
    while(name_buf[0])//这里不能写while(sub_path)，因为如果old_path=/a，则第一次path_parse后sub_path=NULL，就不会执行下面的代码
    {
        if (!strcmp("..",name_buf))//如果是上一级目录“..”
        {
            char * last_slash_pos=strrchr(new_path,'/');//找到new_path最后的'/'的位置
            if (last_slash_pos!=new_path)//如果最后的'/'不是在最左边，如new_path为“/a/b/c”,".."之后则变为“/a/b”
            {
                *last_slash_pos=0;//最右边的'/'替换为0,这样便去除了new_path中最后一层路径,相当于到了上一级目录
            }else{//如果最后的'/'是在最左边,如new_path为“/a”,".."之后则变为“/”
                *(last_slash_pos+1)=0;
            }
        }else if(strcmp(".",name_buf)){// 如果路径不是'.'
            if (strcmp(new_path,"/")) //如果没有这句的话，会是这样//a/b/c
            {
                strcat(new_path,"/"); //先拼接个'/'
            }
            strcat(new_path,name_buf);//再将name_buf拼接到new_path
        }//如果name_buf为'.'，不用操作new_path

        memset(name_buf,0,sizeof(name_buf));

        if (sub_path)//不为NULL说明没解析到最后，还需要继续解析
        {
            sub_path=path_parse(sub_path,name_buf);
        }
    }
}

//将路径src_path(可以是相对路径，也可以是绝对路径)转成最终的绝对路径abs_path,注意这里的abs_path是外面传进来的
void get_abs_path(char * src_path,char * abs_path )
{
    char cwd[MAX_PATH_LENGTH] ={0};//当前工作目录
    if(src_path[0]!='/')//如果输入的不是绝对路径，则先获取当前工作目录
    {
        memset(cwd,0,MAX_PATH_LENGTH);
        if(getcwd(cwd,MAX_PATH_LENGTH)!=NULL)
        {
            if(!(cwd[0]=='/'&&cwd[1]==0))//如果不是根目录
            {
                strcat(cwd,"/");//要在最后追加'/'，比如/a/b，追加后为/a/b/
            }   
        }
    }
    strcat(cwd,src_path);//将当前工作目录放在src_path前面，比如/a/b下输入cd ../dir1,那么最后拼接得到cwd就是/a/b/..dir1 
    //这里有个疑问，如果输入的是绝对路径为什么还需要执行path_handler()进行转换呢？
    //原因是如果绝对路径里面包含'.'或者'..'，比如/a/b/../c，还是要解析的
    path_handler(cwd,abs_path);
}

int32_t buildin_mkdir(uint32_t argc, char** argv)
{
    int32_t ret =-1;
    if (argc !=2)
    {
        printf("mkdir: only one parameter is supported\r\n");
    }else{
        //获取绝对路径
        memset(abs_path, 0, MAX_PATH_LENGTH);
        get_abs_path(argv[1],abs_path);
        // printf("mkdir abs_path=%s \r\n",abs_path);
        if (strcmp("/",abs_path))//如果不是根目录
        {
            if (mkdir(abs_path)==0)
            {
                ret =0;
            }else{
                printf("mkdir: create directory failed\r\n");
            }    
        }
    }
    return ret;
}

int32_t buildin_rmdir(uint32_t argc, char** argv)
{
    int32_t ret =-1;
    if (argc !=2)
    {
        printf("rmdir: only one parameter is supported\r\n");
    }else{
        //获取绝对路径
        memset(abs_path, 0, MAX_PATH_LENGTH);
        get_abs_path(argv[1],abs_path);
        char cwd[MAX_PATH_LENGTH] ={0};//当前工作目录
        if (strcmp("/",abs_path))//不能删除根目录
        {
            getcwd(cwd,MAX_PATH_LENGTH);
            if (strcmp(cwd,abs_path))//不能在当前目录下删除当前目录
            {
                    if (rmdir(abs_path)==0)
                {
                    ret =0;
                }else{
                    printf("rmdir: remove directory failed\r\n");
                }    
            }else{
                printf("can't remove current directory in current directory\r\n");
            }
        }
    }
    return ret;
}

int32_t buildin_rm(uint32_t argc, char** argv)
{
    int32_t ret =-1;
    if (argc !=2)
    {
        printf("rm: only one parameter is supported\r\n");
    }else{
        //获取绝对路径
        get_abs_path(argv[1],abs_path);

        if (strcmp("/",abs_path))//不能删除根目录
        {
            if (unlink(abs_path)==0)
            {
                ret =0;
            }else{
                printf("rm: remove file failed\r\n");
            }    
        }
    }
    return ret;    
}

int32_t buildin_cd(uint32_t argc, char** argv)
{
    if (argc>2)
    {
        printf("cd: only one or zero parameter is supported\r\n");
    }
    memset(abs_path, 0, MAX_PATH_LENGTH);

    if (argc==1)//如果只输入cd，直接返回根目录
    {
        abs_path[0]='/';
        abs_path[1]=0;
    }else{
        get_abs_path(argv[1],abs_path);
    }

    if (chdir(abs_path)==-1)
    {
        printf("cd : no such directory %s \r\n",abs_path);
        return -1;
    }
    
    return 0;
}

void buildin_pwd(uint32_t argc, char** argv)
{
    if (argc != 1) {
        printf("pwd: no parameter is required \r\n");
        return;
    }

    memset(abs_path, 0, MAX_PATH_LENGTH);

    if (getcwd(abs_path,MAX_PATH_LENGTH)!=NULL)
    {
        printf("%s\r\n",abs_path);
    }else{
        printf("pwd: error \r\n");
    }
}

void buildin_ps(uint32_t argc, char** argv)
{
    if (argc != 1) {
        printf("ps: no parameter is required\r\n");
        return;
    }
    ps();
}

void buildin_clear(uint32_t argc, char** argv)
{
    if (argc != 1) {
        printf("clear: no parameter is required\r\n");
        return;
    }
    clear();
}

//最多支持一个选项'-l'，命令可以为ls 、 ls pathname 、ls -l 、ls -l pathname
void buildin_ls(uint32_t argc, char** argv)
{
    if (argc>3)
    {
        printf("ls: up to three parameters are supported\r\n");
    }
    
    uint32_t i=1;//注意是从1开始的，argv[0]是命令的名字ls
    bool long_display=false;
    char * pathname =NULL;
    while(i<argc) //如果argc大于1，除了名字ls，剩下的要么是-l，要么是pathname
    {
        if (argv[i][0] == '-')//如果是参数-l
        {
            if(!strcmp(argv[i],"-l"))
            {
                long_display = true;
            }
        }else{//如果是路径
            pathname = argv[i];
        }
        i++;
    }

    memset(abs_path, 0, MAX_PATH_LENGTH);

    if (pathname == NULL)// 如果只输入ls 或 ls -l,默认以当前路径的绝对路径为参数
    {
        if (getcwd(abs_path,MAX_PATH_LENGTH)!=NULL)
        {
            pathname=abs_path;
        }else{
            printf("ls: getcwd error \r\n");
            return;
        }
    }else{//输入带有路径，如ls pathname 、ls -l pathname
        get_abs_path(pathname,abs_path); 
        pathname= abs_path;
    }

    struct stat file_stat;
    memset(&file_stat,0,sizeof(struct stat));
    if(stat(pathname,&file_stat)==-1)//获取文件的属性
    {
        printf("ls: no such file or directory %s\r\n",pathname);
        return ;
    }
    
    if (file_stat.f_type == FT_DIRECTORY)//如果是目录
    {
        struct dir * dir=opendir(pathname);
        struct dir_entry * dir_e=NULL;
        char dir_e_abs_path[MAX_PATH_LENGTH] = {0};//目录项绝对路径
        uint32_t dir_path_len=strlen(pathname);
        memcpy(dir_e_abs_path,pathname,dir_path_len);//先将目录的名字拷贝进来
        if (dir_e_abs_path[dir_path_len-1] != '/')//如果目录的路径最后一个字符不是'/'
        {
            dir_e_abs_path[dir_path_len]='/';//在最后添上'/'，比如/a/b，则变成/a/b/，为的是后面拼出目录项的绝对路径
            dir_path_len++;
        }
        rewinddir(dir);//这句要有，不然下次ls不会从头遍历所有的目录项

        if(long_display){//有参数项 -l ，显示目录下各文件或子目录的详细信息
            printf("total: %d\r\n",file_stat.f_size); //打印目录下目录项的总个数
            char f_type;
            while(dir_e=readdir(dir))//遍历所有的目录项
            {
                f_type='d'; //目录项默认为是目录
                if (dir_e->f_type==FT_REGULAR)//如果目录项是普通文件
                {
                    f_type='-';
                }
                dir_e_abs_path[dir_path_len]=0;//这句话一定要加上，以便下面strcat，否则ls -l 出错
                strcat(dir_e_abs_path,dir_e->filename);//得到目录项的绝对路径
                memset(&file_stat,0,sizeof(struct stat));
                if(stat(dir_e_abs_path,&file_stat)==-1)//获取子目录项的属性
                {
                    printf("ls: no such dir_entry %s\r\n",dir_e_abs_path);
                    return ;
                }
                printf("%c  %d  %d  %s\r\n",f_type,dir_e->i_num,file_stat.f_size,dir_e->filename);
            }

        }else{//只简单显示该目录下的文件或子目录的名字
            while((dir_e=readdir(dir)))
            {
                printf("%s ",dir_e->filename);   
            }
            printf("\r\n");
        }

        closedir(dir);

    }else{//如果是普通文件
        if (long_display)//有参数项 -l ,显示普通文件的详细信息
        {
            printf("-  %d  %d  %s\r\n",file_stat.i_num,file_stat.f_size,pathname);
        }else{ //只显示路径名
            printf("%s \r\n",pathname);
        }
    }
}