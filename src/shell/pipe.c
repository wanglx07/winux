#include <pipe.h>
#include <thread.h>
#include <file.h>
#include <memory.h>
#include <kfifo.h>

//判断fd对应的文件是否是管道
bool is_pipe(uint32_t fd)
{
    struct task_struct * cur_task =get_current_thread();
    int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
    return file_table[f_idx].fd_flag==PIPE_FLAG;
}

//创建管道，成功返回0，失败返回-1。其实就是申请一页内存做环形缓冲区，缓存区的地址记录在fd_table的文件结构中，通过
//fd访问这个文件结构，从而访问到环形缓存区
int32_t sys_pipe(int32_t pipefd[2])
{
    //从文件表file_table中获取一个空闲位
    int32_t f_idx = free_file_slot();
    //申请一页内核内存做环形缓冲区,注意地址直接记录在fd_inode中
    file_table[f_idx].fd_inode = malloc_sys(sizeof(struct kfifo));
    file_table[f_idx].fd_inode =apply_kernel_mem(1);
    memset(file_table[f_idx].fd_inode, 0, PG_SIZE);
    if (file_table[f_idx].fd_inode == NULL)
    {
        return -1;
    }
    void * pdd=file_table[f_idx].fd_inode;

    //初始化环形缓冲区
    kfifo_init((struct kfifo *)(pdd));

    //表示此文件对应的是管道
    file_table[f_idx].fd_flag = PIPE_FLAG;

    //表示有两个文件描述符对应这个管道
    file_table[f_idx].fd_pos = 2;
    pipefd[0]= fd_install(f_idx);
    pipefd[1] = fd_install(f_idx);
    return 0;
}

//从管道中读数据
uint32_t pipe_read(int32_t fd,void * buf,uint32_t count)
{
    char * pos=buf;
    uint32_t has_read=0;
    struct task_struct * cur_task =get_current_thread();
    int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
    //获取管道的环形缓冲区
    struct kfifo*  pipe_buf =(struct kfifo*) file_table[f_idx].fd_inode;

    //选择较小的数据读取量,避免阻塞,假如此时管道为空，那么fifo_len=0,size=0,
    //has_read<size为false就不会执行kfifo_getchar()就不会进入阻塞
    uint32_t fifo_len=kfifo_len(pipe_buf);//可读的数据长度
    uint32_t size= fifo_len>count ? count: fifo_len;

    while(has_read < size)
    {
        *pos = kfifo_getchar(pipe_buf);
        pos++;
        has_read++;
    }

    return has_read;
}

//往管道中写数据
uint32_t pipe_write(int32_t fd,void * buf,uint32_t count)
{
    char * pos = buf;
    uint32_t has_write=0;
    struct task_struct * cur_task =get_current_thread();
    int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
    //获取管道的环形缓冲区
    struct kfifo*  pipe_buf =(struct kfifo*) file_table[f_idx].fd_inode;

    //选择较小的数据写入量,避免阻塞。假如此时管道满，那么fifo_left=0，size也为0，
    //has_write<size为false，就不会执行kfifo_putchar()，就不会进入阻塞
    uint32_t fifo_len=kfifo_len(pipe_buf);//缓冲区已有数据长度

    uint32_t fifo_left=BUF_SIZE - fifo_len;//剩余空间大小

    uint32_t size= fifo_left>count ? count: fifo_left;
    
    while(has_write < size)
    {
        kfifo_putchar(pipe_buf,*pos);
        pos++;
        has_write++;
    }
    return has_write;
}

//将文件描述符old_fd重定向为new_fd,其实就是将数组fd_table 中下标为old_fd的元素的值用下标为new_fd的元素的值替换
void sys_fd_redirect(uint32_t old_fd,uint32_t new_fd)
{
    struct task_struct * cur_task =get_current_thread();
    if (new_fd < 3)
    {
        cur_task->fd_table[old_fd] = new_fd;//如果是标准输入输出、错误，不用找f_idx，直接填数值，因为知道数值012分别表示输入输出错误了
    }else{
        uint32_t f_idx = cur_task->fd_table[new_fd];//找到新fd对应的文件结构在文件数组中的索引
        cur_task->fd_table[old_fd] = f_idx;
    }
}
