#include <thread.h>
#include <types.h>
#include <memory.h>
#include <list.h>
#include <assert.h>
#include <asm/io.h>
#include <interrupt.h>
#include <process.h>
#include <sprintf.h>
#include <fs.h>

#define PG_SIZE 4096

struct task_struct * main_thread;
struct list_head ready_list_head;//就绪队列
struct list_head total_list_head;//总队列


extern void switch_to(struct task_struct * cur,struct task_struct * next);

struct task_struct * idle_thread;

extern void init(void);

//pid位图，注意这里类型是uint8_t，也就是128个字节，最多支持1024个pid，在process.c的user_vir_pool_init()中位图
//都是通过向内核申请一整页,现在这里用不到这么大，所以申明为一个数组
uint8_t pid_bmap[128] = {0};

//pid池，和虚拟内存池的区别是没有起始地址ptr，而是换成了起始pid
struct pid_pool{
    uint32_t pid_start; //起始pid
    bitmap bmap;  //位图 
    mutex lock;
};

struct pid_pool pid_pool;

void pid_pool_init()
{
    pid_pool.pid_start=1;
    pid_pool.bmap.ptr = pid_bmap;
    pid_pool.bmap.length=128;
    bitmap_init(&pid_pool.bmap);
    mutex_init(&pid_pool.lock);
}

//分配pid
pid_t alloc_pid()
{
    mutex_lock(&pid_pool.lock) ; 
    int32_t position = bitmap_find(&pid_pool.bmap,1);
    bitmap_set(&pid_pool.bmap,position,1);
    mutex_unlock(&pid_pool.lock) ; 
    return pid_pool.pid_start+position;//第0个位空闲，则pid=1+0 ;第1个位空闲则pid=1+1，使得pid从1开始
}

//释放pid
void free_pid(pid_t pid)
{
    mutex_lock(&pid_pool.lock);
    int32_t position = pid - pid_pool.pid_start;
    bitmap_set(&pid_pool.bmap,position,0);
    mutex_unlock(&pid_pool.lock);
}

void thread_init(struct task_struct* task, char* name, int priority)
{
    memset(task,0, PG_SIZE);
    task->sp=(uint32_t *)((uint32_t)task+PG_SIZE);//pcb的栈指针放在最高端
    strcpy(task->name,name);
    task->pid=alloc_pid();
    task->priority=priority;
    if (task==main_thread)
    {
        task->status=TASK_RUNNING;
    }else{
        task->status=TASK_READY;
    }
    task->ticks=priority;
    task->elapsed_ticks=0;
    task->pdt_base_vir=NULL;

    //初始化文件描述符数组
    task->fd_table[0] = 0;//标准输入
    task->fd_table[1] = 1;//标准输出
    task->fd_table[2] = 2;//标准错误
    int i=3;
    for (; i < MAX_FILES_PER_PROC_OPEN; i++)
    {
        task->fd_table[i]=-1;
    }
    task->cwd_i_num=0;//当前工作目录默认为根目录
    task->parent_pid=-1;
    task->magic=0x20220727;
}

void asm_call_c(thread_func func,void * args)
{
    asm volatile("sti");//开中断
    outb(PIC_S_CTRL,0X20);//一定要加上这几句，否则后面就接收不到时钟中断了，也就切换不到其它线程了
    outb(PIC_M_CTRL,0X20);
    func(args);
}

void thread_create(struct task_struct* task, thread_func func ,void * args)
{
    task->sp = task->sp - sizeof(struct interrupt_stack);
    task->sp = task->sp - sizeof(struct thread_stack);
    struct thread_stack * stack = (struct thread_stack *)task->sp;
    stack->ebp=0x11111111;
    stack->ebx=0x22222222;
    stack->edi=0x33333333;
    stack->esi=0x44444444;
    //ret执行的时候，eip出栈，跳转到eip指向的位置执行，这个位置是个函数，但是这个函数不是通过call调用的，按照平时的习惯call调用函数时,
    //先往栈中压入参数，再压入返回位置，再跳到函数执行，现在是直接跳过来了，那么为了能够使得函数能够正常执行，我们可以手动构造出
    //栈的环境，比如手动压入函数的参数，返回地址
    stack->eip=asm_call_c;
    stack->func= func;
    stack->args=args;
}


struct task_struct* thread_start(char * name,uint32_t priority,thread_func func,void * args)
{
    struct task_struct * task=apply_kernel_mem(1);
    thread_init(task,name,priority);
    thread_create(task,func,args);//手动构造通过ret来调用函数，所必需的环境

    assert(!list_exist(&ready_list_head,&task->general_entry));//不在就绪队列中
    list_append(&task->general_entry,&ready_list_head);//加入就绪队列
    assert(!list_exist(&total_list_head,&task->total_entry));//不在总队列中
    list_append(&task->total_entry,&total_list_head);//加入总队列

  //打印一下
    // struct task_struct *p;
    // printk(" ready_list's length =%d \r\n",list_len(&ready_list_head));
    // printk(" total_list's length =%d \r\n",list_len(&total_list_head));
    // list_for_each_entry(p,&ready_list_head,general_entry)
    // {
    //     printk(" thread's name =%s \r\n",p->name);
    // }
    // printk(" -----------------------\r\n");
    // printk(" total_list's length =%d \r\n",list_len(&total_list_head));
    // list_for_each_entry(p,&total_list_head,total_entry)
    // {
    //     printk(" thread's name =%s \r\n",p->name);
    // }
    // p=get_current_thread();
    // printk(" current thread's name =%s \r\n",p->name);
    // printk(" -----------------------\r\n");

     return task;
}

struct task_struct * get_current_thread(void)
{
    uint32_t esp;
    asm volatile("movl %%esp,%0":"=r"(esp):);
    return (struct task_struct*)(esp & 0xfffff000);
}

//将 kernel 中的 main 函数完善为主线程
void create_main_thread()
{
    main_thread=get_current_thread();
    thread_init(main_thread,"main",1);
    assert(!list_exist(&total_list_head,&main_thread->total_entry));//不在总队列中
    list_append(&main_thread->total_entry,&total_list_head);//加入总队列
}

void idle_fun(void * args)
{
    while(1)
    {
        thread_block(TASK_BLOCKED);
        asm volatile ("sti;hlt"::);//开中断再休眠，不然唤不醒了
    }
}

void main_thread_init()
{
    INIT_LIST_HEAD(&ready_list_head);
    INIT_LIST_HEAD(&total_list_head);
    pid_pool_init();
    process_start("init",init);//创建用户进程init，使其pid为1，init在main.c中
    create_main_thread(); //创建主线程
    idle_thread =thread_start("thread_idle",1,idle_fun,NULL); //创建空闲线程
}

void schedule()
{
    asm volatile("cli");//关中断

    struct task_struct * current_thread=get_current_thread();
    if (current_thread ->status==TASK_RUNNING)
    {
        assert(!list_exist(&ready_list_head,&current_thread->general_entry));//不在就绪队列中
        current_thread->ticks=current_thread->priority;
        current_thread->status=TASK_READY;
        list_append(&current_thread->general_entry,&ready_list_head);//加入就绪队列
    }
    //就绪队列为空，则唤醒空闲线程
    if (list_empty(&ready_list_head))
    {
        thread_unblock(idle_thread);
    }

    assert(!list_empty(&ready_list_head));
    struct list_head * entry=list_pop(&ready_list_head);

    struct task_struct * next_thread=container_of(entry,struct task_struct,general_entry);

    next_thread->status=TASK_RUNNING;

    // printk(" -------after schedule ----------------\r\n");
    // struct task_struct *p;
    // printk(" ready_list's length =%d \r\n",list_len(&ready_list_head));
    // printk(" total_list's length =%d \r\n",list_len(&total_list_head));
    // list_for_each_entry(p,&ready_list_head,general_entry)
    // {
    //     printk(" ready_list thread =%s \r\n",p->name);
    // }
    // printk(" current thread's ticks =%d \r\n",current_thread->ticks);
    // printk(" current thread's name =%s \r\n",current_thread->name);
    // printk(" next thread's name =%s \r\n",next_thread->name);
    // printk(" -----------------------\r\n");
    
    refresh_cr3_tss(next_thread);

    switch_to(current_thread,next_thread);
    
}

void thread_block(enum task_status status)
{
    enum intr_status old_status=intr_disable();
    assert(status==TASK_BLOCKED || status== TASK_WAITING || status==TASK_HANGING);
    struct task_struct * task =get_current_thread();
    task->status=status;
    schedule();
    intr_restore_status(old_status);
}

void thread_unblock(struct task_struct * task)
{
    enum intr_status old_status=intr_disable();
    if(!list_exist(&ready_list_head,&task->general_entry));
    {
        list_push(&task->general_entry,&ready_list_head);
        task->status=TASK_READY;
    }
    intr_restore_status(old_status);

}

//让出CPU使用权
void thred_yield()
{
    enum intr_status old_status=intr_disable();
    struct task_struct * task =get_current_thread();
    if(!list_exist(&ready_list_head,&task->general_entry));//避免重复添加
    {
        list_append(&task->general_entry,&ready_list_head); //加入到就绪队列队尾
        task->status=TASK_READY;
    }
    schedule();
    intr_restore_status(old_status);
}

//对齐输出，原理是先用 switch 结构中sprintf函数把待输出的字符串src写入缓冲区 buf,
//buf 的长度是bufsize ，这是固定的值，无论宇符串src是多少字符，永远输出bufsize长度，如果src长度不足
//bufsize ，就以空格来填充
void align_output(char * buf,uint32_t bufsize,void * src,char format)
{
    memset(buf,0,bufsize);
    uint8_t length=0;//输出字符串的长度
    switch(format)
    {
        case 's':
        length=sprintf(buf,"%s",src);
        break;
        case 'd':
        length= sprintf(buf,"%d",*((uint32_t*)src));
    }
    while(length<bufsize-1)//长度不足，用空格填充，bufsize-1保证以0结尾
    {
        buf[length]=' ';
        length++;
    }

    sys_write(STD_OUT,buf,bufsize);
}

//打印线程信息
bool print_thread(struct list_head * p,void * args )
{
    struct task_struct * task=container_of(p,struct task_struct,total_entry); 
    uint32_t bufsize =16;
    char buf[16]={0};

    //输出pid
    align_output(buf,bufsize,&task->pid,'d');

    //输出ppid
    if (task->parent_pid==-1)
    {
        align_output(buf,bufsize,"NULL",'s');
    }else{
        align_output(buf,bufsize,&task->parent_pid,'d');
    }

    //输出status
    switch (task->status) {
        case TASK_RUNNING:
            align_output(buf, bufsize, "RUNNING", 's');
        break;
        case TASK_READY:
            align_output(buf, bufsize, "READY", 's');
        break;
        case TASK_BLOCKED:
            align_output(buf, bufsize, "BLOCKED", 's');
        break;
        case TASK_WAITING:
            align_output(buf, bufsize, "WAITING", 's');
        break;
        case TASK_HANGING:
            align_output(buf, bufsize, "HANGING", 's');
        break;
        case TASK_DIED:
            align_output(buf, bufsize, "DIED", 's');
    }

    //输出已运行的时间
    align_output(buf,bufsize,&task->elapsed_ticks,'d');

    //输出线程名字
    memset(buf,0,bufsize);
    memcpy(buf,task->name,strlen(task->name));
    strcat(buf,"\r");
    strcat(buf,"\n");
    sys_write(STD_OUT,buf,bufsize);

    return false;//让list_find继续循环
}

void sys_ps()
{
    char* ps_title = "PID            PPID           STAT           TICKS          NAME\r\n";
    sys_write(STD_OUT, ps_title, sizeof(ps_title));
    list_find(&total_list_head, print_thread, NULL);
}

//对比entry对应的task的pid是否为参数中的pid，是则返回true，否则返回false
bool pid_compare(struct list_head * entry,uint32_t pid )
{
    struct task_struct * task=container_of(entry,struct task_struct,total_entry);
    if (task->pid == pid)
    {
        return true;
    }
    return false;
}

//根据pid在线程总队列中查找pcb ，若找到则返回该pcb，否则返回 NULL
struct task_struct * pid2thread(uint32_t pid)
{
    struct list_head * entry= list_find(&total_list_head, pid_compare, pid);
    if (entry==NULL)
    {
        return NULL;
    }

    struct task_struct * task=container_of(entry,struct task_struct,total_entry);

    return task;
}

//回收 任务thread_over的 pcb 和页目录表，并将其从调度队列中去除
void thread_exit(struct task_struct * task_over,bool need_schedule)
{
    task_over->status=TASK_DIED;

    //从就绪队列中移除
    if(list_exist(&ready_list_head,&task_over->general_entry))
    {
        list_del(&task_over->general_entry);
    }

    //从总队列中移除
    list_del(&task_over->total_entry);

    //如果是用户进程，回收页目录表
    if (task_over->pdt_base_vir)
    {
        free_page(PHY_POOL_KERNEL,task_over->pdt_base_vir,1);
    }

    //归还pid
    free_pid(task_over->pid);

    //回收 pcb 所在的页，主线程的 pcb 不在堆中，跨过
    if(task_over != main_thread)
    {
        free_page(PHY_POOL_KERNEL,task_over,1);
    }


    if (need_schedule)
    {
        schedule();
        panic("thread_exit : error\r\n");
    }
}
