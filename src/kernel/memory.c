#include <memory.h>
#include <types.h>
#include <string.h>
#include <assert.h>
#include <bitmap.h>
#include <thread.h>
#include <mutex.h>
#include <interrupt.h>


#define PHY_BMAP_BASE 0xc009a000 //物理内存池位图起始地址
#define KERNEL_VIR_ADDR_START 0xc0100000 //内核虚拟内存起始地址

typedef struct ards{
    uint64_t base_addr;
    uint64_t len;
    uint16_t type;
}ards;


struct phy_pool phy_pool_kernel; //内核物理内存池
struct phy_pool phy_pool_user; //用户物理内存池
struct vir_pool vir_pool_kernel ; //内核虚拟内存池

struct mem_block_type mem_type_table_k[TYPE_COUNT]; //内核内存块类型数组

//从ards_buf中寻找最大的内存块，一定是操作系统可使用的部分，即主板上配置的物理内存容量。
uint32_t get_mem_total_size()
{ 
    uint32_t ards_count;
    int i;
    uint32_t max_mem;
    uint32_t tmp;

    ards * ards_buf;
    ards_count = * ((uint16_t *)0x1039);
    ards_buf = (ards *)0x103b;//在loader.asm中

    max_mem=(uint32_t)ards_buf->base_addr+(uint32_t)ards_buf->len;

    for(i=1 ; i<ards_count ; i++)
    {
        tmp=(uint32_t)ards_buf->base_addr+(uint32_t)ards_buf->len;//（base_add low + length low ）的最大值，即内存的容量
        if (tmp>max_mem)
        {   
            max_mem=tmp;
        }
        ards_buf++;
    }
    return max_mem;
}

void phy_pool_init(uint32_t mem_total_size)
{   
    /***************求出能使用的空闲内存******************/
    uint32_t page_used = 256;//1M之外已使用的页=1张页目录表+ 1张页表（0和第768目录项对应）+内核剩余254个目录项对应的254个页表 
    uint32_t mem_used = page_used * 4096 +0x100000 ; //已经使用的物理内存
    uint32_t mem_free = mem_total_size - mem_used; //空闲的物理内存
    uint32_t page_free = mem_free / PG_SIZE ;//空闲的页
    uint32_t page_free_kernel =page_free / 2; //内核能使用的空闲页
    uint32_t page_free_user = page_free - page_free_kernel ;//用户能使用的空闲页

    /***************初始化物理内存池******************/
    phy_pool_kernel.ptr = mem_used; //内核物理内存池起始地址
    phy_pool_user.ptr = phy_pool_kernel.ptr + page_free_kernel * PG_SIZE; //用户物理内存池起始地址

    phy_pool_kernel.size = page_free_kernel * PG_SIZE; //容量  多少个字节
    phy_pool_user.size = page_free_user * PG_SIZE;

    phy_pool_kernel.bmap.length = page_free_kernel / 8; //位图长度 单位字节
    phy_pool_user.bmap.length = page_free_user / 8 ;
    phy_pool_kernel.bmap.ptr = (uint8_t *)PHY_BMAP_BASE ;
    phy_pool_user.bmap.ptr =(uint8_t *) (PHY_BMAP_BASE+ phy_pool_kernel.bmap.length); //用户物理内存池位图放在内核物理内存池位图的后面

    bitmap_init(&phy_pool_kernel.bmap); //位图初始化
    bitmap_init(&phy_pool_user.bmap);

    mutex_init(&phy_pool_kernel.lock);//初始化锁
    mutex_init(&phy_pool_user.lock);

    // printk("kernel phy_addr_start = %p ,bimap_start = %d \r\n",phy_pool_kernel.ptr,(int)phy_pool_kernel.bmap.ptr);
    // printk("user phy_addr_start = %p ,bimap_start = %d \r\n",phy_pool_user.ptr,(int)phy_pool_user.bmap.ptr);
}


void vir_pool_init()
{   
    //起始地址
    vir_pool_kernel.ptr = KERNEL_VIR_ADDR_START;
    //初始化内核虚拟地址的位图，容量和内核能使用的物理内存大小一样
    vir_pool_kernel.bmap.length = phy_pool_kernel.bmap.length;
    //内核虚拟内存位图放在 内核物理内存位图和用户物理内存位图之后
    vir_pool_kernel.bmap.ptr = (uint8_t *) (PHY_BMAP_BASE+ phy_pool_kernel.bmap.length +phy_pool_user.bmap.length);
    //初始化位图
    bitmap_init(&vir_pool_kernel.bmap);
    //初始化锁
    mutex_init(&vir_pool_kernel.lock);
}

void memory_init()
{
    uint32_t mem_total_size;
    mem_total_size=get_mem_total_size();
    phy_pool_init(mem_total_size);
    vir_pool_init();
    mem_type_table_init(mem_type_table_k);
}



void * vir_addr_get(enum phy_pool_flag pf,uint32_t pg_count)
{
    int position;
    int i;
    uint32_t vir_addr_start;
    if(pf == PHY_POOL_KERNEL)
    {
        //从位图中查找是否有符合条件的空闲页
        position = bitmap_find(&vir_pool_kernel.bmap,pg_count);
        if (position==-1)
        {
            return NULL;
        }
        struct task_struct * cur_task =get_current_thread();
        //如果找到可用的页，将位图中相关的位置1,标记为已使用
        for ( i = 0; i < pg_count; i++)
        {
            bitmap_set(&vir_pool_kernel.bmap,position+i,1);
                
        }

        //得到申请成功后的起始虚拟地址
        vir_addr_start = vir_pool_kernel.ptr + position * PG_SIZE;
    }else{
        // 用户内存池
        struct task_struct * cur_task=get_current_thread();

        //从用户进程位图中查找是否有符合条件的空闲页
        position = bitmap_find(&cur_task->vir_pool_user.bmap,pg_count);
        if (position==-1)
        {
            return NULL;
        }

        //如果找到可用的页，将位图中相关的位置1,标记为已使用
        for ( i = 0; i < pg_count; i++)
        {
            bitmap_set(&cur_task->vir_pool_user.bmap,position+i,1);
        }

        //得到申请成功后的起始虚拟地址
        vir_addr_start = cur_task->vir_pool_user.ptr + position * PG_SIZE;

    }
    return (void *)vir_addr_start;
}

void * phy_addr_get(struct phy_pool * pool)
{
    int position;
    uint32_t phy_addr_start;
    //从位图中查找是否有符合条件的空闲页
    position = bitmap_find(&pool->bmap,1);
    if (position == -1)
    {
        return NULL;
    }
    bitmap_set(&pool->bmap,position,1);//如果找到可用的页，将位图中相关的位置1,标记为已使用
    phy_addr_start=pool->ptr + position*PG_SIZE;
    return (void*) phy_addr_start;
}

//获取vaddr对应的pde的物理地址
uint32_t * pde_get(uint32_t vaddr)
{
    return (uint32_t *)( 0xfffff000  | (vaddr >> 22 )*4 );
}

//获取vaddr对应的pte的物理地址
uint32_t * pte_get(uint32_t vaddr)
{
    return (uint32_t *)(0x3ff << 22 | (vaddr & 0xffc00000) >>10 | ((vaddr & 0x003ff000)>>12)*4 );
}

void mapping(void * vir_addr , void * phy_addr)
{
    uint32_t vaddr = (uint32_t)vir_addr;
    uint32_t paddr = (uint32_t)phy_addr;
    uint32_t* pde = pde_get(vaddr);
    uint32_t* pte = pte_get(vaddr);
    
    if(*pde & 0x1) //由P位看看对应的页表是否存在
    {

        assert(!(*pte & 0x1));

        if (!(*pte & 0x1))
        {
            *pte = paddr  | 0x7 ;// us=1,w=1,p=1 这里paddr不用左移3位
        }else{
            panic("pte repeat");
            
        }
    }else{
        uint32_t pt_phy_addr=(uint32_t) phy_addr_get(&phy_pool_kernel);//如果页表不存在则申请一个页作为页表

        *pde = pt_phy_addr  | 0x7;

        memset((void*)((uint32_t)pte & 0xfffff000), 0, PG_SIZE);//注意memset使用的是虚拟地址，然后这里应该使用pte，不是pde
        assert(!(*pte & 0x1));
        *pte = paddr | 0x7 ;
    }
}

void * malloc_page(enum phy_pool_flag pf,uint32_t pg_count)
{
     //1、申请虚拟内存
     void * vir_addr_start = vir_addr_get(pf,pg_count);

     if (vir_addr_start == NULL)
     {
         return NULL;
     }

    uint32_t vir_addr = (uint32_t)vir_addr_start;
    struct phy_pool * mem_pool = pf == PHY_POOL_KERNEL? &phy_pool_kernel : &phy_pool_user;

     //因为虚拟地址是连续的,但物理地址可以是不连续的,所以逐个做映射
    for(;pg_count>0;pg_count--)
    {
        void * phy_addr = phy_addr_get (mem_pool);//2、申请物理内存 3、虚拟内存和物理内存建立映射
        if (phy_addr == NULL)
        {
            return NULL;
        }
        mapping((void *)vir_addr,phy_addr); //映射
        vir_addr += PG_SIZE;
    }
    return vir_addr_start;
}

//从内核物理内存池中申请pg_count页物理内存，成功则返回其对应的虚拟地址，失败则返回null
void * apply_kernel_mem(uint32_t pg_count)
{
    mutex_lock(&phy_pool_kernel.lock);
    void * vir_addr=malloc_page(PHY_POOL_KERNEL,pg_count);
    if (vir_addr != NULL)
    {
        memset(vir_addr , 0 , pg_count*PG_SIZE);
    }
    mutex_unlock(&phy_pool_kernel.lock);
    return vir_addr;
}

//为当前进程一个虚拟地址vir_addr申请一个物理页
void * apply_a_page(enum phy_pool_flag pf,uint32_t vir_addr)
{
    struct phy_pool * mem_pool = pf == PHY_POOL_KERNEL? &phy_pool_kernel : &phy_pool_user;
    mutex_lock(&mem_pool->lock);
    //1、修改虚拟地址对应的位图
    struct task_struct * cur_task=get_current_thread();
    int32_t position = -1;
    if (cur_task->pdt_base_vir!=NULL && pf == PHY_POOL_USER)
    {
        position = (vir_addr - cur_task->vir_pool_user.ptr)/PG_SIZE;
        bitmap_set(&cur_task->vir_pool_user.bmap,position,1);   
    }else if(cur_task->pdt_base_vir == NULL && pf == PHY_POOL_KERNEL){
        position = (vir_addr - vir_pool_kernel.ptr)/PG_SIZE;
        bitmap_set(&vir_pool_kernel.bmap,position,1);
    }else{
        panic("apply  a page fault");
    }

    //2、申请物理页
    void * phy_addr = phy_addr_get(mem_pool);
    if (phy_addr==NULL)
    {
        mutex_unlock(&mem_pool->lock);
        return NULL;
    }

    //3、进行映射
    mapping((void *)vir_addr,phy_addr); 

    mutex_unlock(&mem_pool->lock);
    return (void*)vir_addr;
}

//不需要设置虚拟内存池位图，为当前进程一个虚拟地址vir_addr申请一个物理页
void * apply_a_page_no_bmap(enum phy_pool_flag pf,uint32_t vir_addr)
{
    struct phy_pool * mem_pool = pf == PHY_POOL_KERNEL? &phy_pool_kernel : &phy_pool_user;
    mutex_lock(&mem_pool->lock);
     //1、申请物理页
    void * phy_addr = phy_addr_get(mem_pool);
    if (phy_addr==NULL)
    {
        mutex_unlock(&mem_pool->lock);
        return NULL;
    }

    //2、进行映射
    mapping((void *)vir_addr,phy_addr); 
    mutex_unlock(&mem_pool->lock);
    return (void*)vir_addr;
}

//得到虚拟地址映射到的物理地址
uint32_t to_phy_addr(uint32_t vir_addr)
{
    uint32_t* pte = pte_get(vir_addr);
    return ((*pte & 0xfffff000) + (vir_addr & 0x00000fff));//pte里面存的值再加上虚拟地址的低12位就是要求的物理地址
}

//初始化内存块分配表
void mem_type_table_init(struct mem_block_type * table)
{
    uint32_t block_size=16;
    int i;
    for ( i = 0; i < TYPE_COUNT; i++)
    {
        table[i].block_size=block_size;
        table[i].blocks_per_arena=(PG_SIZE-sizeof(struct arena))/block_size;
        INIT_LIST_HEAD(&table[i].free_list_head);
        block_size *=2;
    }
}


//返回arena中第index个内存块的地址
struct mem_block * arena2block(struct arena * a,uint32_t index)
{
     //跨过arena的大小才是内存块的分配区域
     return (struct mem_block *)((uint32_t)a + sizeof(struct arena) + index * a->type->block_size);
}

//返回内存块b所在的arena地址
struct arena * block2arena(struct mem_block * b)
{
    return (struct arena*)((uint32_t)b&0xfffff000);
}

//在堆中申请size字节内存
void * kmalloc(uint32_t size)
{
    enum phy_pool_flag pf; //用来标识在内核物理内存池还是用户物理内存池分配内存
    uint32_t pool_size; 
    struct mutex * lock; 
    struct mem_block_type * mem_type_table;
    
    //判断是内核线程还是用户进程申请内存
    struct task_struct  *cur_task =get_current_thread();
    if(cur_task->pdt_base_vir== NULL)
    {
        //内核线程
        pf = PHY_POOL_KERNEL;
        pool_size = phy_pool_kernel.size;
        lock = &phy_pool_kernel.lock;
        mem_type_table = mem_type_table_k;

    }else{
        pf = PHY_POOL_USER;
        pool_size = phy_pool_user.size;
        lock = &phy_pool_user.lock;
        mem_type_table = cur_task->mem_type_table_u;
    }

    //判断形参size是否在内存池大小范围内
    if(size<=0 || size>pool_size)
    {
        return NULL;
    }
    struct arena * a;
    struct mem_block * b;

    mutex_lock(lock);
    //超过1024字节就分配一整页
    if (size>1024)
    {
        //向上取整需要分配的页数，因为加上了arena的大小后再取整，所以不会存在申请4095字节只分配一页，然后area占了12字节导致不够用的情况
        uint32_t pg_count = DIV_ROUND_UP(size+sizeof(struct arena),PG_SIZE);
        a = malloc_page(pf,pg_count);

        if (a != NULL)
        {
            memset(a,0,pg_count*PG_SIZE);//清0
            //对于分配的大块内存,将type置为NULL,block_count置为页数,large置为true
            a->type=NULL;//不属于那7种类型之一
            a->block_count=pg_count;
            a->large=true;
            mutex_unlock(lock);
            return (void*)(a+1);// 跨过arena大小，把剩下的内存返回
        }else{
            mutex_unlock(lock);

            return NULL;
        }
    }else{
        // 若申请的内存小于等于1024,可在mem_type_table中根据需要的内存块大小去找是哪种类型，然后去看它的内存块链表
        int type_index;
        for ( type_index = 0; type_index <TYPE_COUNT; type_index++)
        {
            if(size<=mem_type_table[type_index].block_size)// 从小往大后,找到后退出
            {
                break;
            }
        }

         //如果内存分配表中某种类型的内存块已经没有可用的了,就创建新的arena提供内存块
        if (list_empty(&mem_type_table[type_index].free_list_head))
        {
            a= malloc_page(pf,1);//小于1024个字节的内存申请一页就够用了
            if (a == NULL)
            {
                mutex_unlock(lock);
                return NULL;
            }
            
            memset(a,0,PG_SIZE);
            a->type=&mem_type_table[type_index];
            a->large = false;
            a->block_count=mem_type_table[type_index].blocks_per_arena;

            //开始将arena拆分成内存块,并添加到内存块描述符的free_list_head中,注意区别前面大于1024字节的内存块没有加入链表
            uint32_t block_index;
            struct task_struct * cur_task =get_current_thread();
            enum intr_status old_status = intr_disable(); //这句一定要加上或者将b定义为静态即static struct mem_block * b;
            for(block_index=0 ; block_index < mem_type_table[type_index].blocks_per_arena ; block_index++)
            {
                b = arena2block(a,block_index);
                assert(!list_exist(&mem_type_table[type_index].free_list_head,&b->block_entry));//不在空闲队列中
                list_append(&b->block_entry,&mem_type_table[type_index].free_list_head);
            }
            intr_restore_status(old_status);
        }

        //开始分配内存块 
        struct list_head * entry=list_pop(&mem_type_table[type_index].free_list_head);
        b =container_of(entry,struct mem_block,block_entry);
        memset(b,0,mem_type_table[type_index].block_size);

        a=block2arena(b);
        a->block_count--;
        mutex_unlock(lock);
        return (void*)b;
    }

}

//回收paddr到物理内存池
void phy_addr_free(uint32_t paddr)
{
    struct phy_pool * mem_pool;
    uint32_t position;
    if (paddr >= phy_pool_user.ptr)//用户物理内存池
    {
        mem_pool = &phy_pool_user;
        position =(paddr - phy_pool_user.ptr)/PG_SIZE;
    }else{
        mem_pool = &phy_pool_kernel;
        position =(paddr -phy_pool_kernel.ptr)/PG_SIZE;
    }
    bitmap_set(&mem_pool->bmap,position,0);//将位图中该位清0

}

//将虚拟地址vaddr对应的pte的P位清0，则表示取消了物理地址和该vaddr的映射
void mapping_cancel(uint32_t vaddr)
{
    uint32_t * pte_addr =pte_get(vaddr);//获取vaddr对应的pte物理地址
    *pte_addr &= ~0x1; //pte的P位清0
    asm volatile("invlpg %0"::"m"(vaddr));//更新tlb
}

//回收vaddr到虚拟内存池
void vir_addr_free(enum phy_pool_flag pf, void* vaddr, uint32_t pg_count)
{
    uint32_t position_start=0;
    uint32_t i=0;
    if (pf == PHY_POOL_KERNEL)
    {
        position_start = ((uint32_t)vaddr - vir_pool_kernel.ptr) /PG_SIZE;

        while(i<pg_count)
        {
            bitmap_set(&vir_pool_kernel.bmap,position_start+i,0);//将位图中该位清0 
            i++;   
        }
    }else{
        struct task_struct * cur_task =get_current_thread();
        vir_pool * vir_pool_user=&cur_task->vir_pool_user; 
        position_start= ((uint32_t)vaddr-vir_pool_user->ptr) /PG_SIZE;
        while(i < pg_count)
        {
            bitmap_set(&vir_pool_user->bmap,position_start+i,0);//将位图中该位清0 
            i++;  
        }
    }
}

//释放以vaddr为起始地址的pa_count个物理页
void free_page(enum phy_pool_flag pf,void * vaddr,uint32_t pg_count)
{
    assert((uint32_t)vaddr%PG_SIZE ==0 && pg_count>0);
    uint32_t vaddr_start=(uint32_t)vaddr;
    struct phy_pool * mem_pool = pf == PHY_POOL_KERNEL? &phy_pool_kernel : &phy_pool_user;
    mutex_lock(&mem_pool->lock);

    uint32_t i=0;
    uint32_t paddr;
    while(i < pg_count)
    {
        paddr=to_phy_addr(vaddr);
        //确保物理地址在范围内
        assert(paddr % PG_SIZE ==0 && paddr>= mem_pool->ptr);
        //1、先将对应的物理页归还到内存池
        phy_addr_free(paddr);
        //2、从页表中清除此虚拟地址所在的页表项 pte
        mapping_cancel(vaddr);
        
        vaddr+= PG_SIZE;//这句一定要记得加上
        i++;
    }
    //3、清空虚拟地址位图中的相应位
    vir_addr_free(pf,vaddr_start,pg_count);//注意这里用的是vaddr_start，不是vaddr

    mutex_unlock(&mem_pool->lock);
}

//回收vaddr对应的堆内存，其中vaddr必须是kmalloc()的返回值，不然是没有arena的，不能通过这个函数释放，应该使用free_page()
void kfree(void * vaddr)
{
    assert(vaddr!=NULL);
    enum phy_pool_flag pf;
    mutex * lock;
    struct task_struct * cur_task =get_current_thread();
    if (cur_task->pdt_base_vir == NULL)
    {
        pf=PHY_POOL_KERNEL;
        lock=&phy_pool_kernel.lock;
    }else{
        pf=PHY_POOL_USER;
        lock=&phy_pool_user.lock;
    }
    
    mutex_lock(lock);
    struct mem_block *b =vaddr;
    struct arena * a=block2arena(b);

    if (a->type==NULL&&a->large==true)//大于1024字节的内存
    {
        free_page(pf,a,a->block_count);//整页整页的释放，注意这里没有重新放回内存分配表中
    }else{
        list_append(&b->block_entry,&a->type->free_list_head);//重新放回内存分配表中
        a->block_count++;
        if (a->block_count == a->type->blocks_per_arena)//此 arena 中的内存块是否都是空闲
        {
            uint32_t i;
            for(i=0;i<a->type->blocks_per_arena;i++)
            {
                struct mem_block * block=arena2block(a,i);
                //从内存分配表的链表中将这种类型的内存块删除，注意这里只是从链表中删除，真正的回收在下面的page_free()
                list_del(&block->block_entry); 
            }
            free_page(pf,a,1);//将arena所在的页给回收掉
        }
    }   

    mutex_unlock(lock);
}

