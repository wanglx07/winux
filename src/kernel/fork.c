#include <types.h>
#include <string.h>
#include <memory.h>
#include <thread.h>
#include <process.h>
#include <mutex.h>
#include <file.h>
#include <inode.h>
#include <assert.h>
#include <list.h>

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))
extern void syscall_exit(void);//在handler.asm中定义的

//注意汇编形式的syscall_exit，和c语言形式的c_syscall_exit的区别
//汇编形式的syscall_exit需要addl $12,%%esp
//而c语言形式的syscall_exit则需要addl $16,%%esp，这是因为调用者
//调用syscall_exit会把调用者返回地址压进栈中
// void syscall_exit()
// {
//     asm volatile ("addl $16,%%esp\n\t"
//                 "popa \n\t" 
//                 "popl %%gs \n\t"
//                 "popl %%fs \n\t"
//                 "popl %%es \n\t"
//                 "popl %%ds \n\t"
//                 "iret \n\t"
//                 : : );
// }

//将父进程的用户空间中有数据的页拷贝到子进程中
void copy_mem(struct task_struct * child_task,struct task_struct *parent_task,void * buf)
{
    uint8_t * bmap_ptr=parent_task->vir_pool_user.bmap.ptr;
    uint32_t bmap_len=parent_task->vir_pool_user.bmap.length;
    uint32_t vaddr_start=parent_task->vir_pool_user.ptr;
    uint32_t byte_idx ;
    uint32_t bit_idx ;
    uint32_t vaddr;

    for(byte_idx=0;byte_idx<bmap_len;byte_idx++)
    {
        if (*(bmap_ptr+byte_idx))//说明这个页有数据
        {
            for (bit_idx=0;bit_idx<8; bit_idx++)
            {
                if ((1 << bit_idx) & *(bmap_ptr+byte_idx)) {
                    vaddr = vaddr_start + (byte_idx*8 + bit_idx)* PG_SIZE;//算出有数据的页的虚拟地址
                    //因为每个进程都有各自的页目录表，不能访问彼此的空间，需将父进程的数据由用户空间拷贝到内核空间，这样子进程才能访问到
                    memcpy(buf,vaddr,PG_SIZE);

                    //切换到子进程的页目录
                    refresh_cr3_tss(child_task);

                    //在子进程中为这个虚拟地址申请物理页并做好映射，注意这里调用的是apply_a_page_no_bmap()，因为父进程已经申请了空闲位存数据，
                    //子进程已经拷贝了父进程的虚拟内存池位图，子进程的不需要再从虚拟内存池位图中申请空闲位了
                    apply_a_page_no_bmap(PHY_POOL_USER,vaddr);

                    //将内核空间的父进程数据拷贝到子进程用户空间,注意此时cr3指向子进程页目录表，所以vaddr指向子进程用户空间
                    memcpy(vaddr,buf,PG_SIZE);

                    //切换回父进程的页目录表，继续拷贝
                    refresh_cr3_tss(parent_task);
                }
            }
        }
    }

}

void build_child_stack(struct task_struct * child_task)
{
    //修改子进程的系统调用栈syscall_stack（在thread.h中），使得返回的pid为0。我们在copy_process()中已经拷贝了父进程的pcb，
    //包含了系统调用栈,注意这里使用的不是intr_stack，intr_stack是其它中断使用的
    struct syscall_stack * syscall_stack=(struct syscall_stack *)((uint32_t)child_task + PG_SIZE - sizeof(struct syscall_stack));
    syscall_stack->eax=0;

    //到时候子进程是由switch_to()进行调度的，所以要构造返回的环境（注意不是构造thread_stack）。
    //将其构建在紧临intr_stack之下的空间，我们设置它的返回地址是中断退出位置，不需要参数
    uint32_t * eip=(uint32_t *) syscall_stack-1;
    uint32_t * esi =(uint32_t *) syscall_stack-2;
    uint32_t * edi =(uint32_t *) syscall_stack-3;
    uint32_t * ebx =(uint32_t *) syscall_stack-4;
    uint32_t * ebp =(uint32_t *) syscall_stack-5;

    *eip = syscall_exit; //设置返回地址，跳到child_process_entry直接操作中断栈
    *esi=*edi=*ebx=*ebp=0;

    child_task->sp=ebp;//将sp设置为ebp，到时候switch_to()中改变esp的时候，就可以指向child_task的ebp，接着弹出ebx...eip
}

// 复制进程
int32_t copy_process(struct task_struct * child_task,struct task_struct *parent_task)
{
    /*****************************1 拷贝父进程的pcb****************************************/

    //因为forK()到时候是通过系统调用进来的，系统调用又是通过中断产生，到时候上下文(包含了返回地址)保存在父进程pcb的内核栈中
    memcpy(child_task,parent_task,PG_SIZE);
    strcat(child_task->name,"_child");
    child_task->pid=alloc_pid();
    child_task->status = TASK_READY;
    child_task->ticks= child_task->priority;
    child_task->elapsed_ticks=0;
    child_task->parent_pid = parent_task->pid;
    mem_type_table_init(child_task->mem_type_table_u);

    /******************************2 拷贝父进程的虚拟内存池位图*********************************/

    //位图长度(单位字节)
    uint32_t bmap_len=(0xc0000000 - USER_VIR_ADDR_START) / PG_SIZE / 8;
    //转换为页单位，因为需要申请内存来存放位图
    uint32_t bmap_len_pg=DIV_ROUND_UP(bmap_len , PG_SIZE);
    //位图存放在这里，为什么要在内核的内存池申请呢？就像process_func里面还要apply_a_page，如果内核拿不到位图，那么就无法进行内存管理
    void * vaddr=apply_kernel_mem(bmap_len_pg);
    if (vaddr == NULL)
    {
        return -1;
    }
    memcpy(vaddr,parent_task->vir_pool_user.bmap.ptr,bmap_len_pg*PG_SIZE);
    child_task->vir_pool_user.bmap.ptr = vaddr; 
    child_task->vir_pool_user.bmap.length = bmap_len;
    mutex_init(&child_task->vir_pool_user.lock);//初始化锁
    
    /******************************3 为子进程创建新的页目录表*********************************/

    child_task->pdt_base_vir=pdt_create();
    if (child_task->pdt_base_vir== NULL)
    {
        free_page(PHY_POOL_KERNEL,vaddr,bmap_len_pg);
        return -1;
    }

    /******************************4 拷贝父进程0~3G的代码和数据*********************************/
    
    void* buf = apply_kernel_mem(1);//内核缓冲区,作为父进程用户空间的数据复制到子进程用户空间的中转
    if (buf==NULL)
    {
        free_page(PHY_POOL_KERNEL,vaddr,bmap_len_pg);
        return -1;
    }
    copy_mem(child_task,parent_task,buf);

    /*************************5 构建子进程thread_stack和修改返回值****************************/

    build_child_stack(child_task);
    

    /*****************6 如果父进程中有文件是打开的，则将对应文件的打开次数增1********************/

    int fd_idx;
    uint32_t f_idx;
    for ( fd_idx = 3; fd_idx <MAX_FILES_PER_PROC_OPEN; fd_idx++)
    {
        f_idx=child_task->fd_table[fd_idx];//因为子进程已经拷贝了父进程的资源，所以这里用child_task
        if (f_idx!=-1)
        {
            if(is_pipe(fd_idx))
            {
                file_table[f_idx].fd_pos++;
            }else{
                file_table[f_idx].fd_inode->i_open_count++;
            }
        }
    }

    free_page(PHY_POOL_KERNEL,buf,1);
    return 0;
}

//fork出用户进程的子进程,内核线程不可直接调用
pid_t sys_fork()
{
    struct task_struct * parent_task=get_current_thread();
    struct task_struct * child_task =apply_kernel_mem(1);
    if (child_task== NULL)
    {
        return -1;
    }

    if (copy_process(child_task,parent_task)==-1)
    {
        return -1;
    }

    assert(!list_exist(&ready_list_head,&child_task->general_entry));//不在就绪队列中
    list_append(&child_task->general_entry,&ready_list_head);//加入就绪队列
    assert(!list_exist(&total_list_head,&child_task->total_entry));//不在总队列中
    list_append(&child_task->total_entry,&total_list_head);//加入总队列

    return child_task->pid;//返回子进程pid
}

