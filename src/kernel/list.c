#include <list.h>
#include <interrupt.h>
//如果已知是一个struct list_head变量，则使用如下函数初始化
void INIT_LIST_HEAD(struct list_head *list)
{
    list->prev=list;
    list->next=list;
}

//在prev和next之间插入new
void list_insert(struct list_head *new,
                            struct list_head *prev,
                            struct list_head*next)
{   
    // asm volatile("cli");//关中断
    enum intr_status old_status=intr_disable();
    next->prev=new;
    new->next=next;
    new->prev=prev;
    prev->next=new;
    intr_restore_status(old_status);
    // asm volatile("sti");
}

//添加元素到链表头部
void list_push(struct list_head* new ,struct list_head* head)
{
    list_insert(new,head,head->next);
}


//添加元素到链表尾部
void list_append(struct list_head* new ,struct list_head* head)
{
    list_insert(new,head->prev,head);
}

//从链表中删除某个元素
void list_del(struct list_head * entry)
{
    // asm volatile("cli");
    enum intr_status old_status=intr_disable();
    entry->prev->next=entry->next;
    entry->next->prev=entry->prev;
    entry->prev=NULL;
    entry->next=NULL;
    intr_restore_status(old_status);
    // asm volatile("sti");
}

//弹出第一个节点
struct list_head * list_pop(struct list_head * head)
{
    struct list_head * entry=head->next;
    list_del(entry);
    return entry;
}

//在链表中查找是否存在某个节点,成功返回0，失败返回1
int list_exist(struct list_head *head,struct list_head *entry)
{
    struct list_head *p;
    list_for_each(p,head)
    {
        if (p==entry)
        {
            return 1;
        }
    }
    return 0;
}

//为空则返回1，否则返回0
int list_empty(const struct list_head *head)
{
   return head->next==head;  
} 

//把链表中的每个节点和 arg 传给回调函数 func,
//找到符合条件的返回节点指针，否则返回NULL
struct list_head * list_find(struct list_head *head, func_ptr func,void* arg )
{
    if( list_empty(head))
    {
        return NULL;
    }
    struct list_head *p;
    list_for_each(p,head)
    {
        if (func(p,arg))
        {
            return p;
        }
    }
    return NULL;
}

//返回链表长度，不包括头节点
int list_len(const struct list_head *head)
{
    struct list_head * p;
    int i=0;
    list_for_each(p,head)
    {
        i++;
    }
    return i;
}