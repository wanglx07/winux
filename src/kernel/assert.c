#include <assert.h>
#include <printf.h>

void __panic(const char * filename,char * func,int line,char * e)
{
    asm volatile("cli");
    printf("error occurred at %s : function=%s line=%d condition= %s",filename,func,line,e);
    while(1);
}
