#include <stdarg.h>
#include <printf.h>
#include <syscall.h>
#include <string.h>



extern int vsprintf (char *buf, const char *fmt, va_list args);


int printf(const char *fmt,...)
{
	va_list args;			// va_list 实际上是一个字符指针类型。
	int i;

	va_start (args, fmt);		// 参数处理开始函数。在（include/stdarg.h）
	char buf[1024]={0};//不能将buf定义成全局静态变量，否则打印会有问题，可以和printk的buf对比下
	vsprintf (buf, fmt, args);	// 使用格式串fmt 将参数列表args 输出到buf 中,返回值i 等于输出字符串的长度

	va_end (args);		// 参数处理结束函数。

    i=write(1,buf,strlen(buf)); //调用write系统调用进行打印

	return i;			// 返回字符串长度。
}