#include <process.h>
#include <tss.h>
#include <memory.h>
#include <string.h>
#include <assert.h>
#include <interrupt.h>

#define PG_SIZE 4096
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))


void process_func(void* process_entry)
{
    struct task_struct * cur_task = get_current_thread();
    cur_task->sp += sizeof(struct thread_stack);
    struct interrupt_stack* stack = (struct interrupt_stack*) cur_task->sp;
    stack->vector_num=0;
    stack->edi=0;
    stack->esi=0;
    stack->ebp=0;
    stack->esp_manual=0;
    stack->ebx = 0;
    stack->edx = 0;
    stack->ecx = 0 ;
    stack->eax = 0;

    stack->gs = 0;	// 用户态用不上,直接初始为0
    stack->ds = stack->es = stack->fs = USER_DATA_SEG_SELECTOR;//操作系统不允许用户进程访问显存,所以fs也置为数据段
    stack->eip = process_entry;	 // 待执行的用户程序地址
    stack->cs = USER_CODE_SEG_SELECTOR;
    stack->eflags = (1<<9)| (1<<1);//IOPL=0,IF=1,MBS=1

    stack->esp = (void*)((uint32_t)apply_a_page(PHY_POOL_USER, 0xc0000000-PG_SIZE) + PG_SIZE) ;//申请一个进程的栈，注意区分thread_stack和interrupt_stack
    stack->ss = USER_DATA_SEG_SELECTOR; 
    asm volatile ("movl %0, %%esp \n\t"
                    "addl $4,%%esp\n\t" /*这句一定要写上*/
                   "popa \n\t" 
                   "popl %%gs \n\t"
                   "popl %%fs \n\t"
                   "popl %%es \n\t"
                   "popl %%ds \n\t"
                   "addl $4,%%esp\n\t"
                   "iret \n\t"
                    : 
                    : "r" (stack));
                    //    "jmp . \n\t" //停顿在某行
}

//进程虚拟地址空间初始化
void user_vir_pool_init(struct task_struct * task)
{   
    //虚拟内存起始地址
    task->vir_pool_user.ptr = USER_VIR_ADDR_START;
    //位图长度(单位字节)
    uint32_t bmap_len=(0xc0000000 - USER_VIR_ADDR_START) / PG_SIZE / 8;
    //转换为页单位，因为需要申请内存来存放位图
    uint32_t bmap_len_pg=DIV_ROUND_UP(bmap_len , PG_SIZE);
    //位图存放在这里，为什么要在内核的内存池申请呢？就像process_func里面还要apply_a_page，如果内核拿不到位图，那么就无法进行分配
    task->vir_pool_user.bmap.ptr = apply_kernel_mem(bmap_len_pg); 
    task->vir_pool_user.bmap.length = bmap_len;
    bitmap_init(&task->vir_pool_user.bmap);//初始化位图
    mutex_init(&task->vir_pool_user.lock);//初始化锁
}

// 创建进程页目录表,其实就是将内核的页目录复制到自己这里
uint32_t * pdt_create()
{
    uint32_t * pdt_base_vir=apply_kernel_mem(1);//申请一个页用来存放页目录
    if (pdt_base_vir==NULL)
    {
        printk("process pdt error\r\n");  
        return NULL;  
    }
    
    //将内核的页目录复制到进程的页目录中
    memcpy((uint32_t*)((uint32_t)pdt_base_vir+768*4),(uint32_t*)(0xfffff000+768*4),256);

    //将进程页目录表的最后一项改为自己页目录表的地址，之前里面填的是内核页目录表的地址
    uint32_t pdt_base_phy=to_phy_addr((uint32_t)pdt_base_vir);
    pdt_base_vir[1023] = pdt_base_phy | 0b111;//US=1(允许0、1、2、3特权级访问),RW=1(可读可写),P=1

    return pdt_base_vir;
}

void process_start(char *process_name,void *process_entry)
{
    //内核负责进程的调度，所以内核要访问得到进程的数据结构pcb,因此要在内核内存池中申请
    struct task_struct * task= apply_kernel_mem(1);//pcb和虚拟地址空间的位图都是在内核的内存池中
    thread_init(task,process_name,DEFAULT_PRIO); 
    //因为进程有自己的地址空间，所以要有对应的虚拟内存池位图和页目录表
    //进程页目录表初始化，主要就是为0xc0000000以上的虚拟空间建立对应的页目录（其实就是将内核的页目录复制过来），那样才能访问到内核提供的东西，不然地址转换都转换不了
    task->pdt_base_vir=pdt_create();
    //进程虚拟地址空间初始化,线程不需要，所以thread_init里面没有对虚拟内存池进行初始化，这里是进程，单独提出来进行
    //主要就是为0x8048000-0xc0000000这段虚拟空间建立对应的位图
    //前面这几步到这里都是对task_struct进行设置
    user_vir_pool_init(task);
    //用户进程才需要独立初始化内存块分配表，内核线程的话用的是内核的内存块分配表，在memory.c中初始化
    mem_type_table_init(task->mem_type_table_u);
    thread_create(task,process_func,process_entry);
 
    enum intr_status old_status=intr_disable();
    assert(!list_exist(&ready_list_head,&task->general_entry));//不在就绪队列中
    list_append(&task->general_entry,&ready_list_head);//加入就绪队列
    assert(!list_exist(&total_list_head,&task->total_entry));//不在总队列中
    list_append(&task->total_entry,&total_list_head);//加入总队列
    intr_restore_status(old_status);
}

//重新设置cr3中页目录的地址和tss中的栈地址
void refresh_cr3_tss(struct task_struct* next_task)
{
    /*******************1、设置CR3************************/
    struct task_struct * cur=get_current_thread();
    // 若为内核线程,使用内核页目录表，物理地址为0x100000; 如果是用户进程，则使用它自己的页目录表
   uint32_t pdt_base_phy = 0x100000; 
   if (next_task->pdt_base_vir != NULL)	{  
      pdt_base_phy = to_phy_addr((uint32_t)next_task->pdt_base_vir);
   }
   // 更新页目录寄存器cr3,使新页目录表生效 
   asm volatile ("movl %0, %%cr3" : : "r" (pdt_base_phy) );
    
   /*******************2、设置tss************************/
    //如果新的任务是个用户进程则需要更新tss为它准备好0特权级的栈，内核线程用不上，所以不用重新设置tss
    if (next_task->pdt_base_vir)	{    
        refresh_tss(next_task);
   }
}