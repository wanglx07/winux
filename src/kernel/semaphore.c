#include <semaphore.h>
#include <interrupt.h>
#include <list.h>
#include <assert.h>
#include <thread.h>

void sema_init(struct semaphore* sema, int val)
{
    sema->count=val;
    INIT_LIST_HEAD(&sema->waiters_head);
}

void down(struct semaphore* sema)
{
    enum intr_status old_status=intr_disable();//关闭中断，保证原子操作
    while(sema->count==0) //表明别人已经持有了锁
    {
        struct task_struct * task =get_current_thread();
        list_append(&task->general_entry,&sema->waiters_head);//加入到锁的等待队列
        thread_block(TASK_BLOCKED); //这句不能放到list_append前面，因为thread_blocked里面有schedule ,会导致lock->waiters_head可能为0
    }

    assert(sema->count==1);
    sema->count--;
    assert(sema->count==0);
    intr_restore_status(old_status);
}

void up(struct semaphore* sema)
{
    enum intr_status old_status=intr_disable();//关闭中断，保证原子操作
    assert(sema->count==0);
    sema->count++;//这个一定要放在thread_unblock前面，不然唤醒了下一个进程，此时lock->count还是0还得继续睡眠，是不对的，因为本意是可以使用锁了，不用睡眠了
    assert(sema->count==1);
     if(!list_empty(&sema->waiters_head))
    {
        //唤醒互斥体等待队列上的下一个线程
        struct list_head * entry = list_pop(&sema->waiters_head);
        struct task_struct * task = container_of(entry,struct task_struct,general_entry);
        thread_unblock(task);
    }
    intr_restore_status(old_status);
}