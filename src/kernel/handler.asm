[bits 32]
;-------------------------------------------
section .data
;-------------------------------------------
output db "interrupt occur!", 0x0a,0x0d,0
global asm_handler_table
asm_handler_table:

;---------------------------------------------
section .text 
;---------------------------------------------
%define ERROR_CODE nop		 ; 若在相关的异常中cpu已经自动压入了错误码,为保持栈中格式统一,这里不做操作.
%define ZERO push 0		 ; 若在相关的异常中cpu没有压入错误码,为了统一栈中格式,就手工压入一个0

extern printk			 ;声明外部函数
extern c_handler_table
extern sys_call_table ;c语言版系统调用表，在lib/usr/syscall.c中

%macro MAKE_HANDLER 2
section .text
handler_entry_%1:		 ; 每个中断处理程序都要压入中断向量号,所以一个中断类型一个中断处理程序，自己知道自己的中断向量号是多少
   %2 ;这句千万不能少，不然时钟中断只打印一次
   push ds 
   push es
   push fs
   push gs
   pushad 

   mov al,0x20                   ; 中断结束命令EOI
   out 0xa0,al                   ; 向从片发送
   out 0x20,al                   ; 向主片发送

   push %1 
   call [c_handler_table+%1*4]
   add esp,4 ;弹出中断号
   
   popad
   pop gs
   pop fs
   pop es
   pop ds
   add esp,4 ; 跨过error_code
   iret 

section .data
   dd    handler_entry_%1	 
%endmacro
   
global asm_syscall_handler
asm_syscall_handler:
   push ds 
   push es
   push fs
   push gs
   pushad 

   push edx ;按照c语言的风格要求从右往左将参数入栈 
   push ecx 
   push ebx 
   call [sys_call_table+eax*4] ;根据eax中的功能号在系统调用表中调用不同的函数从而实现一个中断多种功能
   add esp,12

   mov [esp+7*4],eax ;为了避免popad的时候将eax中的返回值覆盖，将eax的值保存在栈中eax的位置，后面popad的时候eax中存的就是返回值

   popad
   pop gs
   pop fs
   pop es
   pop ds
   iret

global syscall_exit;在fork.c中用到
syscall_exit:
   add esp,12
   popad
   pop gs
   pop fs
   pop es
   pop ds
   iret

MAKE_HANDLER 0x00,ZERO
MAKE_HANDLER 0x01,ZERO
MAKE_HANDLER 0x02,ZERO
MAKE_HANDLER 0x03,ZERO 
MAKE_HANDLER 0x04,ZERO
MAKE_HANDLER 0x05,ZERO
MAKE_HANDLER 0x06,ZERO
MAKE_HANDLER 0x07,ZERO 
MAKE_HANDLER 0x08,ERROR_CODE
MAKE_HANDLER 0x09,ZERO
MAKE_HANDLER 0x0a,ERROR_CODE
MAKE_HANDLER 0x0b,ERROR_CODE 
MAKE_HANDLER 0x0c,ZERO
MAKE_HANDLER 0x0d,ERROR_CODE
MAKE_HANDLER 0x0e,ERROR_CODE
MAKE_HANDLER 0x0f,ZERO 
MAKE_HANDLER 0x10,ZERO
MAKE_HANDLER 0x11,ERROR_CODE
MAKE_HANDLER 0x12,ZERO
MAKE_HANDLER 0x13,ZERO 
MAKE_HANDLER 0x14,ZERO
MAKE_HANDLER 0x15,ZERO
MAKE_HANDLER 0x16,ZERO
MAKE_HANDLER 0x17,ZERO 
MAKE_HANDLER 0x18,ERROR_CODE
MAKE_HANDLER 0x19,ZERO
MAKE_HANDLER 0x1a,ERROR_CODE
MAKE_HANDLER 0x1b,ERROR_CODE 
MAKE_HANDLER 0x1c,ZERO
MAKE_HANDLER 0x1d,ERROR_CODE
MAKE_HANDLER 0x1e,ERROR_CODE
MAKE_HANDLER 0x1f,ZERO 
MAKE_HANDLER 0x20,ZERO ;时钟中断
MAKE_HANDLER 0X21,ZERO ;键盘中断
MAKE_HANDLER 0x22,ZERO	;级联用的
MAKE_HANDLER 0x23,ZERO	;串口2对应的入口
MAKE_HANDLER 0x24,ZERO	;串口1对应的入口
MAKE_HANDLER 0x25,ZERO	;并口2对应的入口
MAKE_HANDLER 0x26,ZERO	;软盘对应的入口
MAKE_HANDLER 0x27,ZERO	;并口1对应的入口
MAKE_HANDLER 0x28,ZERO	;实时时钟对应的入口
MAKE_HANDLER 0x29,ZERO	;重定向
MAKE_HANDLER 0x2a,ZERO	;保留
MAKE_HANDLER 0x2b,ZERO	;保留
MAKE_HANDLER 0x2c,ZERO	;ps/2鼠标
MAKE_HANDLER 0x2d,ZERO	;fpu浮点单元异常
MAKE_HANDLER 0x2e,ZERO	;硬盘
MAKE_HANDLER 0x2f,ZERO	;保留