#include <stdarg.h>
#include <sprintf.h>

extern int vsprintf (char *buf, const char *fmt, va_list args);

uint32_t sprintf(char * buf,const char* fmt,...)
{
	int i;
	va_list args;			// va_list 实际上是一个字符指针类型。
	va_start(args, fmt);		// 参数处理开始函数。在（include/stdarg.h）
	i=vsprintf(buf, fmt, args);	// 使用格式串fmt 将参数列表args 输出到buf 中,返回值i 等于输出字符串的长度
	va_end (args);		// 参数处理结束函数。
	return i;			// 返回字符串长度。
}



