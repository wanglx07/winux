#include <mutex.h>
#include <list.h>
#include <thread.h>
#include <assert.h>

//初始化 mutex
void mutex_init(mutex *lock)
{
    sema_init(&lock->sema,1);//表示未被人持有
    lock->holder=NULL;
    lock->repeat=0;
}

//获取 mutex，也就是给 mutex 上锁。如果获取不到就进休眠
void mutex_lock( mutex *lock)
{
    struct task_struct * task=get_current_thread();
    if(lock->holder != task){
        down(&lock->sema);
        lock->holder =task;//锁的拥有者为当前线程
    }else{
        lock->repeat++;
    }
}

//释放 mutex，也就给 mutex 解锁
void mutex_unlock( mutex *lock)
{
    //这个要放在intr_disable()前面，否则当出现mutex嵌套的话会出问题 ，比如说现在repeat=1,第一次获取old_status=intr_disable()=1，
    //repeat--,再次进来再执行一次old_status=intr_disable()，此时old_status=0，但是正确的应该是我们第一次获取的值1；
    //那么将这个if语句放在intr_disable()前面此时并没有关中断，控制并发的语句，会不会执行到if (lock->repeat > 0)语句切换到
    //下一个线程，新线程使lock->repeat--到0，然后再切回旧线程的时候，旧线程接着执行lock->repeat--导致lock->repeat<0从而出现问题
    //这个不用担心，只有持有锁的线程才会执行到mutex_unlock()，其它线程在获取锁的时候已经阻塞在锁的等待队列上面了，根本不会执行
    //到mutex_unlock()，因为还没拿到锁，何谈解锁呢
    if (lock->repeat > 0)
    {
        lock->repeat--;
        return;
    }
    lock->holder =NULL;//锁的拥有者重新置为NULL，表示锁没人使用
    up(&lock->sema);
}