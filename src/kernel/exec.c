#include <types.h>
#include <memory.h>
#include <fs.h>
#include <thread.h>
#include <string.h>
#include <file.h>

extern void syscall_exit(void);//在handler.asm中定义的

typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

// 32位elf头 
struct Elf32_Ehdr {
   unsigned char e_ident[16];
   Elf32_Half    e_type;
   Elf32_Half    e_machine;
   Elf32_Word    e_version;
   Elf32_Addr    e_entry; //程序入口地址
   Elf32_Off     e_phoff; //程序头表在文件中的偏移
   Elf32_Off     e_shoff;
   Elf32_Word    e_flags;
   Elf32_Half    e_ehsize;
   Elf32_Half    e_phentsize; //每个程序头的大小
   Elf32_Half    e_phnum; //多少个程序头
   Elf32_Half    e_shentsize;
   Elf32_Half    e_shnum;
   Elf32_Half    e_shstrndx;
};

// 程序头表Program header.就是段描述头 
struct Elf32_Phdr {
   Elf32_Word p_type;	// 见下面的enum segment_type
   Elf32_Off  p_offset; //本段在文件内的起始偏移字节
   Elf32_Addr p_vaddr; //段要被加载到的虚拟地址
   Elf32_Addr p_paddr;
   Elf32_Word p_filesz;
   Elf32_Word p_memsz;
   Elf32_Word p_flags;
   Elf32_Word p_align;
};

// 段类型 
enum segment_type {
   PT_NULL,            // 忽略
   PT_LOAD,            // 可加载程序段
   PT_DYNAMIC,         // 动态加载信息 
   PT_INTERP,          // 动态加载器名称
   PT_NOTE,            // 一些辅助信息
   PT_SHLIB,           // 保留
   PT_PHDR             // 程序头表
};

//将文件描述符fd指向的文件中,偏移为p_offset,大小为p_filesz的段加载到虚拟地址为p_vaddr的内存
bool load_segment(uint32_t fd,uint32_t p_offset,uint32_t p_filesz,uint32_t p_vaddr)
{
    
    /******************1 计算段需要占用的总页数*********************/
    uint32_t available_in_first=PG_SIZE - (p_vaddr & 0x00000fff);// 加载到内存后,段在第一个页中可使用的字节大小   
    uint32_t occupy_pages = 0;//段占用的总页数

    if (p_filesz>available_in_first)//第一个页剩下的容量装不下这个段
    {
        uint32_t left_size = p_filesz - available_in_first; //剩下待处理的字节数
        occupy_pages = DIV_ROUND_UP(left_size,PG_SIZE)+1; //注意要加上1,1指的是第一个页
    }else{// 否则一个页就够了
        occupy_pages=1;
    }

    /**************2 为段分配内存****************************/
    uint32_t i=0;
    uint32_t page_vaddr=p_vaddr & 0xfffff000;//页地址
    while( i < occupy_pages)
    {
        uint32_t * pde = pde_get(page_vaddr);
        uint32_t * pte = pte_get(page_vaddr);

        //只有页不存在才分配，如果旧程序已经申请过页了直接覆盖旧程序的内容就行了。通过pde和pte
        //判断页有没有分配过，而且要先判断pde，否则pde若不存在会导致判断pte时缺页异常
        if (!(*pde & 0x1) || !(*pte & 0x1))
        {
            if(apply_a_page(PHY_POOL_USER,page_vaddr)==NULL)
            {
                return false;
            }
        }

        page_vaddr += PG_SIZE;
        i++;
    }

    /*****************3 将段加载进来********************/
    sys_lseek(fd,p_offset,SEEK_SET);//将文件指针定位到段在文件中的偏移地址
    sys_read(fd,p_vaddr,p_filesz);
    return true;
}

int32_t load_program(const char * pathname)
{
    int32_t ret =-1;
    //1 获取文件句柄
    int32_t fd = sys_open(pathname,O_RDONLY);
    if (fd ==-1 )
    {
        return -1;
    }

    //2 读取elf头
    struct Elf32_Ehdr elf_header;
    int32_t size= sys_read(fd , &elf_header ,sizeof(struct Elf32_Ehdr));
    if (size != sizeof(struct Elf32_Ehdr))
    {
        goto close_file;
    }

    //3 校验elf头
    //e_ident[0~3]=7f E L F
    //e_ident[4]=1 //32位
    //e_ident[5]=1 //小端
    //e_ident[6]=1 //版本，默认为1
    //e_type=2 //可执行文件
    //e_machine =3 //EM_386，即80386体系
    //e_version =1 //当前版本，为0则非法版本
    //e_phnum //程序头的数量
    //e_phentsize //程序头的大小（注意不是程序头表）
    if(memcmp(elf_header.e_ident,"\177ELF\1\1\1",7) || elf_header.e_type !=2 || elf_header.e_machine !=3 
        || elf_header.e_version!=1 || elf_header.e_phnum >1024 || elf_header.e_phentsize !=sizeof(struct Elf32_Phdr))
        {
            goto close_file;
        }

    //4 遍历所有程序头
    uint32_t i=0;
    struct Elf32_Phdr pro_header;
    Elf32_Half pro_header_size = elf_header.e_phentsize; //每个程序头的大小
    Elf32_Off pro_header_off = elf_header.e_phoff; //第一个程序头相对于文件的偏移地址
    while(i<elf_header.e_phnum)
    {
        memset(&pro_header,0,pro_header_size);

        //定位到第一个程序头位置，后面的程序头是紧挨着的
        sys_lseek(fd,pro_header_off,SEEK_SET);

        //读取程序头
        if(sys_read(fd,&pro_header,pro_header_size)!=pro_header_size)
        {
            goto close_file;
        }

        //根据程序头的段类型p_type判断如果是可加载程序段，将段加载到内存
        if (pro_header.p_type == PT_LOAD)
        {
            if(!load_segment(fd,pro_header.p_offset,pro_header.p_filesz,pro_header.p_vaddr))
            {
                goto close_file;
            }
        }

        //下一个程序头偏移地址
        pro_header_off+=pro_header_size;
        i++;
    }

    //如果读取成功，返回程序入口地址
    ret = elf_header.e_entry;

close_file:
    sys_close(fd);
    return ret;
}

//用path指向的程序替换当前程序
int32_t sys_execv(const char * path,const char ** argv)
{
    uint32_t argc=0;
    while(argv[argc])
    {
        argc ++;
    }

    int32_t entry_point = load_program(path);
    if (entry_point == -1)
    {
        return -1;
    }

    struct task_struct * cur_task =get_current_thread();
    //修改进程名字
    memcpy(cur_task->name,path,MAX_TASK_NAME_LENGTH);
    cur_task->name[MAX_TASK_NAME_LENGTH-1]=0;//注意这里不能写cur_task->name[MAX_TASK_NAME_LENGTH]=0，会导致pid为0
    struct syscall_stack * syscall_stack=(struct syscall_stack *)((uint32_t)cur_task + PG_SIZE - sizeof(struct syscall_stack));
   
    syscall_stack->ebx = (int32_t)argv; //参数传递给用户空间
    syscall_stack->ecx = argc;
    syscall_stack->eip = entry_point; //修改为新程序的入口
    syscall_stack->esp = (void*)0xc0000000;

    //fork中新任务的执行需要通过switch_to，那样的话就需要准备switch_to的环境,即压入ABI要求的寄存器
    //现在这里是系统调用进来的，修改系统调用栈直接从中断返回即可，也就是说execv并没有创建新的进程来执行新程序
    //而是将系统调用栈的eip修改为新程序的入口，假装从中断返回，实现了新程序的运行。
    asm volatile ("movl %0, %%esp \n\t"
                   "jmp syscall_exit \n\t"
                    : 
                    : "r" (syscall_stack));

   // syscall_exit();//不能写这个，因为这个进程不是新进程，没有调用switch_to()，必须手动移动esp到这个syscall_stack才行

    return 0;

}


