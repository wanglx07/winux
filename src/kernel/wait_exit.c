#include <thread.h>
#include <memory.h>
#include <fs.h>
#include <list.h>
#include <assert.h>
#include <file.h>

//释放用户进程所占用的资源 
void free_task_resource(struct task_struct * free_task)
{
    uint32_t * pde_vaddr = NULL; //页目录项虚拟地址
    uint32_t pde_idx=0;
    uint32_t pt_paddr_value;//页表物理地址值，注意是值，不是指针
    uint32_t * pt_vaddr=NULL;//页表的虚拟地址
    uint32_t * first_pte_vaddr = NULL; //每个页表中第一个页表项的虚拟地址
    uint32_t pte_idx=0;
    uint32_t * pte_vaddr=NULL; //页表项虚拟地址
    uint32_t page_paddr_value;//页的物理地址

    // 1、  回收进程用户空间占用的页
    while(pde_idx<768)    //遍历每个页目录项
    {
        pde_vaddr = free_task->pdt_base_vir+ pde_idx; 
        pt_paddr_value=*pde_vaddr;
        if (pt_paddr_value & 0x1) //p位为1，说明页表存在
        {
            pt_vaddr=pde_idx * 0x400000; //页表的虚拟地址，一个目录项对应一个页表，一个页表能表示4M容量,即0x400000
            first_pte_vaddr=pte_get(pt_vaddr);//每个页表的起始虚拟地址就是第一个页表项的虚拟地址
            pte_idx=0;

            while(pte_idx<1024)//遍历每个页表项
            {
                pte_vaddr= first_pte_vaddr+pte_idx;
                page_paddr_value=*pte_vaddr;
                if (page_paddr_value & 0x1)//p位为1，说明被使用中
                {
                    page_paddr_value &=0xfffff000;//将页的低12位清0
                    phy_addr_free(page_paddr_value);//回收paddr对应的页到物理内存池
                }
                pte_idx++;
            }

            //将页清完0后，也将页表的p位置0。memory.c的free_page()仅仅是将页的物理地址的p位清0，这里页表的物理地址的p位也清0
            pt_paddr_value &=0xfffff000;
            phy_addr_free(pt_paddr_value);
        }
        pde_idx++;
    }

    // 2、  回收用户虚拟内存池位图占用的页
    uint32_t bmap_len_pg=DIV_ROUND_UP(free_task->vir_pool_user.bmap.length , PG_SIZE);
    free_page(PHY_POOL_KERNEL,free_task->vir_pool_user.bmap.ptr,bmap_len_pg);

    // 3、  关闭进程打开的文件
    uint32_t fd_idx = 3;
    while(fd_idx < MAX_FILES_PER_PROC_OPEN)
    {
        if (free_task->fd_table[fd_idx] != -1)
        {
            if (is_pipe(fd_idx))
            {
                struct task_struct * cur_task =get_current_thread();
                int32_t f_idx= cur_task->fd_table[fd_idx]; //获取文件表下标
                file_table[f_idx].fd_pos--;
                //如果此管道上的描述符都被关闭了，释放管道的环形缓冲区
                if (file_table[f_idx].fd_pos==0)
                {
                    free_page(PHY_POOL_KERNEL,file_table[f_idx].fd_inode,1);
                    file_table[f_idx].fd_inode = NULL;
                }
            }else{
                sys_close(fd_idx);
            }
        }
        fd_idx++;
    }
}

//查找parent_pid是ppid的子进程,成功返回true,失败则返回false
bool find_child(struct list_head * entry,int32_t ppid)
{
    struct task_struct * task=container_of(entry,struct task_struct,total_entry);
    if (task->parent_pid == ppid)
    {
        return true;
    }
    return false;
}

//查找parent_pid是ppid并且status是hanging的子进程,成功返回true,失败则返回false
bool find_hanging_child(struct list_head * entry,int32_t ppid)
{
    struct task_struct * task=container_of(entry,struct task_struct,total_entry);
    if (task->parent_pid == ppid && task->status == TASK_HANGING)
    {
        return true;
    }
    return false;
}


//init进程领养一个子进程
bool init_adopt_a_child(struct list_head * entry,int32_t ppid)
{
    struct task_struct * task=container_of(entry,struct task_struct,total_entry);
    if (task->parent_pid == ppid)
    {
        task->parent_pid=1;
    }
    return false;
}


//等待子进程调用exit,子进程将退出的状态保存到status指向的变量,成功则返回子进程的pid,失败则返回-1 
pid_t sys_wait(uint32_t * status)
{
    struct task_struct * cur_task=get_current_thread();

    while(1)
    {
        //优先处理已经是挂起状态的子进程
        struct list_head * entry=list_find(&total_list_head,find_hanging_child,cur_task->pid);
        if (entry!=NULL)
        {
            struct task_struct * child_task=container_of(entry,struct task_struct,total_entry);
            *status = child_task->exit_status;
            //thread_exit之后,pcb会被回收,因此提前获取pid
            uint32_t child_pid = child_task->pid;

            //删除子进程pcb,传入false,使thread_exit调用后回到此处
            thread_exit(child_task,false);

            return child_pid;
        }

        //查看是否有子进程
        entry = list_find(&total_list_head,find_child,cur_task->pid);
        if (entry == NULL)
        {
            return -1;
        }else{
            //如果有子进程，此时说明它的状态必然不是 TASK_HANGING ，也就是说子进程尚未调用 exit,则将自己挂起,直到子进程在执行exit时将自己唤醒
            thread_block(TASK_WAITING);
        }   
    }
}


void sys_exit(int32_t status)
{
    struct task_struct * cur_task = get_current_thread();
    cur_task->exit_status = status;

    if (cur_task->parent_pid==-1)
    {
        panic("sys_exit: parent_pid error\r\n");
    }   

    //将自己的所有子进程都过继给init进程
    list_find(&total_list_head,init_adopt_a_child,cur_task->pid);

    //回收进程的资源
    free_task_resource(cur_task);

    struct task_struct * parent_task = pid2thread(cur_task->parent_pid);
    if (parent_task->status== TASK_WAITING)
    {
        thread_unblock(parent_task);
    }

    //将自己挂起,等待父进程获取其status,并回收其pcb
    thread_block(TASK_HANGING);
}