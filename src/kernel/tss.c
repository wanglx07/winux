#include <tss.h>
#include <types.h>
#include <string.h>
#define PG_SIZE 4096
struct tss{
    uint32_t prev;
    uint32_t* esp0;
    uint32_t ss0;
    uint32_t* esp1;
    uint32_t ss1;
    uint32_t* esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t (*eip) (void);
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldt;
    uint32_t trace;
    uint32_t io_base;
};

//段描述符
typedef struct  __attribute__((packed)) seg_desc{
    u16 limit_0_15;
    u16 base_0_15;
    u8 base_16_23;
    u8 type:4; 
    u8 s:1; 
    u8 DPL:2;
    u8 P:1;
    u8 limit_16_19:4;
    u8 AVL:1;
    u8 L:1;
    u8 DB:1;
    u8 G:1;
    u8 base_24_31;
}  seg_desc;

static struct tss tss;

seg_desc make_seg_descriptor(uint32_t base_addr,uint32_t limit,uint32_t attr)
{
    seg_desc desc;
    desc.limit_0_15=limit;
    desc.base_0_15=base_addr;
    desc.base_16_23=base_addr>>16 & 0xff;
    desc.type=attr;
    desc.s=attr>>4;
    desc.DPL=attr>>5 & 0x3;
    desc.P=attr>>7;
    desc.limit_16_19=limit>>16 & 0xf; 
    desc.AVL = attr>>8;
    desc.L =attr>>9;
    desc.DB=attr>>10;
    desc.G=attr>>11;
    desc.base_24_31=base_addr>>24;
    return desc;
}

void tss_init()
{
    uint64_t gdtr_value;
    asm volatile("sgdt %0":"=m" (gdtr_value):);//获取GDTR中的值

    uint32_t gdt_base=gdtr_value>>16;//得到GDT基地址
    
    // printk("gdt base= %d \r\n",gdt_base);

    // 创建#4描述符，即TSS描述符。GDT基地址是0xc0007000
    *((seg_desc*)(gdt_base+0x20)) = make_seg_descriptor(((uint32_t)&tss), sizeof(tss),0x089);//G=0(粒度是字节),D=0(TSS固定为0),L=0,AVL=0 P=1,DPL=00,S=0(系统段),type=1001(B位为0)

    // 创建#5、#6描述符,即添加dpl为3的代码段和数据段描述符，和内核的数据段和代码段的范围是一样的，只是DPL位不同，主要为了方便访问内存 
    *((seg_desc*)(gdt_base+0x28)) = make_seg_descriptor(0, 0xfffff, 0xcf8);//G=1(粒度是4k),D=1(32位),L=0,AVL=0 P=1,DPL=11,S=1(代码段),type=1000(x=1,c=0,r=0,a=0 代码段是可执行的,非依从的,不可读的,已访问位a清0)
    *((seg_desc*)(gdt_base+0x30)) = make_seg_descriptor(0, 0xfffff,0xcf2);//G=1(粒度是4k),D=1(32位),L=0,AVL=0 P=1,DPL=11,S=1(数据段),type=0010(x=0,e=0,w=1,a=0 数据段是不可执行的,向上扩展的,可写的,已访问位a清0)
    
    //重新计算gdt_limit更新到GDTR中
    uint16_t gdt_limit=7*8-1;
    gdtr_value=gdt_base;
    gdtr_value=gdtr_value<<16 | gdt_limit;
    asm volatile("lgdt %0"::"m"(gdtr_value));

    // printk("gdtr_value= %x \r\n",gdtr_value);

    //初始化tss
    memset(&tss,0,sizeof(tss));
    tss.ss0=DATA_SEG_SELECTOR;//内核数据段作为栈段
    tss.io_base=sizeof(tss);//没有io位图
    asm volatile("ltr %w0" :: "r"(TSS_SELECTOR));//将 tss 加载到 TR,B位会被置1

}

void refresh_tss(struct task_struct *next_task)
{
    tss.esp0 = (uint32_t*)((uint32_t)next_task + PG_SIZE);
}