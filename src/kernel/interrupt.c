#include <interrupt.h>
#include <asm/io.h>
#include <types.h>
#include <printk.h>
//中断门
typedef struct  __attribute__((packed)) interrupt_gate{
    u16 offset_low;
    u16 code_seg_selector;
    u8 reserved;
    u8 type:4; //1110
    u8 s:1; //0
    u8 DPL:2;
    u8 P:1;
    u16 offset_hight;
}  interrupt_gate;

static interrupt_gate idt[IDT_SIZE]; //描述符表

extern void *asm_handler_table[IDT_SIZE];//汇编语言版中断处理函数

void * c_handler_table[IDT_SIZE];//c语言版中断处理函数
char * intr_info[IDT_SIZE];//中断的相关信息

extern void * asm_syscall_handler(void);//0x80中断处理函数

void general_interrupt_handler(u8 vector)
{
    printk("Exception %d occurred: %s \r\n",vector,intr_info[vector]);
    if (vector == 14) {	  // 若为Pagefault,将缺失的地址打印出来并悬停
      int page_fault_vaddr = 0; 
      asm ("movl %%cr2, %0" : "=r" (page_fault_vaddr));	  // cr2是存放造成page_fault的地址
      printk("\npage fault virtual address is %d \r\n",page_fault_vaddr);
   }
    while(1);
}


void interrupt_init()
{
   /*******************初始化c中断处理函数*********************/
   int j;
    for (j =0 ; j < IDT_SIZE; j++)
    {
        c_handler_table[j]=general_interrupt_handler;
        intr_info[j]="unknow";
    }

   intr_info[0] = "#DE Divide Error";
   intr_info[1] = "#DB Debug Exception";
   intr_info[2] = "NMI Interrupt";
   intr_info[3] = "#BP Breakpoint Exception";
   intr_info[4] = "#OF Overflow Exception";
   intr_info[5] = "#BR BOUND Range Exceeded Exception";
   intr_info[6] = "#UD Invalid Opcode Exception";
   intr_info[7] = "#NM Device Not Available Exception";
   intr_info[8] = "#DF Double Fault Exception";
   intr_info[9] = "Coprocessor Segment Overrun";
   intr_info[10] = "#TS Invalid TSS Exception";
   intr_info[11] = "#NP Segment Not Present";
   intr_info[12] = "#SS Stack Fault Exception";
   intr_info[13] = "#GP General Protection Exception";
   intr_info[14] = "#PF Page-Fault Exception";
   // intr_info[15] 第15项是intel保留项，未使用
   intr_info[16] = "#MF x87 FPU Floating-Point Error";
   intr_info[17] = "#AC Alignment Check Exception";
   intr_info[18] = "#MC Machine-Check Exception";
   intr_info[19] = "#XF SIMD Floating-Point Exception";

   /*******************初始化idt*********************/
    int i ;
    for ( i = 0; i < IDT_SIZE; i++)
    {
        interrupt_gate * gate=&(idt[i]);
        u32 handler_ptr=(u32)(asm_handler_table[i]);
        gate->offset_low=handler_ptr;
        gate->code_seg_selector=1<<3;
        gate->reserved=0;
        gate->type=0b1110;
        gate->s=0;
        gate->DPL=0;
        gate->P=1;
        gate->offset_hight=handler_ptr>>16;
    }

    //新增0x80中断描述符，注意中断门DPL为3,否则用户进程调用不了
    interrupt_gate * gate=&(idt[IDT_SIZE-1]);//注意这里不能写idt[80],80和0x80还是有区别的
    u32 handler_ptr=(u32)(asm_syscall_handler);
    gate->offset_low=handler_ptr;
    gate->code_seg_selector=1<<3;
    gate->reserved=0;
    gate->type=0b1110;
    gate->s=0;
    gate->DPL=3;//DPL要为3
    gate->P=1;
    gate->offset_hight=handler_ptr>>16;

   //  uint64_t idt_operand = ( (sizeof (idt) - 1) | ( (uint64_t) ( (uint32_t) idt < 16))); //错误
   u64 idt_info=idt;
   idt_info=idt_info<<16 | (sizeof(idt) - 1);
   asm volatile ("lidt %0"::"m"(idt_info));

   /*******************设置8259A中断控制器*********************/
   //8259是如何区分这些命令字的？
   //先初始化，才能使用
   //初始化需输入ICW且需要依次输入ICW1,ICW2,ICW3,ICW4
   //初始化完之后，随时可以使用
   //OCW1:控制屏蔽哪个中断 OCW2:中断回复EOI OCW3:不常用
   //OCW1 需要从奇数端口输入，OCW2需要从偶数端口输入，OCW3需要从偶数端口输入，OCW2和OCW3通过中间的两位进行区分
   outb(PIC_M_CTRL,0x11); //ICW1:边沿触发/级联方式
   outb(PIC_M_DATA,0x20); //ICW2:起始中断向量
   outb(PIC_M_DATA,0x04); //ICW3:从片级联到IR2
   outb(PIC_M_DATA,0x01);  //ICW4:非总线缓冲，全嵌套，正常EOI

   //初始化从片
   outb(PIC_S_CTRL,0x11); //ICW1：边沿触发/级联方式
   outb(PIC_S_DATA,0x28); //ICW2:起始中断向量
   outb(PIC_S_DATA,0x04); //ICW3:从片级联到IR2
   outb(PIC_S_DATA,0x01);  //ICW4:非总线缓冲，全嵌套，正常EOI

//    outb(PIC_M_DATA,0xfe);//屏蔽其它管脚，IR0例外，使它可以接收时钟的中断，而它刚好是0x20号中断
   outb(PIC_M_DATA,0xf8);//同时打开时钟中断，键盘中断
   outb(PIC_S_DATA,0xbf);
}

//注册中断处理函数
void register_handler(uint8_t vector,void * handler)
{
    c_handler_table[vector]=handler;
}

enum intr_status intr_get_status()
{
    uint32_t eflags=0;
    asm volatile("pushfl; popl %0" : "=r" (eflags));
    return (eflags & (1<<9) )? INTR_ON:INTR_OFF;
}

void intr_restore_status (enum intr_status status)
{
    status==INTR_ON ?intr_enable():intr_disable();
}

enum intr_status intr_enable ()
{
    enum intr_status status;//原来的状态
    if (intr_get_status()==INTR_ON)
    {
        status=INTR_ON;
    }else{
        status=INTR_OFF;
        outb(PIC_S_CTRL,0X20);//一定要加上这几句，否则后面就接收不到时钟中断了，也就切换不到其它线程了
        outb(PIC_M_CTRL,0X20);
        asm volatile("sti");
    }
    return status;
}

enum intr_status intr_disable ()
{
    enum intr_status status;//原来的状态
    if (intr_get_status()==INTR_ON)
    {
        status=INTR_ON;
        asm volatile("cli");
    }else{
        status=INTR_OFF;
    }
    return status;
}