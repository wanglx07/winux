#include <stdarg.h>
#include <print.h>
#include <mutex.h>

static char buf[1024];

extern int vsprintf (char *buf, const char *fmt, va_list args);

static mutex printk_lock;

int printk(const char *fmt,...)
{
    mutex_lock(&printk_lock);
	va_list args;			// va_list 实际上是一个字符指针类型。
	int i;

	va_start (args, fmt);		// 参数处理开始函数。在（include/stdarg.h）
	i = vsprintf (buf, fmt, args);	// 使用格式串fmt 将参数列表args 输出到buf 中,返回值i 等于输出字符串的长度

	va_end (args);		// 参数处理结束函数。

    print(buf); //进行打印
	mutex_unlock(&printk_lock);
	return i;			// 返回字符串长度。
}

void console_init()
{
	clear_screen();
	mutex_init(&printk_lock);
}