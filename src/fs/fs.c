#include <fs.h>
#include <inode.h>
#include <dir.h>
#include <file.h>
#include <disk.h>
#include <string.h>
#include <memory.h>
#include <list.h>
#include <assert.h>
#include <thread.h>
#include <kfifo.h>
#include <pipe.h>

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

extern struct list_head partition_list;	 // 分区队列
extern struct ata_channel channel_ata0; //通道ata0 

struct partition * current_part;

//创建文件系统
void create_fs(struct partition * part)
{
    /*********** 1、根据分区 part 大小，计算分区文件系统各元信息需要的扇区数********/
    uint32_t boot_block_sector_count= 1;
    uint32_t super_block_sector_count = 1;
    uint32_t inode_bmap_sector_count=DIV_ROUND_UP(MAX_FILES_PER_PART,BITS_PER_SECTOR); //inode位图占用扇区数，注意这里不是除以512，而是4096，因为一个扇区有4096个位
    uint32_t inode_table_sector_count= DIV_ROUND_UP(MAX_FILES_PER_PART * sizeof(struct inode),SECTOR_SIZE);

    //空闲扇区数=part分区总的扇区数-已使用的扇区数，注意这里的分区是主分区或者逻辑分区，不包括MBR或者EBR
    uint32_t free_sectors=part->sector_count-boot_block_sector_count-super_block_sector_count-inode_bmap_sector_count-inode_table_sector_count;
    //因为空闲块位图自身也要占用一部分内存，所以空闲扇区数和空闲块位图占用的扇区数相互影响
    //这里以最大的空闲扇区数来算出空闲块位图占用的扇区数，这样算简便
    uint32_t free_block_bmap_sector_count = DIV_ROUND_UP(free_sectors,BITS_PER_SECTOR);
    //最少有那么多空闲扇区数
    free_sectors=free_sectors-free_block_bmap_sector_count;
    //重新计算空闲块位图扇区数
    free_block_bmap_sector_count=DIV_ROUND_UP(free_sectors,BITS_PER_SECTOR);

    /************* 2、在内存中创建超级块，将以上步骤计算的元信息写入超级块。***********/
    struct super_block sb; 
    sb.magic = 0x20220820;
    sb.sector_count= part->sector_count;
    sb.inode_count = MAX_FILES_PER_PART;
    sb.start_sector=part->start_sector;
    
    sb.free_block_bmap_start_sector =sb.start_sector+2;//空闲块位图绝对起始扇区,第0块是引导块，第1块是超级块
    sb.free_block_bmap_sector_count = free_block_bmap_sector_count;//空闲块位图占用的扇区数

    sb.inode_bmap_start_sector =sb.free_block_bmap_start_sector+sb.free_block_bmap_sector_count;//i结点位图绝对起始扇区
    sb.inode_bmap_sector_count =inode_bmap_sector_count;//i结点位图占用的扇区数

    sb.inode_table_start_secotr = sb.inode_bmap_start_sector+sb.inode_bmap_sector_count;//i结点表绝对起始扇区
    sb.inode_table_sector_count =inode_table_sector_count;//i结点表占用的扇区数

    sb.data_start_sector =sb.inode_table_start_secotr+sb.inode_table_sector_count;//空闲数据块（注意不是空闲块位图）起始扇区号
    sb.root_inode_num = 0;//根目录所在的i结点号,也就是inode 数组中第0个inode 我们留给了根目录。
    sb.dir_entry_size = sizeof(struct dir_entry);//目录项大小

    printk("%s info:\r\n", part->name);
    printk("   magic:0x%x\r\n   part start_sector:0x%x\r\n   all_sectors:0x%x\r\n   inode_count:0x%x\r\n \  
    free_block_bmap_start_sector:0x%x\r\n free_block_bmap_sector_count:0x%x\r\n   inode_bmap_start_sector:0x%x\r\n \ 
    inode_bmap_sector_count:0x%x\r\n   inode_table_start_secotr:0x%x\r\n  inode_table_sector_count:0x%x\r\n \
    data_start_sector:0x%x\r\n", sb.magic, sb.start_sector, sb.sector_count, sb.inode_count, \
    sb.free_block_bmap_start_sector, sb.free_block_bmap_sector_count, sb.inode_bmap_start_sector, \
    sb.inode_bmap_sector_count, sb.inode_table_start_secotr,sb.inode_table_sector_count, sb.data_start_sector);
        

    /************************ 3、将超级块写入本分区的第1扇区  **************************/
    struct disk * hd = part->to_which_disk;
    disk_write(hd,part->start_sector+1,&sb,1);
    printk("   super_block start_sector:0x%x\n", part->start_sector + 1);


    /*********** 4、将元信息(空闲块位图、inode位图、inode数组)写入磁盘上各自的位置 *************/
    //注意第2步只是将空闲块位图、inode位图、inode数组信息记录到超级块中，并没有将空闲块位图、inode位图、inode数组写入磁盘
    //申请一个缓冲区，大小是空闲块位图、inode位图、inode数组三者中最大的
    uint32_t bufsize;
    if (free_block_bmap_sector_count >= inode_bmap_sector_count)
    {
        bufsize =free_block_bmap_sector_count;
    }else{
        bufsize=inode_bmap_sector_count;
    }

    if(bufsize <= inode_table_sector_count)
    {
        bufsize=inode_table_sector_count;
    }

    uint8_t * buf=(uint8_t *)malloc_sys(bufsize* SECTOR_SIZE);//申请堆内存,注意这里要乘以扇区大小

    //4.1 将空闲块位图写入sb.free_block_bmap_start_sector
    buf[0] |= 0x01;//我们把第0个空闲块作为根目录，因此我们需要在空闲块位图中将第0位置1，别人就申请不了它了 
    uint32_t free_block_bmap_last_byte= free_sectors/8;//假如free_sectors=4097,则free_block_bmap_last_byte=512
    uint8_t free_block_bmap_last_bit= free_sectors% 8;//则free_block_bmap_last_bit=1，也就是计算位图占用了多少个字节+多少个位
    //位图所在最后一个扇区中，不足一扇区的剩余字节。
    uint32_t left_byte= SECTOR_SIZE - (free_block_bmap_last_byte % SECTOR_SIZE);

    //先将位图最后一字节到其所在的扇区的结束全置为1,即超出实际块数的部分直接置为已占用
    memset(&buf[free_block_bmap_last_byte], 0xff, left_byte);

    //再将上一步中覆盖的最后一个字节内的有效位重新置0
    int offset=0;
    while(offset<free_block_bmap_last_bit)//这里不能是等于
    {
        buf[free_block_bmap_last_byte] &= ~(1<<offset);
        offset++;
    }
    disk_write(hd,sb.free_block_bmap_start_sector,buf,sb.free_block_bmap_sector_count);

    //4.2 将inode位图写入sb.inode_bmap_start_sector
    memset(buf,0,bufsize);//清0
    buf[0] |= 0x01;//第0个inode留给根目录
    disk_write(hd,sb.inode_bmap_start_sector,buf,sb.inode_bmap_sector_count);
    //4.3 将inode数组写入sb.inode_table_start_secotr
    memset(buf,0,bufsize);//清0
    struct inode * i_node=(struct inode *) buf;
    i_node->i_num=0; //第0个inode
    i_node->i_size=sb.dir_entry_size*2; //.和..
    i_node->i_sectors[0]=sb.data_start_sector;//第0个空闲块
    disk_write(hd,sb.inode_table_start_secotr,buf,sb.inode_table_sector_count);   


    /************************ 5、将根目录写入磁盘  **************************/
    memset(buf,0,bufsize);//清0
    struct dir_entry * de=(struct dir_entry *)buf;

    //初始化当前目录"."
    memcpy(de->filename,".",1);
    de->i_num = 0; //指向第0个inode，也就是根目录自己
    de->f_type=FT_DIRECTORY;

    de++;

    //初始化当前目录父目录".."
    memcpy(de->filename,"..",2);
    de->i_num=0; //仍然指向根目录
    de->f_type=FT_DIRECTORY;

    //将根目录项写入磁盘
    disk_write(hd,sb.data_start_sector,buf,1);
    printk("filesystem create done-------------\r\n");
    free_sys(buf);
}

bool partition_mount(struct list_head * p,void * args )
{
    char * part_name = args;
    struct partition * part=container_of(p,struct partition,entry); 
    if (!strcmp(part_name,part->name))
    {
        current_part = part;
        struct disk * hd=part->to_which_disk;
        
        //用来装从磁盘读到的超级块
        struct super_block * sb_buf = (struct super_block *)malloc_sys(sizeof(struct super_block));
        if (sb_buf==NULL)
        {
            panic("malloc_sys failed \r\n");
        }
        memset(sb_buf,0,sizeof(struct super_block));

        //1、从磁盘读取超级块，并复制到当前分区的超级块中
        //注意super_block里面的只是个超级块指针，并没有具体的超级块，所以在内存里为current_part分配超级块内存
        current_part->super_b =(struct super_block *)malloc_sys(sizeof(struct super_block));
        if (current_part->super_b==NULL)
        {
            panic("malloc_sys failed \r\n");
        }
        disk_read(hd,current_part->start_sector+1,sb_buf,1);
        memcpy(current_part->super_b,sb_buf,sizeof(struct super_block));

        //2、从磁盘读取空闲块位图到内存
        current_part->block_bitmap.ptr = (uint8_t *)malloc_sys(sb_buf->free_block_bmap_sector_count * SECTOR_SIZE);
        if (current_part->block_bitmap.ptr==NULL)
        {
            panic("malloc_sys failed \r\n");
        }
        current_part->block_bitmap.length = sb_buf->free_block_bmap_sector_count* SECTOR_SIZE;
        disk_read(hd,sb_buf->free_block_bmap_start_sector,current_part->block_bitmap.ptr,sb_buf->free_block_bmap_sector_count);
    
        //3、从磁盘读取inode位图到内存
        current_part->inode_bitmap.ptr = (uint8_t *)malloc_sys(sb_buf->inode_bmap_sector_count * SECTOR_SIZE);
        if (current_part->inode_bitmap.ptr==NULL)
        {
            panic("malloc_sys failed \r\n");
        }
        current_part->inode_bitmap.length = sb_buf->inode_bmap_sector_count* SECTOR_SIZE;
        disk_read(hd,sb_buf->inode_bmap_start_sector,current_part->inode_bitmap.ptr,sb_buf->inode_bmap_sector_count);

        INIT_LIST_HEAD(&current_part->open_inodes_list);//初始化本分区打开的i结点队列

        printk("mount %s done......\r\n",current_part->name);

        free_sys(sb_buf);
        return true;//找到符合条件的了，返回true结束循环
    }

    return false;//让list_find继续循环
}

//例如要解析路径“／a/b/c ”的话，调用 path_parse 时， name_buf 的值是"a",然后返回子路径字符串“/b/c ”的地址
//下次再调用 path_parse 时，主调函数若传给 pathname 的参数是“/b/c”， path_parse
//执行后 name_buf中的值变为“b”，然后返回子路径“／c ”的地址。
//第一个参数可以是：  char * pathname="/a/b/c";
//第二个参数必须是：  char name_buf[12]={0}; 这种类型
char * path_parse(char * pathname,char* name_buf)
{
    //路径中出现1个或多个连续的字符'/',将这些'/'跳过,如"///a/b"
    while(* pathname == '/')
    {
        pathname++;
    }

    while(*pathname!='/' && *pathname!=0)//注意这里不能是||，||的话会出错
    {
        *name_buf++=*pathname++;
    }

    if (*pathname==0)
    {
        return NULL;
    }
    
    return pathname;
}

//返回路径深度,比如/a/b/c,深度为3 
uint32_t path_depth(const char * pathname)
{
    char name_buf[MAX_FILE_NAME_LENGTH]={0};
    uint32_t depth=0;
    char * p;
    p=path_parse(pathname,name_buf);
    while(*name_buf)
    {
        depth++;
        memset(name_buf,0,sizeof(name_buf));
        if (p)//不为NULL说明里面有东西，还需要继续解析
        {
            p=path_parse(p,name_buf);
        }  
    }
    return depth;
}


//搜索文件pathname,若找到则返回其inode号,否则返回-1，文件的信息存在record中
//注意pathname是绝对路径，比如有路径/a/b/c,我要找件c，则pathname=/a/b/c
//我要找b，则pathname=/a/b
int file_search(const char * pathname,struct path_record * record)
{
    //如果待查找的是根目录,为避免下面无用的查找,直接返回已知根目录信息
    if (!strcmp(pathname,"/") || !strcmp(pathname,"/.") || !strcmp(pathname,"/.."))
    {
        record->path_traveled[0]=0;	   // 搜索路径置空
        record->f_type=FT_DIRECTORY;
        record->parent_dir=&root_dir;
        return 0;
    }

    //先记录一下
    record->parent_dir=&root_dir; //当前最新目录
    record->f_type=FT_UNKNOWN;

    //为解析路径做准备
    char name_buf[MAX_FILE_NAME_LENGTH]={0};
    char * sub_path;
    sub_path=path_parse(pathname,name_buf);
    struct dir * parent_dir=&root_dir;
    struct dir_entry dir_e;
    uint32_t parent_inode_num=0; 

    while(*name_buf)
    {
        //将已经解析过的目录或者文件路径记录下来
        strcat(record->path_traveled,"/");
        strcat(record->path_traveled,name_buf);

        if (dir_entry_search(current_part,parent_dir,name_buf,&dir_e))//已经解析过的目录或文件路径存在
        {
            if (dir_e.f_type==FT_DIRECTORY) 
            {
                //如果pathname是目录，而且它是最后的一个目录了，那么最后需要将它的上一级目录打开，以便操作，所以这里记录一下它的上一级目录
                parent_inode_num = parent_dir->inode->i_num;
                //关闭之前的父目录，以新解析得到的目录作为下一轮的父目录，比如/a/b/c ,解析a了，则关闭根目录/，以 a作为/b/c的父目录，
                dir_close(parent_dir);
                parent_dir = dir_open(current_part,dir_e.i_num);
                record->parent_dir = parent_dir;//记录一下最新的目录

                //之前解析的是目录的话,它可能会包含其他文件，还是能继续往下解析的
                memset(name_buf,0,sizeof(name_buf));
                if (sub_path)//并且还没解析完，继续解析
                {
                    sub_path=path_parse(sub_path,name_buf);
                }
                
            }else if(dir_e.f_type==FT_REGULAR){
                record->f_type=FT_REGULAR;
                //之前解析的是文件的话不能再往下解析了，直接返回文件的inode编号
                return dir_e.i_num;
            }
           
        }else{
            //否则返回-1，可以通过record->path_traveled知道是在这个路径断了，直接路径保存在record->parent_dir中
            //就可以知道原来record->parent_dir下没有这个文件，可以在record->parent_dir下新建这个文件
            return -1;
        }  
    }

    //如果解析到最后是个目录,则关闭record->parent_dir，因为我们前面while中看它是目录，以为它还有其它子目录或者文件的，没想到它自己就是最后
    //一个目录了，所以这里把它关了，而打开它的父目录，因为用户有可能想在这个它的同级目录下创建新的目录或文件
    //比如说，/a/b，b是个目录，那么就把b关了，重新打开它的父目录a，以便在a下创建新的目录或者文件
    dir_close(record->parent_dir);
    record->parent_dir=dir_open(current_part,parent_inode_num);
    record->f_type=FT_DIRECTORY;
    return dir_e.i_num;

}

//创建文件或打开成功后(注意这里是两项功能)，返回 pcb fd_table 中的下标，否则返回-1
int32_t sys_open(const char * pathname,enum open_flag flags)
{
    //如'/a/'，返回-1
    if (pathname[strlen(pathname)-1] == '/')
    {
        printk("can't open a directory with sys_open() , you can try dir_open() %s\r\n",pathname);
        return -1;
    }

    //默认找不到
    int32_t fd =-1;

    //检查文件是否存在
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num = file_search(pathname,&record);
    bool found =i_num !=-1 ? true : false;

    if (record.f_type == FT_DIRECTORY) {//不管存不存在，你通过sys_open()函数操作目录，都不允许
        printk("can`t open a direcotry with open()\n");
        dir_close(record.parent_dir);
        return -1;
    }

    if (found && (flags & O_CREAT))//文件存在了，还想创建同名文件，肯定不行，就算想创建同名目录也不行
    {
        printk(" %s already exist\r\n",pathname);
        dir_close(record.parent_dir);
        return -1;
    }

    if(!found && !(flags & O_CREAT))//文件不存在，但是你又不是创建文件，而是想读或者写，肯定不行
    {
        printk(" %s not exist , but you don't want to create \r\n",pathname);
        dir_close(record.parent_dir);
        return -1;
    }

    if(!found)
    {
        //不存在，还要判断是不是pathname中间的某个路径不存在，没判断到最后，
        uint32_t normal_depth = path_depth(pathname);//正常情况下的深度
        uint32_t traveled_depth = path_depth(record.path_traveled);//实际遍历得到的深度
        if(normal_depth != traveled_depth)
        {
            printk("a directory in the middle does not exist \r\n");// 说明并没有访问到全部的路径,某个中间目录是不存在的
            dir_close(record.parent_dir);
            return -1;
        }
    }
    
    //到这里就是要么是文件不存在，想创建 ，要么是文件存在，想读写，两种情况，所以通过flags进行区分
    if (flags & O_CREAT)
    {
        printk("creating file \r\n");
        fd=file_create(record.parent_dir,(strrchr(pathname, '/') + 1),flags);
    }else{ //O_RDONLY, O_WRONLY,O_RDWR
        fd =file_open(i_num,flags);
    }

    dir_close(record.parent_dir);
    return fd;
}

//根据文件描述符fd关闭文件，成功返回0，失败返回-1
int32_t sys_close(int32_t fd)
{
    int32_t ret=-1;
    if (fd>2)
    {
        struct task_struct * cur_task =get_current_thread();
        int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
        if (is_pipe(fd))
        {
            file_table[f_idx].fd_pos--;
            //如果此管道上的描述符都被关闭了，释放管道的环形缓冲区
            if (file_table[f_idx].fd_pos==0)
            {
                free_page(PHY_POOL_KERNEL,file_table[f_idx].fd_inode,1);
                file_table[f_idx].fd_inode = NULL;
            }
            ret =0;
        }else{
            ret =file_close(&file_table[f_idx]); 
        }
        
        cur_task->fd_table[fd]=-1; 
    }
    return ret;
}

//文件系统初始化
void fs_init()
{
    struct super_block * sb=(struct super_block *)malloc_sys(sizeof(struct super_block));
    struct disk* hd_slave=&channel_ata0.disks[1];
    struct partition * part ;
    list_for_each_entry(part,&partition_list,entry)
    {
        memset(sb,0,SECTOR_SIZE);
        //读出分区超级块
        disk_read(hd_slave,part->start_sector+1,sb,1);
        //判断是否存在文件系统
        if(sb->magic== 0x20220820)
        {
            printk("    partition %s has filesystem\r\n",part->name);
        }else{
            printk("    creating filesystem in %s \r\n",part->name);
            create_fs(part);
        }
    }
    free_sys(sb);

    char default_part[8]="sdb1";//默认分区
    //挂载分区
    list_find(&partition_list,partition_mount,default_part);

    //打开当前分区根目录
    root_dir_open(current_part);

    //初始化文件表
    int i;
    for ( i = 0; i <MAX_FILE_OPEN; i++)
    {
        file_table[i].fd_inode =NULL;
    }

}

int32_t sys_write(int32_t fd,const void * buf, uint32_t count)
{
    if (fd<0)
    {
        printk("fd error in sys_write \r\n");
        return -1;
    }

    if (fd == STD_OUT)//输出到屏幕
    {
        if (is_pipe(fd))//标准输出有可能被重定向为管道缓冲区
        {
            return pipe_write(fd,buf,count);
        }else{
            // printk("%s",buf); //这里不用加\r\n，因为buf有就有，没有就没有，看使用者的意思
            print(buf);//这里直接用print，不然用户进程加载那节的应用程序用到printf需要链接很多.o文件
            return strlen(buf); 
        }
    }
    
    if (is_pipe(fd))//如果是管道，则写入管道，用于进程间通信
    {
        return pipe_write(fd,buf,count);
    }

    struct task_struct * cur_task =get_current_thread();
    int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
    struct file * file =&file_table[f_idx]; 

    if (file->fd_flag & O_WRONLY || file->fd_flag & O_RDWR)//只有包含 O_WRONLY、 O_RDWR 的文件才允许写入数据
    {
        return file_write(file,buf,count);
    }else{
        // printk("not allow to write file without flag O_RDWR or O_WRONLY in sys_write \r\n");
        print("not allow to write file without flag O_RDWR or O_WRONLY in sys_write \r\n");
        return -1;
    }

}

int32_t sys_read(int32_t fd , void * buf , uint32_t count)
{
    int32_t ret =-1;
       if (fd<0)
    {
        printk("fd error in sys_read \r\n");
        return -1;
    }

    if(fd==STD_IN)//从屏幕读
    {
        if (is_pipe(fd))//标准输入有可能被重定向为管道缓冲区
        {
            ret= pipe_read(fd,buf,count);
        }else{
            int32_t i=0; 
            char * pos=buf;
            while(i<count)
            {
                *pos= kfifo_getchar(&fifo);//从键盘缓冲区获取

                if (*pos!=0)
                {
                    i++;
                }
                pos++;
            }
            ret = i==0 ? -1 : i;            
        }
    }else if(is_pipe(fd)){//如果是管道，则从管道读，用于进程间通信
         ret= pipe_read(fd,buf,count);
    }else{//从文件读
        struct task_struct * cur_task =get_current_thread();
        int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
        struct file * file =&file_table[f_idx]; 
        ret=file_read(file,buf,count);
    }
    return ret;

}

//重置用于文件读写操作的偏移指针,成功时返回新的偏移指针,出错时返回-1，注意offset可为负值
int32_t sys_lseek(int32_t fd , int32_t offset ,enum whence w)
{
    if (fd<0)
    {
        printk("fd error in sys_read \r\n");
        return -1;
    }

    int32_t new_pos=0;
    struct task_struct * cur_task =get_current_thread();
    int32_t f_idx= cur_task->fd_table[fd]; //获取文件表下标
    struct file * file =&file_table[f_idx]; 

    switch(w)
    {
        case SEEK_SET: //新的读写位置是相对于文件开头再增加offset个位移量
        new_pos = offset;
        break;
        case SEEK_CUR: //新的读写位置是相对于当前的位置增加offset个位移量
        new_pos = (int32_t)file->fd_pos+offset; //offset可正可负
        break;
        case SEEK_END://新的读写位置是相对于文件尺寸再增加offset个位移量
        new_pos = (int32_t)file->fd_inode->i_size + offset; //offset应为负
        break;
    }

    if (new_pos < 0 || new_pos >=(int32_t)file->fd_inode->i_size )
    {
        return -1;
    }

    file->fd_pos = new_pos;
    return new_pos;
}

//删除文件，成功返回0，失败返回-1
int32_t sys_unlink(const char * pathname)
{
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num=file_search(pathname,&record);

    if (i_num ==-1)
    {
       printk("file not found in sys_unlink\r\n");
       dir_close(record.parent_dir);
       return -1; 
    }

    if (record.f_type == FT_DIRECTORY)
    {
        printk("cann't delete a directory with sys_unlink \r\n");
        dir_close(record.parent_dir);
        return -1; 
    }

    //检查是否在文件数组中，如果在，说明正在被使用，不能删
    uint32_t i; 
    for ( i = 0; i <MAX_FILE_OPEN; i++)
    {
        if(file_table[i].fd_inode != NULL && (uint32_t)i_num == file_table[i].fd_inode->i_num)
        {
            break;
        }
    }

    if (i<MAX_FILE_OPEN)
    {
        dir_close(record.parent_dir);
        printk("file is using , can't delete \r\n");
        return -1;
    }

    void * io_buf = malloc_sys(512*2);
    if (io_buf == NULL)
    {
        dir_close(record.parent_dir);
        printk("malloc for io_buf failed in sys_unlink\r\n");
        return -1;
    }

    dir_entry_delete(current_part ,record.parent_dir ,i_num ,io_buf);
    inode_release(current_part,i_num);
    free_sys(io_buf);
    dir_close(record.parent_dir);
    return 0;
}

//创建目录，成功返回0，失败返回-1
int32_t sys_mkdir(const char * pathname)
{
    //先申请缓冲区
    void * io_buf=malloc_sys(1024);
    if (io_buf==NULL)
    {
        printk(" malloc_sys for io_buf failed in sys_mkdir\r\n");
        return -1;
    }

    //检查目录是否存在
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num = file_search(pathname,&record);

    if (i_num !=-1)//目录已存在
    {
        printk("directory already exist in sys_mkdir \r\n");
        goto free_io_buf;
    }else{
        //不存在，还要判断是不是pathname中间的某个路径不存在，没判断到最后，
        uint32_t normal_depth = path_depth(pathname);//正常情况下的深度
        uint32_t traveled_depth = path_depth(record.path_traveled);//实际遍历得到的深度
        if(normal_depth != traveled_depth)
        {
            printk("a directory in the middle does not exist in sys_mkdir\r\n");// 说明并没有访问到全部的路径,某个中间目录是不存在的
            goto free_io_buf;
        }
    }

    /**************1、为新目录分配inode编号并创建inode*************************/
    //申请inode编号
    i_num = inode_bmap_alloc(current_part);
    if (i_num==-1)
    {
        printk("alloc inode num failed in sys_mkdir \r\n");
        goto free_io_buf;
    }

    //在内存中创建个inode，后面会更新到磁盘。
    //这个文件肯定是谁都能访问的，包括内核线程或者用户进程，所以应该将这个文件的inode创建在内核空间中，
    //而且为了长久保存，应该在内核的堆空间中创建
    struct inode * new_dir_node;
    struct task_struct * cur_task =get_current_thread();
    uint32_t * pdt_base_vir_temp=cur_task->pdt_base_vir;
    cur_task->pdt_base_vir=NULL; //使得malloc_sys()在内核堆空间中申请内存
    new_dir_node=(struct inode *)malloc_sys(sizeof(struct inode));
    cur_task->pdt_base_vir=pdt_base_vir_temp; //恢复pdt_base_vir
    if (new_dir_node==NULL)
    {
        printk("alloc inode failed in sys_mkdir \r\n");
        goto free_io_buf;
    }

    //初始化这个i结点
    inode_init(i_num,new_dir_node);

    /****2、为新目录分配一个块用于存目录项'.'、'..'，因为目录就是用来存目录项的*********/
    int32_t block_start_sector =-1;//块地址
    uint32_t block_bmap_idx=0;

    //分配新块
    block_start_sector= block_bmap_alloc(current_part); 
    if (block_start_sector==-1)
    {
        printk("block_bmap_alloc failed in sys_mkdir \r\n");
        goto roll_inode_bmap;
    }
    new_dir_node->i_sectors[0]=block_start_sector;

    //空闲块位图同步到硬盘
    block_bmap_idx = block_start_sector- current_part->super_b->data_start_sector;
    bmap_sync(current_part,block_bmap_idx,BLOCK_BMAP);

    /************3、将当前目录的目录项'.'和'..'写入新目录************* */
    memset(io_buf,0,1024);
    struct dir_entry * de = (struct dir_entry *)io_buf;

    //初始化当前目录"."
    memcpy(de->filename,".",1);
    de->i_num = i_num; //注意这个指向目录自己，不能写0
    de->f_type=FT_DIRECTORY;

    de++;

    //初始化当前目录父目录".."
    memcpy(de->filename,"..",2);
    de->i_num=record.parent_dir->inode->i_num; //注意这里指向父目录，不能写i_num
    de->f_type=FT_DIRECTORY;

    //将目录项写入磁盘
    disk_write(current_part->to_which_disk,new_dir_node->i_sectors[0],io_buf,1);
    new_dir_node->i_size=current_part->super_b->dir_entry_size*2; //.和..

    /************4、为新目录创建目录项并添加到父目录中**************/
    struct dir_entry dir_dir_entry; //可以和file.c的file_create()那里进行对比
    memset(&dir_dir_entry,0,sizeof(struct dir_entry));
    //目录名称后可能会有字符'/',所以最好直接用record.path_traveled,无'/'
    char * dir_name=strrchr(record.path_traveled, '/') + 1;
    dir_entry_init(dir_name , i_num , FT_DIRECTORY , &dir_dir_entry);
    memset(io_buf,0,1024);

    //在目录parent_dir下安装目录项new_dir_entry, 写入硬盘成功后返回true,否则false
    if (!dir_entry_sync(record.parent_dir,&dir_dir_entry,io_buf))
    {
        printk("dir_entry_sync failed in sys_mkdir \r\n");
        goto roll_inode_bmap;
    }

    /************5、父目录的inode同步到硬盘************* */
    memset(io_buf, 0, SECTOR_SIZE * 2);
    inode_sync(current_part, record.parent_dir->inode, io_buf);

    /************6、将新创建的目录inode同步到硬盘************* */
    memset(io_buf, 0, SECTOR_SIZE * 2);
    inode_sync(current_part, new_dir_node, io_buf);

    /************7、将inode位图同步到硬盘************* */
    bmap_sync(current_part, i_num, INODE_BMAP);

    free_sys(io_buf);

    //关闭所创建目录的父目录 
    dir_close(record.parent_dir);
    return 0;

    roll_inode_bmap:
        bitmap_set(&current_part->inode_bitmap,i_num,0);//新目录的块分配失败，之前位图中分配的i_num也要恢复
    free_io_buf:
        dir_close(record.parent_dir);
        free_sys(io_buf);
        return -1;
}

//打开目录，成功返回目录指针，失败则返回NULL
struct dir * sys_opendir(const char * name)
{
    //如果是根目录'/'
    if (name[0] == '/'&& name[1]==0)
    {
        return &root_dir;
    }

    //检查目录是否已经存在
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num = file_search(name,&record);
    struct dir * open_dir = NULL;
    if (i_num == -1) //不存在
    {
        printk("sub path %s doesn't exist in sys_opendir\r\n",record.path_traveled);
    }else{
        if (record.f_type == FT_REGULAR)
        {
            printk("can't open regular file  %s with sys_opendir\r\n",name);
        }else if(record.f_type == FT_DIRECTORY){
            open_dir = dir_open(current_part,i_num);
        }
    }
    dir_close(record.parent_dir);
    return open_dir;
}

int32_t sys_closedir(struct dir * pdir)
{
    if (pdir!= NULL)
    {
        dir_close(pdir);
        return 0;
    }else{
        return -1;
    }
}

struct dir_entry * sys_readdir(struct dir * dir)
{
    assert(dir!=NULL);
    return dir_read(dir);
}

//把目录dir的目录项已读字节数dir_offset置0
void sys_rewinddir(struct dir * dir)
{
    dir->dir_offset =0;
}

//删除空目录，成功则返回0，失败返回-1
int32_t sys_rmdir(const char * pathname)
{
    //检查文件是否存在
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num = file_search(pathname,&record);

    int32_t ret =-1;
    if (i_num ==-1)
    {
       printk("file %s not found in sys_rmdir\r\n",pathname);
    }else{
        if (record.f_type == FT_REGULAR)
        {
            printk("can't remove regular file  %s with sys_rmdir\r\n",pathname);
        }else{
            struct dir * dir =dir_open(current_part,i_num);
            if (!dir_empty(dir))
            {
                printk("dir %s is not empty ,can't remove\r\n",pathname);
            }else{
                if (!dir_remove(record.parent_dir,dir))
                {
                    ret=0;
                }
            }
            dir_close(dir);
        }
    }
    dir_close(record.parent_dir);
    return ret;
}

//由子目录或文件inode编号获取父目录inode编号
int32_t parent_dir_inode_get(uint32_t child_i_num ,void * io_buf)
{
    struct inode * node = inode_open(current_part,child_i_num);
    uint32_t block_start_sector = node->i_sectors[0]; //'..'位于第0个块
    inode_close(node);
    disk_read(current_part->to_which_disk,block_start_sector,io_buf,1);
    struct dir_entry * d_e = (struct dir_entry *) io_buf;
    return d_e[1].i_num;//返回父目录'..'的inode编号
}

// 在parent_i_num的目录中查找child_i_num对应的子目录名字,将名字存入缓冲区path.成功返回0,失败返-1
int32_t child_dir_name_get(uint32_t parent_i_num,uint32_t child_i_num ,char * path ,void * io_buf)
{
    struct inode * node= inode_open(current_part,parent_i_num);

    /****************1、收集父目录占用的块到all_blocks***********************/
    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in inode_release\r\n");
        return -1;
    }

    uint32_t block_idx=0;
    uint32_t block_count=12;
    while(block_idx<12)//先将前12个直接块存入all_blocks
    {
        all_blocks[block_idx] =node->i_sectors[block_idx];
        block_idx++;
    }

    if (node->i_sectors[12]!=0)//再收集间接块
    {
        disk_read(current_part->to_which_disk,node->i_sectors[12],all_blocks+12,1);
        block_count=140;
    }

    inode_close(node);

    /****************2、遍历所有块，寻找目录项***********************/
    block_idx =0;
    uint32_t dir_entry_idx;
    uint32_t dir_entry_size = current_part->super_b->dir_entry_size;
    uint32_t dir_entry_count=SECTOR_SIZE/dir_entry_size; //一个扇区有多少个目录项
    struct dir_entry * d_e=(struct dir_entry *)io_buf;//注意，这里是dir_buf

    while(block_idx<block_count)
    {
        if (all_blocks[block_idx] ==0)
        {
            block_idx++;
            continue;
        }

        //块地址不为空则将目录项读取进来
        memset(io_buf,0,SECTOR_SIZE);
        disk_read(current_part->to_which_disk,all_blocks[block_idx],io_buf,1);
        
        //遍历所有的目录项
        for (dir_entry_idx = 0; dir_entry_idx < dir_entry_count; dir_entry_idx++)
        {
            if ((d_e + dir_entry_idx)->i_num == child_i_num)//该目录项不是不支持的文件类型
            {
                strcat(path,"/");
                strcat(path,(d_e + dir_entry_idx)->filename);
                free_sys(all_blocks);
                return 0;
            }
        }
        block_idx ++;
    }
    free_sys(all_blocks);
    return -1;
}

//将当前工作目录的绝对路径写入 buf 中， size是buf的大小。
char * sys_getcwd(char * buf,uint32_t size)
{
    //获取线程当前工作目录
    struct task_struct * cur_task =get_current_thread();
    uint32_t cwd_i_num= cur_task->cwd_i_num;
    //如果是根目录，直接返回'/'
    if (cwd_i_num == 0)
    {
        *buf='/';
        *(buf+1)=0;
        return buf;
    }
    
    uint32_t parent_i_num;
    char path[MAX_PATH_LENGTH] = {0};
    void * io_buf = malloc_sys(512);
    if (io_buf == NULL)
    {
        return NULL;
    }

    //从后往前，直到找到根目录为止，如果是根目录，cwd_i_num为0,结束循环
    while(cwd_i_num)
    {
        //获取当前目录的父目录
        parent_i_num = parent_dir_inode_get(cwd_i_num,io_buf);
        //获取当前目录的名字
        if(child_dir_name_get(parent_i_num,cwd_i_num ,path,io_buf)==-1)
        {
            free_sys(io_buf);
            return NULL;    
        }
        cwd_i_num =parent_i_num;    
    } 

    memset(buf,0,size);
    char* last_slash;	// 用于记录字符串中最后一个斜杠地址
    while ((last_slash = strrchr(path, '/'))) {
        uint32_t len = strlen(buf);
        strcpy(buf + len, last_slash);
        *last_slash = 0;//注意，这里不是last_slash =0 ，差别很大
    }
    free_sys(io_buf);
    return buf;
}

//将当前工作目录改为pathname，成功返回0，失败返回-1
int32_t sys_chdir(const char * pathname)
{
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num=file_search(pathname,&record);
    if (i_num ==-1)
    {
       printk("directory not found in sys_chdir\r\n");
       dir_close(record.parent_dir);
       return -1; 
    }

    if (record.f_type == FT_DIRECTORY)
    {
        struct task_struct * cur_task =get_current_thread();
        cur_task->cwd_i_num = i_num;
        dir_close(record.parent_dir);
        return 0;
    }else{
        printk("the %s isn't directory in sys_chdir\r\n",pathname);
        dir_close(record.parent_dir);
        return -1;
    }
}


//将pathname对应的文件信息保存到stat中,成功时返回0，失败时返回-1
int32_t sys_stat(const char * pathname ,struct stat * stat)
{
    //如果是根目录
    if (!strcmp(pathname,"/"))
    {
        stat->i_num=0;
        stat->f_size = root_dir.inode->i_size;
        stat->f_type=FT_DIRECTORY;
        return 0;
    }
    
    //直接搜索这个文件，获取相关信息
    struct path_record record;
    memset(&record ,0 ,sizeof(struct path_record));
    int32_t i_num=file_search(pathname,&record);
    if (i_num ==-1)
    {
       printk("file not found in sys_stat\r\n");
       dir_close(record.parent_dir);
       return -1; 
    }else{
        struct inode * node=inode_open(current_part,i_num);
        stat->i_num=i_num;
        stat->f_size = node->i_size;
        stat->f_type=record.f_type;
        inode_close(node);
        dir_close(record.parent_dir);
        return 0; 
    }
}