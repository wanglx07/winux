#include <fs.h>
#include <file.h>
#include <printk.h>
#include <thread.h>
#include <bitmap.h>
#include <list.h>
#include <inode.h>
#include <dir.h>
#include <interrupt.h>
#include <assert.h>

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

//文件表
struct file file_table[MAX_FILE_OPEN];


//从文件表file_table中获取一个空闲位,成功返回下标,失败返回-1 
int free_file_slot(void)
{
    int f_idx;
    for ( f_idx=3 ; f_idx < MAX_FILE_OPEN; f_idx++) //前3个成员预留给标准输入、标准输出及标准错误
    {
        if (file_table[f_idx].fd_inode==NULL)//NULL说明空闲
        {
            return f_idx;
        }
    }
    if (f_idx==MAX_FILE_OPEN)
    {
        printk("there is no free slot in file_table\r\n");
        return -1;
    }
}

//将file_table的下标填入线程或者进程的fd_table,成功则返回fd_table的下标，否则返回-1
int fd_install(int f_idx)
{
    struct task_struct * cur_task =get_current_thread();
    int fd_idx;
    for (fd_idx = 3; fd_idx < MAX_FILES_PER_PROC_OPEN; fd_idx++)
    {
        if (cur_task->fd_table[fd_idx]==-1)//-1说明空闲
        {
            cur_task->fd_table[fd_idx] = f_idx; //注意fd_idx和f_idx的区别
            return fd_idx;//注意返回的是fd_table的下标fd_idx
        }
    }

    if (fd_idx==MAX_FILES_PER_PROC_OPEN)
    {
        printk("there is no free slot in thread's fd_table\r\n");
        return -1;
    }
}

//在inode结点位图中分配一个inode,返回i结点号
int32_t inode_bmap_alloc(struct partition * part)
{
    int32_t position =bitmap_find(&part->inode_bitmap,1);
    if(position==-1)
    {
        return -1;
    }
    bitmap_set(&part->inode_bitmap,position,1);
    return position;
}

//分配1个空闲块,返回其绝对起始扇区
int32_t block_bmap_alloc(struct partition * part)
{
    int32_t position =bitmap_find(&part->block_bitmap,1);
    if (position==-1)
    {
        return -1;
    }
    bitmap_set(&part->block_bitmap,position,1);
    return (part->super_b->data_start_sector+position); //返回可用的空闲块绝对起始扇区
}

//将内存中bitmap第bit_idx位所在的512字节同步到硬盘
void bmap_sync(struct partition * part,uint32_t bit_idx,enum bmap_type type)
{
    uint32_t off_sec = bit_idx /4096 ;//偏移多少个扇区
    uint32_t off_byte=off_sec *512;//偏移多少个字节

    uint32_t disk_bmap_start_sec ;//磁盘的待写入位置的绝对起始扇区
    uint8_t * buf_start; //内存中缓存的起始地址，注意，这里的类型是字节指针

    switch(type)
    {
        case INODE_BMAP:
            disk_bmap_start_sec=part->super_b->inode_bmap_start_sector+off_sec;
            //也就是说part结构体中超级块记录的是磁盘中的信息，inode_bitmap或者block_bitmap记录的是内存中的位图信息
            buf_start = part->inode_bitmap.ptr + off_byte; 
        break;

        case BLOCK_BMAP:  
            disk_bmap_start_sec=part->super_b->free_block_bmap_start_sector+off_sec;
            buf_start= part->block_bitmap.ptr+off_byte;
        break;   
    }
    disk_write(part->to_which_disk,disk_bmap_start_sec,buf_start, 1);
}

//在目录parent_dir 中以模式 flag 去创建普通文件 filename，成功返回文件描述符，失败返回-1
int32_t file_create(struct dir * parent_dir,char * filename,uint8_t flag)
{
    //先申请缓冲区
    void * io_buf=malloc_sys(1024);
    if (io_buf==NULL)
    {
        printk(" malloc_sys for io_buf failed in file_create\r\n");
        return -1;
    }

   /**************1、为新文件分配inode编号并创建inode*************************/
    //申请inode编号
    int32_t i_num = inode_bmap_alloc(current_part);
    if (i_num==-1)
    {
        printk("alloc inode num failed in file_create \r\n");
        return -1;
    }

    //在内存中创建个inode，后面会更新到磁盘。
    //这个文件肯定是谁都能访问的，包括内核线程或者用户进程，所以应该将这个文件的inode创建在内核空间中，
    //而且为了长久保存，应该在内核的堆空间中创建
    struct inode * new_file_node;
    struct task_struct * cur_task =get_current_thread();
    uint32_t * pdt_base_vir_temp=cur_task->pdt_base_vir;
    cur_task->pdt_base_vir=NULL; //使得malloc_sys()在内核堆空间中申请内存
    new_file_node=(struct inode *)malloc_sys(sizeof(struct inode));
    cur_task->pdt_base_vir=pdt_base_vir_temp; //恢复pdt_base_vir
    if (new_file_node==NULL)
    {
        printk("alloc inode failed in file_create \r\n");
        goto roll_inode_bmap;
    }

    //初始化这个i结点
    inode_init(i_num,new_file_node);

    /*************2、为新文件分配空闲的文件结构********************/
    //在文件数组中寻找空闲位
    int f_idx = free_file_slot();
    if (f_idx == -1)
    {
        printk("exceed max open files in file_create\r\n");
        goto roll_inode;
    }
    //初始化空闲位对应的文件结构，这样就和文件的i结点建立起了联系 
    file_table[f_idx].fd_inode = new_file_node;
    file_table[f_idx].fd_pos = 0;
    file_table[f_idx].fd_flag = flag;

    /**********************3、为新文件创建新的目录项********************/
    struct dir_entry file_dir_entry;
    memset(&file_dir_entry,0,sizeof(struct dir_entry));
    dir_entry_init(filename , i_num , FT_REGULAR , &file_dir_entry);

    /*************************同步内存数据到硬盘**************************/

    /****a 在目录parent_dir下安装目录项new_dir_entry, 写入硬盘后返回true,否则false***/
    if (!dir_entry_sync(parent_dir,&file_dir_entry,io_buf))
    {
        printk("dir_entry_sync failed in file_create \r\n");
        goto roll_file_table;
    }

    /****b 安装目录项之后，父目录i结点肯定变了，将父目录i结点的内容同步到硬盘 ***/
    memset(io_buf,0,1024);
    inode_sync(current_part,parent_dir->inode,io_buf);

    /************c 新文件的i结点的内容同步到硬盘 ***************/
    memset(io_buf,0,1024);
    inode_sync(current_part,new_file_node,io_buf);

    /************d 将inode_bitmap位图同步到硬盘 ***************/ 
    bmap_sync(current_part,i_num,INODE_BMAP);

    /************e 将创建的文件i结点添加到open_inodes链表 ***************/
    list_push(&new_file_node->inode_entry , &current_part->open_inodes_list);
    new_file_node->i_open_count =1;

    free_sys(io_buf);
    return fd_install(f_idx);

roll_file_table: 
    //将文件数组中的对应文件结构清零
    memset(&file_table[f_idx],0,sizeof(struct file));
roll_inode:
    //文件数组中没有空闲位，则释放新申请的inode
    cur_task->pdt_base_vir=NULL; //使得free_sys在内核堆空间中释放内存
    free_sys(new_file_node);
    cur_task->pdt_base_vir=pdt_base_vir_temp; //恢复pdt_base_vir
roll_inode_bmap:
    //如果新文件的i结点创建失败,之前位图中分配的i_num也要恢复
    bitmap_set(&current_part->inode_bitmap,i_num,0);

    free_sys(io_buf);
    return -1;
}


//打开已存在的文件，成功则返回文件描述符，否则返回-1
int32_t file_open( uint32_t i_num, enum open_flag flags)
{
    /*************为文件分配空闲的文件结构********************/
    //在文件数组中寻找空闲位
    int f_idx = free_file_slot();
    if (f_idx == -1)
    {
        printk("exceed max open files in file_open\r\n");
        return -1;
    }
    //初始化空闲位对应的文件结构，这样就和文件的i结点建立起了联系 
    file_table[f_idx].fd_inode = inode_open(current_part,i_num);//inode_open里面已经自动将i_open_count++了
    file_table[f_idx].fd_pos = 0;
    file_table[f_idx].fd_flag = flags;
    bool* write_deny= &file_table[f_idx].fd_inode->write_deny;

    //如果有其它进程正在写文件，则write_deny=true
    if (flags & O_WRONLY || flags & O_RDWR)
    {
        enum intr_status old_status=intr_disable();
        if (!(*write_deny))
        {
            *write_deny=true;
            intr_restore_status(old_status);
        }else{ //有进程在写，你还想写，肯定不行，返回-1
            intr_restore_status(old_status);
            printk("file write reject , other process is writing  \r\n");
            return -1;
        }
    }

    //若是读文件，不用设置write_deny
    return fd_install(f_idx);
}


//根据文件结构关闭文件，成功返回0，失败返回-1
int32_t file_close(struct file * f)
{
    if (f==NULL)
    {
        return -1;
    }
    f->fd_inode->write_deny = false;
    inode_close(f->fd_inode);
    f->fd_inode = NULL;
    return 0;
}

//把data_buf中的count个字节写入file,成功则返回写入的字节数,失败则返回-1
int32_t file_write(struct file* file, const void* data_buf, uint32_t count)
{
    if((file->fd_inode->i_size+count) > (SECTOR_SIZE *140)) // 文件目前最大只支持512*140=71680字节
    {
        printk("Maximum file size exceeded \r\n");
        return -1;
    }

    //写入count个字节前,该文件已经占用的块数，has_used_blocks相当于数组下标，从0开始
    uint32_t has_used_blocks = file->fd_inode->i_size / SECTOR_SIZE;
    //存储count字节后该文件将占用的块数,will_use_blocks相当于数组的长度
    uint32_t will_use_blocks = DIV_ROUND_UP(file->fd_inode->i_size+count,SECTOR_SIZE);

    /******************一、开始将文件所有块地址收集到all_blocks,(系统中块大小等于扇区大小),*********************/
    /******************************后面都统一在all_blocks中获取写入扇区地址*********************************/  
    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in file_write\r\n");
        return -1;
    }
    
    int32_t block_start_sector =-1;//块地址
    uint32_t block_bmap_idx=0;
    uint32_t block_idx;

    if (file->fd_inode->i_sectors[0]==0)
    {
        //分配新块
        block_start_sector= block_bmap_alloc(current_part); 
        if (block_start_sector==-1)
        {
            printk("block_bmap_alloc failed in file_write \r\n");
            free_sys(all_blocks);
            return -1;
        }
        file->fd_inode->i_sectors[0]=block_start_sector;

        //空闲块位图同步到硬盘
        block_bmap_idx = block_start_sector- current_part->super_b->data_start_sector;
        bmap_sync(current_part,block_bmap_idx,BLOCK_BMAP);
    }

    if ((will_use_blocks-1) == has_used_blocks )//不需要增加新的块来存数据
    {
        if (has_used_blocks < 12) //占用的都是直接块
        {
            //如果has_used_blocks=0，而且是第一次写，即i_sectors[0]肯定是0，需要先分配一个块
            //如果不是第一次写，i_sectors[i]是肯定存在的
            block_idx=has_used_blocks;//指向最后一个已使用的扇区
            all_blocks[block_idx] = file->fd_inode->i_sectors[block_idx];
        }else{
            //未写入新数据之前已经占用了间接块,需要将间接块地址读进来
            disk_read(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }
    }else{//需要增加新块来存数据，需要分三种情况来考虑
        if(will_use_blocks<=12)//情况一：12个直接块够用
        {
            //先将有剩余空间的可继续用的扇区地址写入all_blocks
            block_idx = has_used_blocks;
            assert(file->fd_inode->i_sectors[block_idx]!=0);
            all_blocks[block_idx]=file->fd_inode->i_sectors[block_idx];

            //再将后面要用的扇区分配好后写入all_blocks
            block_idx++;
            while(block_idx<will_use_blocks)
            {
                //分配新块
                block_start_sector= block_bmap_alloc(current_part);
                if (block_start_sector==-1)
                {
                    printk("block_bmap_alloc failed in file_write for situation_1\r\n");
                    free_sys(all_blocks);
                    return -1;
                }
                 //空闲块位图同步到硬盘
                block_bmap_idx = block_start_sector- current_part->super_b->data_start_sector;
                bmap_sync(current_part,block_bmap_idx,BLOCK_BMAP);

                assert(file->fd_inode->i_sectors[block_idx]==0);
                file->fd_inode->i_sectors[block_idx]= all_blocks[block_idx]=block_start_sector;
                block_idx++;
            }

        }else if(has_used_blocks < 12 && will_use_blocks>12){//情况二：旧数据在12个直接块内,新数据将使用间接块
            //创建一级间接块表
            block_start_sector = block_bmap_alloc(current_part);
            if (block_start_sector ==-1)
            {
                printk("block_bmap_alloc failed in file_write for situation_2\r\n");
                free_sys(all_blocks);
                return -1;
            }
            assert(file->fd_inode->i_sectors[12]==0);
            file->fd_inode->i_sectors[12]=block_start_sector;

            block_idx = has_used_blocks;// 指向旧数据所在的最后一个扇区
            all_blocks[block_idx] = file->fd_inode->i_sectors[block_idx];
            
            //分配新的扇区
            block_idx++;
            while(block_idx < will_use_blocks)
            {
                block_start_sector = block_bmap_alloc(current_part);
                if (block_start_sector ==-1)
                {
                    printk("block_bmap_alloc failed in file_write for situation_2\r\n");
                    free_sys(all_blocks);
                    return -1;
                }
                //空闲块位图同步到硬盘
                block_bmap_idx = block_start_sector- current_part->super_b->data_start_sector;
                bmap_sync(current_part,block_bmap_idx,BLOCK_BMAP);

                if (block_idx<12)// 新创建的0~11块直接存入all_blocks数组
                {
                    assert(file->fd_inode->i_sectors[block_idx]==0);
                    file->fd_inode->i_sectors[block_idx]= block_start_sector;
                    all_blocks[block_idx] = block_start_sector;
                }else{
                    all_blocks[block_idx] = block_start_sector;
                }
                block_idx++;
            }
            // 同步一级间接块表到硬盘
            disk_write(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }else if(has_used_blocks >=12){ //情况三：新数据只占据间接块
            assert(file->fd_inode->i_sectors[12]!=0);
            //获取所有间接块地址,已使用的间接块也将被读入all_blocks,无须单独收录
            disk_read(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);

            //开始分配新扇区
            block_idx = has_used_blocks+1;	  // 第一个未使用的间接块,即已经使用的间接块的下一块
            while(block_idx < will_use_blocks)
            {
                block_start_sector = block_bmap_alloc(current_part);
                if (block_start_sector ==-1)
                {
                    printk("block_bmap_alloc failed in file_write for situation_3\r\n");
                    free_sys(all_blocks);
                    return -1;
                }
                //空闲块位图同步到硬盘
                block_bmap_idx = block_start_sector- current_part->super_b->data_start_sector;
                bmap_sync(current_part,block_bmap_idx,BLOCK_BMAP);

                all_blocks[block_idx] = block_start_sector;
                block_idx++;
            }
            // 同步一级间接块表到硬盘
            disk_write(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }
    }

    /****all blocks 中包含可继续使用的、含有剩余空间的块地址，以及新数据要占用的新的块地址**/
    /*******************************二、下面开始写数据*********************************/
    uint32_t done =0;//已写入数据大小
    uint32_t each=0;//每次写入硬盘的数据块大小
    uint32_t sector_idx; 
    uint32_t sector_start; //起始扇区号
    uint32_t sector_off_bytes;//扇区已使用字节数
    uint32_t sector_left_bytes;//扇区剩余字节数
    uint32_t left_to_write = count;//待写入的数据大小，初始化为count

    uint8_t* io_buf = malloc_sys(SECTOR_SIZE);//注意指针类型是uint8_t
    if (io_buf == NULL) {
      printk("malloc for io_buf failed in file_write\r\n");
      free_sys(all_blocks);
      return -1;
    }

    while(done < count)
    {
        sector_idx = file->fd_inode->i_size / SECTOR_SIZE;
        sector_start = all_blocks[sector_idx];
        sector_off_bytes =file->fd_inode->i_size % SECTOR_SIZE;
        sector_left_bytes = SECTOR_SIZE - sector_off_bytes;
        memset(io_buf,0,SECTOR_SIZE);//清0

        //每次需要写入的数据大小
        each = left_to_write < sector_left_bytes ? left_to_write : sector_left_bytes;

        //若马上要读的块是本次操作中的第一个块，通常情况下该块中都已存在数据，
        //将数据拷贝到 io_buf 中，拼好数据，在下一行将其写入硬盘
        if (sector_idx == has_used_blocks)
        {
            disk_read(current_part->to_which_disk,sector_start,io_buf,1);
        }
        memcpy(io_buf+sector_off_bytes,data_buf+done,each);

        disk_write(current_part->to_which_disk,sector_start,io_buf, 1);
        // printk("file write at lba 0x%x\n", sector_start);    //调试,完成后去掉
        file->fd_inode->i_size+=each;// 更新文件大小
        file->fd_pos+=each;//当前文件操作的偏移地址
        done+=each;
        left_to_write-=each;
    }

    /*************************三、文件inode的i_sectors[]发生了变化，同步到硬盘****************************/
    inode_sync(current_part,file->fd_inode,io_buf);//同步文件结点到硬盘
    free_sys(io_buf);
    free_sys(all_blocks);
    return done;
}

//从文件file中读取count个字节写入buf, 返回读出的字节数,若读到文件尾则返回-1
int32_t file_read(struct file *file,void * buf,uint32_t count)
{
    //如果已经到文件尾了，返回-1
    if (file->fd_inode->i_size == file->fd_pos )
    {
        return -1;
    }

    uint32_t left_to_read = count;//剩下待读取的数据大小，初始化为count
    uint32_t need_to_read = count;//实际需要读取的数据大小，初始化为count
    //若要读取的字节数超过了文件可读的范围,待读取的数据大小left_to_read还需要校准一下
    if ((file->fd_pos + count) > file->fd_inode->i_size)
    {
        left_to_read =file->fd_inode->i_size - file->fd_pos;
        need_to_read = left_to_read;
    }

    /**************开始将文件所有块地址收集到块地址数组all_blocks*****************/
    uint32_t block_start_idx = file->fd_pos / SECTOR_SIZE; //待读的数据所在的起始块索引
    uint32_t block_end_idx = (file->fd_pos + left_to_read ) /SECTOR_SIZE; //待读的数据所在的最后一块索引

    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in file_read\r\n");
        return -1;
    }
    uint32_t block_idx ;

    if (block_start_idx == block_end_idx) //待读的数据只在一个块中，不需要跨块读取
    {
        if (block_end_idx < 12)// 待读的数据在12个直接块之内
        {
            block_idx = block_end_idx;
            all_blocks[block_idx] = file->fd_inode->i_sectors[block_idx];
        }else{// 若用到了一级间接块表,需要将表中间接块地址读进来
            disk_read(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }
    }else{ //需要跨块读取
        if(block_end_idx< 12) 
        {
            //数据都在直接块
            block_idx= block_start_idx;
            while(block_idx <= block_end_idx)
            {
                all_blocks[block_idx] =file->fd_inode->i_sectors[block_idx];
                block_idx++;
            }
        }else if(block_start_idx < 12 && block_end_idx >=12){
            //数据一部分在直接块，一部分在间接块
            block_idx = block_start_idx;
            while(block_idx <12) //先读取直接块地址
            {
                all_blocks[block_idx] =file->fd_inode->i_sectors[block_idx];
                block_idx++;
            }

            //再读取间接块地址
            assert(file->fd_inode->i_sectors[12]!=0);
            disk_read(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }else if(block_start_idx >=12){
            //数据都在间接块，将间接块地址全部读进all_blocks[]
            assert(file->fd_inode->i_sectors[12]!=0);
            disk_read(current_part->to_which_disk,file->fd_inode->i_sectors[12],all_blocks+12,1);
        }
    }

    /**************下面开始读数据*****************/
    uint32_t done=0;//已读取数据大小
    uint32_t each=0;//每次读取的数据块大小
    uint32_t sector_idx; 
    uint32_t sector_start; //起始扇区号
    uint32_t sector_off_bytes;//扇区已使用字节数
    uint32_t sector_left_bytes;//扇区剩余字节数

    uint8_t* io_buf = malloc_sys(SECTOR_SIZE);//注意指针类型是uint8_t
    if (io_buf == NULL) {
      printk("malloc for io_buf failed in file_write\r\n");
      free_sys(all_blocks);
      return -1;
    }
    while(done < need_to_read)//注意这里如果写done<count，会导致进入死循环，可以试下。
    {
        sector_idx = file->fd_pos / SECTOR_SIZE;
        sector_start = all_blocks[sector_idx];
        sector_off_bytes =file->fd_pos % SECTOR_SIZE;
        sector_left_bytes = SECTOR_SIZE - sector_off_bytes;
        memset(io_buf,0,SECTOR_SIZE);//清0

        //每次需要读取的数据大小
        each = left_to_read < sector_left_bytes ? left_to_read : sector_left_bytes;

        disk_read(current_part->to_which_disk,sector_start,io_buf,1);
        memcpy(buf+done,io_buf+sector_off_bytes,each);

        file->fd_pos+=each;
        done+=each;
        left_to_read-=each;
    }
    free_sys(all_blocks);
    free_sys(io_buf);
    return done;
}
