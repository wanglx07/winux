#include <fs.h>
#include <dir.h>
#include <inode.h>
#include <disk.h>
#include <memory.h>
#include <types.h>
#include <printk.h>
#include <assert.h>

struct dir root_dir;  //根目录

void root_dir_open(struct partition * part)
{
    root_dir.inode=inode_open(part,part->super_b->root_inode_num);
    root_dir.dir_offset=0;
}

//在分区part上打开i结点为inode_no的目录并返回目录指针,其实就是拿到某个目录对应的inode
struct dir * dir_open(struct partition * part,uint32_t i_num)
{
    struct dir * d=(struct dir *)malloc_sys(sizeof(struct dir));
    d->inode = inode_open(part,i_num);
    d->dir_offset =0  ;
    return d;
}

//关闭目录，本质就是将内存中目录对应的inode释放掉
void dir_close(struct dir * d)
{
    if (d == &root_dir)
    {
        return;//不释放根目录的inode
    }
    inode_close(d->inode);
    free_sys(d);//这里要释放掉，在dir_open中在堆内存申请的
}

//初始化某个目录项
void dir_entry_init(char * filename,uint32_t i_num,enum file_type type,struct dir_entry * dir_e)
{
    strcpy(dir_e->filename,filename);
    dir_e->i_num = i_num;
    dir_e->f_type = type;
}

//在part分区内的d目录中搜索名为name的文件或者目录，找到后返回true并将目录项存入dir_e，否则返回false
bool dir_entry_search(struct partition * part,struct dir * d,const char * name,struct dir_entry * dir_e)
{
    uint32_t block_count=140; //12个直接块地址+128个间接块地址
    //注意这里的指针类型是uint32_t，不能是uint8_t，因为blocks_buf存的是地址，每次移动四个字节
    uint32_t * blocks_buf=(uint32_t*)malloc_sys(140*4);//140个地址需要占用560个字节
    if (blocks_buf==NULL)
    {
        printk("dir_entry_search alloc memory failed \r\n");
        return false;
    }

    //将目录的直接块填进去
    int i=0;
    while(i<12)
    {
        //如果blocks_buf是uint8_t *类型，在这里就会出错
        blocks_buf[i] = d->inode->i_sectors[i];
        i++;
    }

    if (d->inode->i_sectors[12]!=0)//如果含有一级间接块表
    {
        //将i_sectors[12]指向128个间接块地址(总共一个扇区大小)拷贝到blocks_buf中，这样这个inode的所有块地址都保存在blocks_buf中了，后面方便操作
        disk_read(part->to_which_disk,d->inode->i_sectors[12],blocks_buf+12,1);
    }

    //开始在这些块中寻找目录项
    uint32_t dir_entry_size = part->super_b->dir_entry_size;
    uint32_t dir_entry_count=SECTOR_SIZE/dir_entry_size; //一个扇区有多少个目录项
    uint8_t * buf=(uint8_t*)malloc_sys(SECTOR_SIZE);
    struct dir_entry * d_e=(struct dir_entry *)buf;
    int j;
    for (i = 0; i < block_count; i++)
    {
        if (blocks_buf[i]==0) //块地址为0，说明这个块没有数据
        {
            continue;
        }
        disk_read(part->to_which_disk,blocks_buf[i],buf,1);
        //遍历扇区中所有目录项
        for (j = 0; j < dir_entry_count; j++)
        {
            if(!strcmp(name,d_e->filename))//找到对应的目录项了
            {
                memcpy(dir_e,d_e,dir_entry_size);
                free_sys(buf);
                free_sys(blocks_buf);
                return true;
            }
            d_e++;
        }
        d_e=(struct dir_entry *)buf;//此时d_e已经指向扇区内最后一个目录项,使其重新指向buf开头
        memset(buf,0,SECTOR_SIZE);//将buf清0,以便遍历到下一个块的时候使用
    }   

    free_sys(buf);
    free_sys(blocks_buf);
    return false;

}

//将目录项dir_e写入父目录parent_dir中,io_buf由主调函数提供
//注意这里面如果有间接块，如果目录项是插入间接块，那么会将间接块更新到内存。
//但是还没有将parent_dir->inode更新到内存，所以后面应该还要通过inode_sync()进行更新
//如果是将目录项插入直接块，那么不会马上更新到内存，是通过后面的inode_sync()进行更新的，因为直接块是位于inode的i_sectors[0-11]中
bool dir_entry_sync(struct dir * parent_dir,struct dir_entry* dir_e ,void * io_buf)
{
    struct inode* dir_inode=parent_dir->inode;
    uint32_t dir_size = dir_inode->i_size;
    uint32_t dir_entry_size = current_part->super_b->dir_entry_size;

    assert(dir_size%dir_entry_size==0);

    //申请一个能装140个块地址的缓存blocks_buf，将parent_dir->inode->i_sectors的块地址
    //存进这个临时缓存blocks_buf，后面方便操作

    uint32_t * blocks_buf=(uint32_t*)malloc_sys(140*4);//140个地址需要占用560个字节
    if (blocks_buf==NULL)
    {
        printk("dir_entry_sync alloc memory for blocks_buf failed \r\n");
        return false;
    }

    memset(blocks_buf,0,140*4);//清0

    //将目录的12个直接块和一级间接块表，
    int i=0;
    while(i<12)
    {
        blocks_buf[i] = dir_inode->i_sectors[i];
        i++;
    }
    //注意这里不能只拷贝前12项，因为不读取一级间接块表的话，每次第12项都为0
    if (dir_inode->i_sectors[12]!=0)//如果含有一级间接块表
    {
        //将i_sectors[12]指向128个间接块地址(总共一个扇区大小)拷贝到blocks_buf中，这样这个inode的所有块地址都保存在blocks_buf中了，后面方便操作
        disk_read(current_part->to_which_disk,dir_inode->i_sectors[12],blocks_buf+12,1);
    }

    // 开始遍历所有块以寻找目录项空位,若已有扇区中没有空闲位,在不超过文件大小的情况下申请新扇区来存储新目录项 
    uint32_t block_idx;
    int block_start_sector=-1;
    int block_bitmap_idx=-1;
    struct dir_entry * entry_p=(struct dir_entry *)io_buf;
    uint32_t entry_count_per_sec = 512/ dir_entry_size;//每个扇区最多容纳的目录项数目
    for (block_idx = 0; block_idx < 140; block_idx++)
    {
        block_bitmap_idx=-1;
        if (blocks_buf[block_idx]==0) //块不存在
        {
            block_start_sector = block_bmap_alloc(current_part); //分配新的块

            if (block_start_sector==-1)//分配失败
            {
                printk("dir_entry_sync alloc block_bmap  failed \r\n");
                free_sys(blocks_buf);
                return false;
            }

            //每分配一个块就同步一次block_bitmap
            block_bitmap_idx = block_start_sector - current_part->super_b->data_start_sector;//偏移量
            bmap_sync(current_part,block_bitmap_idx,BLOCK_BMAP);//将块位图同步到硬盘

            block_bitmap_idx=-1;

            //将新块的地址填进inode的块地址数组i_sectors
            if(block_idx<12)//如果是直接块为空
            {
                dir_inode->i_sectors[block_idx]=blocks_buf[block_idx]=block_start_sector;
            }else if(block_idx==12){ //如果是一级间接块表为空
                dir_inode->i_sectors[12]=block_start_sector; // 将上面分配的块做为一级间接块表地址
                block_start_sector=-1;
                block_start_sector=block_bmap_alloc(current_part);// 再分配一个块做为第0个间接块

                if (block_start_sector==-1)//分配失败，回滚操作
                {
                    block_bitmap_idx=dir_inode->i_sectors[12]-current_part->super_b->data_start_sector;
                    bitmap_set(&current_part->block_bitmap,block_bitmap_idx,0);//将间接块表所占的块释放掉
                    dir_inode->i_sectors[12]=0;
                    printk("dir_entry_sync alloc block_bmap  for i_sectors[12] failed \r\n");
                    free_sys(blocks_buf);
                    return false;
                }

                //每分配一个块就同步一次block_bitmap
                block_bitmap_idx = block_start_sector - current_part->super_b->data_start_sector;//偏移量
                bmap_sync(current_part,block_bitmap_idx,BLOCK_BMAP);//将块位图同步到硬盘
               
                blocks_buf[12]=block_start_sector;
                //把新分配的第0个间接块地址写入一级间接块表
                disk_write(current_part->to_which_disk,dir_inode->i_sectors[12],blocks_buf+12,1);
            }else{
                blocks_buf[block_idx]=block_start_sector;
                 //把新分配的间接块地址写入一级间接块表
                disk_write(current_part->to_which_disk,dir_inode->i_sectors[12],blocks_buf+12,1);
            }

            //再将新目录项dir_e写入新分配的间接块
            memset(io_buf,0,512);
            memcpy(io_buf,dir_e,dir_entry_size);
            disk_write(current_part->to_which_disk,blocks_buf[block_idx],io_buf,1);
            dir_inode->i_size+=dir_entry_size;
            free_sys(blocks_buf);
            return true;
        }
        //若第block_idx块已存在,将其读进内存,然后在该块中查找空目录项
        disk_read(current_part->to_which_disk,blocks_buf[block_idx],io_buf,1);
        //在扇区查找空目录项
        int j=0;
        while(j<entry_count_per_sec)
        {
            //注意这里如果不使用entry_p+j而是使用entry_p的话
            //后面需要令entry_p++,然后最后还要将entry_p重新指向io_buf，不然下次用不了
            if ((entry_p+j)->f_type==FT_UNKNOWN)// FT_UNKNOWN为0,无论是初始化或是删除文件后,都会将f_type置为FT_UNKNOWN.
            {
                memcpy(entry_p+j,dir_e,dir_entry_size);
                //这里有entry_p和io_buf都是指向同一个块，entry_p会移动去寻找空闲的目录项,io_buf是固定不动的，以便disk_write
                disk_write(current_part->to_which_disk,blocks_buf[block_idx],io_buf,1);
                dir_inode->i_size+=dir_entry_size;
                free_sys(blocks_buf);
                return true;
            }
            j++;
        }
    }
    printk("directory is full \r\n");
    free_sys(blocks_buf);
    return false;
}

//把分区part的目录dir中编号为i_num的目录项删除
bool dir_entry_delete(struct partition * part ,struct dir * dir ,uint32_t i_num ,void * io_buf)
{
    struct inode * node = dir->inode;

    /****************1、收集目录占用的块到all_blocks***********************/
    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in inode_release\r\n");
        return -1;
    }

    uint32_t block_idx=0;
    uint32_t block_count=12;
    while(block_idx<12)//先将前12个直接块存入all_blocks
    {
        all_blocks[block_idx] =node->i_sectors[block_idx];
        block_idx++;
    }

    if (node->i_sectors[12]!=0)//再收集间接块
    {
        disk_read(part->to_which_disk,node->i_sectors[12],all_blocks+12,1);
        block_count=140;
    }

    /****************2、遍历所有块，寻找目录项***********************/
    block_idx =0;
    uint32_t dir_entry_idx;
    uint32_t dir_entry_size = part->super_b->dir_entry_size;
    uint32_t dir_entry_count=SECTOR_SIZE/dir_entry_size; //一个扇区有多少个目录项
    struct dir_entry * d_e=(struct dir_entry *)io_buf;
    bool first_block=false;//根目录'.' 、 '..'所在的块？
    uint32_t entry_other_count=0;
    struct dir_entry * found=NULL;
    uint32_t bmap_idx;

    while(block_idx<block_count)
    {
        if (all_blocks[block_idx] ==0)
        {
            block_idx++;
            continue;
        }
        //块地址不为空则将目录项读取进来
        memset(io_buf,0,SECTOR_SIZE);
        disk_read(current_part->to_which_disk,all_blocks[block_idx],io_buf,1);
        entry_other_count=0; //要置0
        first_block=false;
        
        //遍历所有的目录项，看是否存在待删除的目录项，而且是不是只存在一个目录项
        for (dir_entry_idx = 0; dir_entry_idx < dir_entry_count; dir_entry_idx++)
        {
            if ((d_e + dir_entry_idx)->f_type != FT_UNKNOWN)//该目录项不是不支持的文件类型
            {
                if (!strcmp((d_e + dir_entry_idx)->filename,".")) //根目录的目录项
                {
                    first_block=true;//说明这个块是分配给根目录项的，是不能释放的
                }else if(strcmp((d_e + dir_entry_idx)->filename,".")&&strcmp((d_e + dir_entry_idx)->filename,"..")){
                    if ((d_e + dir_entry_idx)->i_num == i_num)//如果是我们要找的目录项，则记录下来
                    {
                        assert(found==NULL);
                        found =d_e + dir_entry_idx;
                    }
                    //统计除了'.'、'..'之外的所有目录项
                    entry_other_count++;
                }
            }
        }

        //遍历完这个块所有目录项之后还是没找到我们要找的目录项，则继续遍历其它块
        if (found == NULL)
        {
            block_idx++;
            continue;
        }

        /****************3、到这里，说明found不为null，说明找到目录项了，下面开始处理***********************/
        if(dir_entry_count==1 && !first_block)//如果这个块不是第一块(即根目录项所在的块)，并且这个块上只有现在这么一个目录项，回收这个块
        {
            //在块位图中将这个块对应的位清0
            bmap_idx = all_blocks[block_idx] - part->super_b->data_start_sector;
            bitmap_set(&part->block_bitmap,bmap_idx,0);
            bmap_sync(part,bmap_idx,BLOCK_BMAP);

            //在目录结点的直接块i_sectors或者间接块索引表中删除这个块地址
            if (block_idx<12)
            {
                node->i_sectors[block_idx]=0;
            }else{// 间接块索引表中删除这个块地址
                //先判断一级间接索引表中间接块的数量,如果仅有这1个间接块,连同间接索引表所在的块一同回收
                uint32_t indirect_block_count =0;//间接块的数量
                uint32_t indirect_block_idx ;
                for (indirect_block_idx =12; indirect_block_idx < 140; indirect_block_idx++)
                {
                    if (all_blocks[indirect_block_idx]!=0)
                    {
                        indirect_block_count++; 
                    }
                }

                if (indirect_block_count >1) //不止一块间接块,仅在索引表中擦除当前这个间接块地址
                {
                    all_blocks[block_idx] =0;
                    disk_write(part->to_which_disk,node->i_sectors[12],all_blocks+12,1);
                }else{// 间接索引表中就当前这1个间接块,直接把间接索引表所在的块回收,然后擦除间接索引表块地址
                    //注意不用回收间接块再回收间接块索引表，而是直接回收间接块索引表
                    bmap_idx= node->i_sectors[12] - part->super_b->data_start_sector;
                    bitmap_set(&part->block_bitmap,bmap_idx,0);
                    bmap_sync(part,bmap_idx,BLOCK_BMAP);

                    node->i_sectors[12]=0; //清0
                }
            }
        }else{// 仅将该目录项清空
            memset(found,0,dir_entry_size);
            disk_write(part->to_which_disk,all_blocks[block_idx],io_buf,1);
        }

        /****************4、更新目录inode到磁盘***********************/
        node->i_size -=dir_entry_size;
        memset(io_buf,0,512*2);
        inode_sync(part,node,io_buf);
        free_sys(all_blocks);
        return true;
    }
    free_sys(all_blocks);
    return false;
}

//读取目录,成功返回1个目录项,失败返回NULL
struct dir_entry * dir_read(struct dir * dir)
{
    struct inode * node = dir->inode;

    /****************1、收集目录占用的块到all_blocks***********************/
    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in dir_read\r\n");
        return -1;
    }

    uint32_t block_idx=0;
    uint32_t block_count=12;
    while(block_idx<12)//先将前12个直接块存入all_blocks
    {
        all_blocks[block_idx] =node->i_sectors[block_idx];
        block_idx++;
    }

    if (node->i_sectors[12]!=0)//再收集间接块
    {
        disk_read(current_part->to_which_disk,node->i_sectors[12],all_blocks+12,1);
        block_count=140;
    }

    /****************2、遍历所有块，寻找目录项***********************/
    block_idx =0;
    uint32_t dir_entry_idx;
    uint32_t dir_entry_size = current_part->super_b->dir_entry_size;
    uint32_t dir_entry_count=SECTOR_SIZE/dir_entry_size; //一个扇区有多少个目录项
    struct dir_entry * d_e=(struct dir_entry *)dir->dir_buf;//注意，这里是dir_buf
    uint32_t entry_has_read =0 ;//已经读取的目录项个数

    while(block_idx<block_count)
    {
        if (dir->dir_offset >= node->i_size)//说明己经遍历了所有的目录项
        {
            return NULL;
        }

        if (all_blocks[block_idx] ==0)
        {
            block_idx++;
            continue;
        }

        //块地址不为空则将目录项读取进来
        memset(d_e,0,SECTOR_SIZE);
        disk_read(current_part->to_which_disk,all_blocks[block_idx],d_e,1);
        
        //遍历所有的目录项
        for (dir_entry_idx = 0; dir_entry_idx < dir_entry_count; dir_entry_idx++)
        {
            if ((d_e + dir_entry_idx)->f_type != FT_UNKNOWN)//该目录项不是不支持的文件类型
            {
                //注意，因为每次遍历目录项的时候，只要一找到目录项就return，
                //下次进来又会从头开始遍历，但是前面的目录项己经被读取过了，
                //没必要再读取，所以通过dir->dir_offset我们可以算出已经
                //返回多少个目录项了，这样跳过已经读取的entry_has_read个目录项，
                //避免重复读取
                if (entry_has_read < (dir->dir_offset /dir_entry_size))
                {
                    entry_has_read ++;
                    continue;
                }
                dir->dir_offset += dir_entry_size;
                free_sys(all_blocks);
                return d_e + dir_entry_idx;
            }
        }
        block_idx ++;
    }
    free_sys(all_blocks);
    return NULL;
}

//判断目录是否为空，若只有'.' 、 '..'两个目录项，则目录为空，返回true，否则false
bool dir_empty(struct dir * dir)
{
    if (dir->inode->i_size == current_part->super_b->dir_entry_size *2)
    {
        return true;
    }
    return false;
}

//在父目录parent_dir删除空目录child_dir，注意是空目录;可以和fs.c的sys_unlink()文件的删除进行对比
int32_t dir_remove(struct dir * parent_dir,struct dir * child_dir)
{
    int32_t block_idx;
    for ( block_idx=1;block_idx<=12;block_idx++)
    {
        assert(child_dir->inode->i_sectors[block_idx]==0);//空目录的i_sectors[1~12]都为0
    }

    void * io_buf=(void*)malloc_sys(1024);
    if (io_buf== NULL)
    {
        printk("malloc for io_buf failed in dir_remove\r\n");
        return -1;
    }

    //在父目录parent_dir中删除子目录child_dir对应的目录项
    dir_entry_delete(current_part ,parent_dir ,child_dir->inode->i_num ,io_buf);
    inode_release(current_part,child_dir->inode->i_num);
    free_sys(io_buf);
    return 0;
}