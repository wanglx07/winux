#include <fs.h>
#include <inode.h>
#include <disk.h>
#include <assert.h>
#include <string.h>
#include <list.h>
#include <interrupt.h>
#include <thread.h>
#include <file.h>

//表示inode在磁盘上的位置
struct inode_position{
    bool two_sec; //是否跨扇区
    uint32_t start_sector;//所在的绝对扇区号
    uint32_t off_byte;//在扇区内的字节偏移，即第几个字节
};

//根据inode标号获取inode所在的扇区和扇区内的偏移量并记录在i_pos中
void inode_locate(struct partition * part,uint32_t i_num,struct inode_position * i_pos)
{
    uint32_t inode_table_start_secotr = part->super_b->inode_table_start_secotr;
    uint32_t off_sec = i_num * sizeof(struct inode) / 512; //第i_num个inode相对于inode_table_start_secotr的扇区偏移量
    uint32_t off_byte = i_num * sizeof(struct inode) % 512; //在扇区内的字节偏移

    //判断是否跨越两个扇区
    uint32_t left_size = 512 - off_byte;
    if (left_size< sizeof(struct inode)) //剩下的区域小于一个inode大小，那么inode就得跨两个扇区来放
    {
        i_pos->two_sec=true;
    }else{
        i_pos->two_sec=false;
    }

    i_pos->start_sector = inode_table_start_secotr + off_sec;
    i_pos->off_byte = off_byte;
}

//将inode同步到分区part，缓冲区io_buf由主调函数提供。其实就是将内存中inode的信息更新到磁盘中的inode结点
void inode_sync(struct partition * part ,struct inode * node,void * io_buf)
{
    //先确定inode的扇区地址，也就是说得知道往硬盘哪里写
    struct inode_position i_pos;
    inode_locate(part , node->i_num ,&i_pos);
    assert(i_pos.start_sector <= (part->start_sector+part->sector_count));
    
    struct inode temp_inode;
    memcpy(&temp_inode,node,sizeof(struct inode));

    //以下inode的三个成员只存在于内存中,现在将inode同步到硬盘,清掉这三项即可。由fs.c的create_fs()中第4.2步可以看出
    temp_inode.i_open_count=0;
    temp_inode.write_deny=false;
    temp_inode.inode_entry.prev=temp_inode.inode_entry.next=NULL;

    if(i_pos.two_sec)
    {
        //先读两个扇区
        disk_read(part->to_which_disk,i_pos.start_sector,io_buf,2);
        //替换原来inode的旧信息
        memcpy((char*)io_buf+i_pos.off_byte,&temp_inode,sizeof(struct inode));
        //再重新写回磁盘
        disk_write(part->to_which_disk,i_pos.start_sector,io_buf,2);
    }else{
        //先读一个扇区
        disk_read(part->to_which_disk,i_pos.start_sector,io_buf,1);
        //替换原来inode的旧信息
        memcpy((char*)io_buf+i_pos.off_byte,&temp_inode,sizeof(struct inode));
        //再重新写回磁盘
        disk_write(part->to_which_disk,i_pos.start_sector,io_buf,1);
    }
}

//根据i结点号返回对应的i结点，其实就是根据i结点号提取磁盘的inode信息填进内存的inode结点
struct inode* inode_open(struct partition * part ,uint32_t i_num)
{
    //1、先在此缓存中查找该 inode ，找到后则直接返回 inode 指针，
    struct inode * node ;
    list_for_each_entry(node,&part->open_inodes_list,inode_entry)
    {
        if (node->i_num == i_num)
        {
            node->i_open_count++;
            return node;
        }
    }

    //2、若未找到，再从磁盘上加载该 inode 到此缓存中并且加入到open_inodes_list中，然后再返回其指针

    //先确定inode的位置
    struct inode_position i_pos;
    inode_locate(part,i_num,&i_pos);

    //这个文件肯定是谁都能访问的，包括内核线程或者用户进程，所以应该将这个文件的inode创建在内核空间中，
    //而且为了长久保存，应该在内核的堆空间中创建
    struct task_struct * cur_task =get_current_thread();
    uint32_t * pdt_base_vir_temp=cur_task->pdt_base_vir;
    cur_task->pdt_base_vir=NULL; //使得malloc_sys()在内核堆空间中申请内存
    node=(struct node *)malloc_sys(sizeof(struct inode));
    cur_task->pdt_base_vir=pdt_base_vir_temp; //恢复pdt_base_vir

    //开始读取磁盘
    char * inode_buf;
    if (i_pos.two_sec)
    {
        inode_buf=(char *)malloc_sys(512*2);
        disk_read(part->to_which_disk,i_pos.start_sector,inode_buf,2);
    }else{
        inode_buf=(char *)malloc_sys(512);
        disk_read(part->to_which_disk,i_pos.start_sector,inode_buf,1);
    }
    //硬盘的读写单位是扇区，我们想要的 inode 还混在这些扇区中,所以将磁盘读到的扇区的inode提取到node中
    memcpy(node,inode_buf+i_pos.off_byte,sizeof(struct inode));

    //插入在前面，以便下次更快被找到
    list_push(&node->inode_entry,&part->open_inodes_list);
    //标志为已使用一次
    node->i_open_count=1;

    free_sys(inode_buf);
    return node;
}

//释放掉inode或减少inode的打开次数
void inode_close(struct inode *node)
{
    enum intr_status old_status=intr_disable();
    node->i_open_count--;
    //若没有进程再打开此文件,将此inode去掉并释放空间
    if (node->i_open_count==0)
    {
        list_del(&node->inode_entry);
        struct task_struct * cur_task =get_current_thread();
        uint32_t * pdt_base_vir_temp=cur_task->pdt_base_vir;
        cur_task->pdt_base_vir=NULL; //使得free_sys在内核堆空间中释放内存
        free_sys(node);
        cur_task->pdt_base_vir=pdt_base_vir_temp; //恢复pdt_base_vir
    }
    intr_restore_status(old_status);
}

//初始化某一个inode
void inode_init(uint32_t i_num,struct inode * new_node)
{
    new_node->i_num = i_num;
    new_node->i_size = 0;
    new_node->i_open_count=0;
    new_node->write_deny=false;

    //初始化块索引数组
    int i=0;
    for (; i < 13; i++)
    {
        new_node->i_sectors[i]=0;
    }
}

//将硬盘上inode_table对应的inode项清空
void inode_delete(struct partition * part,uint32_t i_num,void * io_buf)
{
    struct inode_position i_pos;
    inode_locate(part,i_num,&i_pos);//得先知道inode的位置

    if(i_pos.two_sec)
    {
        //先读两个扇区
        disk_read(part->to_which_disk,i_pos.start_sector,io_buf,2);
        //将对应的inode项清0
        memset((char*)io_buf+i_pos.off_byte,0,sizeof(struct inode));
        //再重新写回磁盘
        disk_write(part->to_which_disk,i_pos.start_sector,io_buf,2);
    }else{
        //先读一个扇区
        disk_read(part->to_which_disk,i_pos.start_sector,io_buf,1);
        //将对应的inode项清0
        memset((char*)io_buf+i_pos.off_byte,0,sizeof(struct inode));
        //再重新写回磁盘
        disk_write(part->to_which_disk,i_pos.start_sector,io_buf,1);
    }
}

//回收inode对应的资源
void inode_release(struct partition * part , uint32_t i_num)
{
    struct inode * node = inode_open(part , i_num);

    /********1、收集inode占用的块到all_blocks**********/
    uint32_t * all_blocks= (uint32_t *)malloc_sys(140*4);
    if (all_blocks==NULL)
    {
        printk("malloc for all_blocks failed in inode_release\r\n");
        return -1;
    }

    uint32_t block_idx=0;
    uint32_t block_count=12;
    while(block_idx<12)//先将前12个直接块存入all_blocks
    {
        all_blocks[block_idx] =node->i_sectors[block_idx];
        block_idx++;
    }

    if (node->i_sectors[12]!=0)//再收集间接块
    {
        disk_read(part->to_which_disk,node->i_sectors[12],all_blocks+12,1);
        block_count=140;
    }

    /********2、设置空闲块位图，回收直接块和间接块**********/
    block_idx =0;
    uint32_t bmap_idx;
    while(block_idx<block_count)
    {
        if (all_blocks[block_idx] !=0)
        {
            bmap_idx = all_blocks[block_idx] - part->super_b->data_start_sector;
            assert(bmap_idx>0);
            bitmap_set(&part->block_bitmap,bmap_idx,0);//内存中bitmap对应位清0
            bmap_sync(part,bmap_idx,BLOCK_BMAP);//同步到磁盘
        }
        block_idx++;
    }

    /********3、回收一级间接块表占用的块**********/
    if (node->i_sectors[12]!=0){
        bmap_idx = node->i_sectors[12]-part->super_b->data_start_sector;
        assert(bmap_idx>0);
        bitmap_set(&part->block_bitmap,bmap_idx,0);//内存中bitmap对应位清0
        bmap_sync(part,bmap_idx,BLOCK_BMAP);//同步到磁盘
    }

     /********4、回收inode位图中inode自身**********/
    bitmap_set(&part->inode_bitmap,i_num,0);//内存中bitmap对应位清0
    bmap_sync(part,i_num,INODE_BMAP);//同步到磁盘

    /**************5、将inode_table上的对应inode清空**********/
    void * io_buf = malloc_sys(512*2);
    inode_delete(part,i_num,io_buf);

    /**************6、释放内存中inode自身******************/
    inode_close(node);

    free_sys(io_buf);
    free_sys(all_blocks);
}