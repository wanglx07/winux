[SECTION mbr align=16 vstart=0x7c00]
jmp start 

boot_text db 'booting...',0x0a,0x0d,0
loader_base dd  0x1000 ;loader被加载的物理起始地址
loader_sector_start equ 2 ;loader在硬盘的第2扇区

%include "16lib.inc"

;------------------------------
;从硬盘读取一个逻辑扇区
;输入di:si=起始逻辑扇区
;ds:bx=目标缓冲区位置
read_disk:  

        push ax
        push bx 
        push cx 
        push dx 

        mov dx,0x1f2
        mov al,1 
        out dx,al ;读取的扇区数

        inc dx ;0x1f3
        mov ax,si 
        out dx,al ;LBA地址0~7位

        inc dx ;0x1f4
        mov al,ah 
        out dx,al ;LBA地址8~15位

        inc dx  ;0x1f5
        mov ax,di 
        out dx,al ;LBA地址16~23位

        inc dx  ;0x1f6
        mov al,0xe0 
        or al,ah 
        out dx,al ;LBA地址24~27位

        inc dx ;0x1f7
        mov al,0x20 ;读命令
        out dx,al 

.wait:
        in al,dx 
        and al,0x88
        cmp al,0x08 ;不忙，且硬盘已准备好数据传输
        jnz .wait

        mov cx,256 ;要读取的次数，这里每次读取2个字节，所以256*2=512字节
        mov dx,0x1f0 
.readw:
        in ax,dx ;注意0x1f0是16位端口
        mov [bx],ax 
        add bx,2 
        loop .readw

        pop dx 
        pop cx 
        pop bx 
        pop ax 
        
        ret 

;-------------------------------------------------
start:
        mov ax,cs 
        mov ds,ax 
        mov ss,ax 
        mov sp,0x7c00
        
        ;清屏
        mov ax,3
        int 0x10 

        ;打印字符串booting...
        mov bx,boot_text
        call print

        ;读取内核加载器loader
        mov ax,[cs:loader_base] ;计算用于加载用户程序的逻辑段地址
        mov dx,[cs:loader_base+0x02]
        mov bx,16
        div bx
        mov ds,ax ;段地址赋给ds,read_disk要用到
        mov es,ax

        xor di,di
        mov si,loader_sector_start ;程序在硬盘上的起始逻辑扇区号
        xor bx,bx ;加载到DS:0x0000处

        mov cx,3
.read_from_disk:
        call read_disk
        add bx,0x200 ;记得要加上0x200以便保存下一个扇区的数据
        xor di,di
        inc si 
        loop .read_from_disk

       jmp [cs:loader_base];跳到loader执行  
        
times 510-($-$$) db 0
    db 0x55,0xaa 

