[SECTION code  vstart=0x1000]
    jmp start 

    loadText db 'loading ...',0x0d,0x0a,0
    memDetOk db 'memory_detect_ok',0x0d,0x0a,0
    memDetFail db 'memory_detect_fail',0x0d,0x0a,0 

    ards_count dw 0 ;用于记录 ARDS 结构体数量
    ards_buf: times 256 db 0

    loader_base equ 0x1000

    pgdt dw 0
         dd 0x00007000 ;GDT的物理地址,覆盖不再使用的mbr，并刚好使其4K对齐

    code_seg_selector equ (1<<3) ;代码段选择子
    data_seg_selector equ (2<<3)  ;数据段选择子
    vedio_seg_selector equ (3<<3) ;视频显示缓冲区的段选择子

    pdt_base equ 0x100000 ;页目录表基地址
    pt_base equ 0x101000 ;页表基地址

    elf_base equ 0x00050000 ;可执行文件基地址
    elf_sector_start equ 10 ;从第10个扇区开始读内核

    KENRNEL_ENTRY_POINT equ 0xc0030000

    %include "16lib.inc"
;----------------------------------------------
start:
    xor ax,ax
    mov ds,ax 
    mov es,ax

    mov bx,loadText 
    call print 

;--------------------------------------------
;内存检测
;--------------------------------------------
    xor ebx,ebx     ;初始化为0，后面用来保存下一个ARDS的地址
    mov edx, 0x534d4150     ;固定签名“SMAP”
    mov di,ards_buf     ;es:di 指向准备写入ARDS的缓冲区地址

.detect_loop:
    mov eax,0x0000e820      ;功能号，每执行一次中断都会发生变化，所以需要更新
    mov ecx,20  ;ARDS结构体的大小
    int 0x15    ;得到ARDS
    jc  .mem_detc_fail  ;CF=1表示出现错误
    add di,20   ;使得es:di指向缓存区下一个ARDS的位置
    inc word [ards_count]   ;记录 ARDS 数量
    cmp ebx,0   ;ebx为0，说明当前ARDS已经是最后一个
    jnz .detect_loop
    jmp .mem_detc_done

.mem_detc_fail:
    mov bx,memDetFail
    call print
    jmp $

.mem_detc_done:
    mov bx,memDetOk
    call print
    
    mov cx,[ards_count]
    mov si,0 
.show_ards:
    mov eax,[ards_buf+si] ;内存基地址
    mov ebx,[ards_buf+si+8] ;内存长度
    mov edx,[ards_buf+si+16] ;内存类型
    add si,20 
    loop .show_ards

;--------------------------------------------
;进入保护模式
;--------------------------------------------
    ;计算GDT所在的逻辑段地址
    mov eax,[cs:pgdt+0x02] ;GDT的32位线性基地址
    xor edx,edx
    mov ebx,16
    div ebx ;分解成16位逻辑地址

    mov ds,eax ;令DS指向该段以进行操作
    mov ebx,edx ;段内起始偏移地址

    ;创建0#描述符，它是空描述符，这是处理器的要求
    mov dword [ebx+0x00],0x00
    mov dword [ebx+0x04],0x00

    ;创建#1描述符，保护模式下的代码段描述符
    mov dword [ebx+0x08],0x0000FFFF;段基址0x00000000,段界限0xFFFFF
    mov dword [ebx+0x0c],0x00CF9800 ;G=1,D=1,L=0,AVL=0 P=1,DPL=00,S=1 type=1000(;x=l,c=0,r=0,a=0 代码段是可执行的，非一致性，不可读，巳访问位清0) 

    ;创建#2描述符，保护模式下的数据段描述符
    mov dword [ebx+0x10],0x0000FFFF;段基址0x00000000,段界限0xFFFFF
    mov dword [ebx+0x14],0x00CF9200 ;G=1,D=1,L=0,AVL=0 P=1,DPL=00,S=1 type=0010(;x=0,e=0,w=1,a=0 数据段是不可执行的，向上扩展的，可写，己访问位清0) 

    ;创建#3描述符，文本模式下的显示缓冲区
    mov dword [ebx+0x18],0x8000ffff;段基址0x000b8000,段界限0x0FFFF
    mov dword [ebx+0x1c],0x0040920b;G=0,D=1,L=0,AVL=0 P=1,DPL=00,S=1 type=0010

    ;初始化描述符表寄存器GDTR
    mov word [cs: pgdt],31 ;描述符表的界限（总字节数减一）

    lgdt [cs:pgdt]

    ;打开A20
    in al,0x92  ;南桥芯片内的端口
    or al,0000_0010B
    out 0x92,al

    cli ;保护模式下中断机制尚未建立，应禁止中断

    mov eax,cr0
    or eax,1
    mov cr0,eax ;设置PE位

    ;刷新流水线
    jmp dword code_seg_selector:go_in_protect_mode

    [bits 32]

    %include "32lib.inc"
;--------------------------------------------
;进入保护模式
;--------------------------------------------
    go_in_protect_mode:
        mov eax,vedio_seg_selector
        mov fs,eax ;将显存段选择子赋给fs
        mov eax,data_seg_selector ;使用向上拓展的数据段作为栈段
        mov ds,eax
        mov es,eax
        mov ss,eax 
        mov esp,loader_base
        
        mov byte [fs:0x1e0],'P' ;在屏幕上显示一个字母P表示已进入了保护模式

;--------------------------------------------
;加载内核
;--------------------------------------------
    ;读取kernel文件
    mov eax,10 ;起始逻辑扇区号
    mov ebx,elf_base
    mov ecx,200 ;读取200个扇区

.read_from_disk:
    call read_disk
    inc eax 
    loop .read_from_disk 

;--------------------------------------------
;打开分页机制
;--------------------------------------------
    ;建内核的页目录表和页表----------------
    ;为打开分页模式做准备
    ;页目录表清0
    mov ecx,1024 ;1024个目录项
    mov ebx,pdt_base ;页目录的物理地址
    xor esi,esi 

.clear_pdt:
    mov dword [es:ebx+esi],0x00000000 ;页目录表项清零
    add esi,4
    loop .clear_pdt

    ;在页目录内创建指向页目录自己的目录项
    mov eax,pdt_base 
    or eax,0x7 ;地址为0x20000，US=1,RW=1,P=1
    mov [es:ebx+4092],eax 

    ;在页目录内创建与线性地址0x00000000对应的目录项
    mov eax,pt_base
    or eax,0x7  ;地址为0x21000
    mov [es:ebx+0x0],eax 

    ;创建第1个目录项对应的页表，并初始化页表项
    xor eax,eax  ;起始页的物理地址，从0开始分配
    mov ebx,pt_base ;页表的物理地址
    mov ecx,256 ; 低端lM内存／每页大小4k = 256
    xor esi,esi

.create_pte:
    mov edx,eax
    or edx,0x7  ;页属性
    mov [es:ebx+esi*4],edx ;每一个页表项占四个字节，往页表项中填入页的物理地址
    add eax,0x1000 ;下一个页的起始地址
    inc esi 
    loop .create_pte 

    ;清空其它页表项
    mov ecx,1024
    sub ecx,256

.clear_other_pte:
    mov dword [es:ebx+esi*4],0x00000000
    inc esi 
    loop .clear_other_pte

    ;令CR3寄存器指向页目录
    mov eax,pdt_base
    mov cr3,eax 

    ;打开 cr0 pg 位（第 31 位）,开启分页机制
    mov eax,cr0 
    or eax,0x80000000
    mov cr0,eax 

    ;进程全局空间和局部空间的页面映射----------------
    ;内核放在物理地址1M的空间内，虚拟地址的低3G（0x0~0xbfffffff）属于进程，高1G（0xc0000000~0xffffffff）属于内核
    ;在页目录内创建与线性地址0xc0000000对应的目录项,使它和虚拟地址0x00000000的页目录项指向同一个页表即最后都指向物理地址1M内存，
    ;《操作系统真相还原》是在启动页机制前操作这步，我们这里是参考《x86汇编语言：实模式到保护模式》在启动页机制后操作这步
    mov eax,pt_base
    or eax,0x7 
    mov ebx,0xfffff000 ;分页机制中要修改页目录，高五位需为fffff
    mov esi,0xc0000000 ;映射的起始地址
    shr esi,22 ;线性地址的高10位是目录索引
    shl esi,2 ;乘以4得到页目录项的偏移地址为0xc00
    mov [es:ebx+esi],eax ;写入目录项（页表的物理地址和属性）
    
    ;创建内核其他页表的 PDE
    mov ecx,254 ;高3G的第一个目录项执行第一个页表，最后一个目录项指向了目录表自己，所以还需要创建254个PDE

    ;创建内核其他页表的 PDE,否则内核空间也仅是其低4MB 被所有进程其事，万一在某些情况下内核使用的空间超过 4MB ，要用到第 769 个页目录项对应的
    ;页表，所以在此处提前准备该目录项
.create_kernel_other_pde:
    add esi,4 
    add eax,0x1000 ;下一个页表的位置，页目录项属性仍然是0x3，即0x102003,0x103003,0x104003,...
    mov [es:ebx+esi],eax 
    loop .create_kernel_other_pde

    ;将GDT中的段描述符映射到线性地址0x80000000
    sgdt [pgdt]

    mov ebx,[pgdt+2]

    ;显存映射到内存的高端，肯定不能让用户进程直接能控制显存，故显存段的段基址也要改为 3GB 以上才行
    or dword [es:ebx+0x18+4],0xc0000000

    add dword [pgdt+2],0xc0000000 ;GDTR也用的是线性地址，将GDT的基地址也映射到内存的高端

    mov esp,0xc009f000 ;修改栈指针，且4K对齐

    lgdt [pgdt] ;将修改后的GDT基地址和界限值加载到GDTR，使修改生效

    mov eax,vedio_seg_selector;显式刷新段寄存器高速缓冲区内容，使处理器转移到内存的高端地址执行
    mov fs,eax
    mov byte [fs:0x280],'v' ;现在ds还是vedio_seg_selector，在屏幕上显示一个字母V表示使用了虚拟地址

;--------------------------------------------
;提取内核并进入内核运行
;--------------------------------------------
    mov ebx,[elf_base+28] ;elf偏移量28字节处是e_phoff,表示第一个program_header在elf文件中的偏移量
    add ebx,elf_base ;得到第一个program_header的起始虚拟地址
    mov ax,[elf_base+42] ;e_phentsize ，表示每个program_header的大小,注意取4个字节就够了，这里不能用eax
    mov cx,[elf_base+44] ;e_phnum,program_header的个数,注意取4个字节就够了，这里不能用ecx

    cmp byte [ebx+0],0 ;program_header偏移量为0字节处是type，这里判断是否为PT_NULL(也就是0)
    jz .next_program
.mov_program:
    ;为函数 mem_cpy 压入参数，参数是从右往左依然压入
    ;函数原型类似于 mem_cpy ( dst, src, size)
    push dword [ebx+16] ;program_header偏移量为16字节处是p_filesz,指明本段在文件中的大小,作为mem_cpy的第三个参数size
    mov edx,[ebx+4] ;program_header偏移量为4字节处是p_offset,指明本段在文件内的起始偏移字节
    add edx,elf_base ;得到本段当前所在的位置
    push edx ;作为mem_cpy的第二个参数src
    push dword [ebx+8] ;作为mem_cpy的第一个参数dst,program_header偏移量为8字节处是p_vaddr，用来指明本段在内存中的起始虚拟地址,程序需要被挪到这里，从这里开始执行

    call mem_cpy
    add esp,12 ;清空栈
    
.next_program:
    add ebx,eax ;类型为PT_NULL则跳过，处理下一个program_header
    loop .mov_program

    jmp KENRNEL_ENTRY_POINT ;跳转到内核运行




     



