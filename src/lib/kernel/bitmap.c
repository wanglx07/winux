#include <bitmap.h>
#include <string.h>
#include <assert.h>

//位图初始化
void bitmap_init(bitmap *bmap)
{
    memset(bmap->ptr,0,bmap->length);
}

//将位图的某个位设置成0或者1
void bitmap_set(bitmap *bmap,uint32_t position ,uint8_t value)
{
    assert((value==0 || value==1)&& ( position>=0 && position < bmap->length*8));
    uint32_t index = position / 8;//第几个字节
    uint8_t offset = position % 8;//字节的第几个位

    if (value)
    {
        *(bmap->ptr+index) |= 1<<offset;    
    }else{
        *(bmap->ptr+index) &= ~(1<<offset);
    }
}

//判断位图的某个位是0还是1
int bitmap_test(bitmap *bmap,uint32_t position)
{
    assert(( position>=0 && position < bmap->length*8));
    uint32_t index = position / 8;//第几个字节
    uint8_t offset = position % 8;//字节的第几个位

    return (*(bmap->ptr+index) & (1<<offset))==0 ? 0 : 1;
}

//在位图中寻找连续的count个可使用的页
int bitmap_find(bitmap *bmap,uint32_t count)
{
    uint32_t index=0;
    uint8_t offset=0;
    uint32_t start_position=0;//0开始的位置

    //找出第一次出现0的位置
    while(index < bmap->length)
    {
        if(*(bmap->ptr+index) != 0xff)
        {   
            offset = 0;
            while (*(bmap->ptr+index) & (1 << offset))   //注意内存是按小端存储的，比如0b10100101,从右往左是内存由低到高，所以这里不能写成(0x80 >> offset)
            {
                offset++;
            }
            start_position = index * 8 + offset;
            if (count == 1) //如果只想找一个空闲的页，现在就已经找到了
            {
               return start_position; 
            }
            break;
        }
        index++;
    }

    //都是1，找不到空闲的，返回-1
    if (index == bmap->length)
    {
        return -1;
    }

    uint32_t next_position=start_position+1;
    uint32_t zero_num=1;//上面已经找到了第一个0

    while(next_position < bmap->length*8)
    {
        if (!bitmap_test(bmap,next_position))//一个bit也为0
        {
            zero_num++;
        }else{
            zero_num=0;
        }

        if (zero_num == count)
        {
            start_position=next_position-count+1;
            return start_position;
        }
        next_position++;
    }

    return -1;

}