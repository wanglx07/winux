#ifndef  _RBTREE_H_
#define _RBTREE_H_

typedef int Type;

//binary search tree
typedef struct BSTNode{
    Type key;
    struct BSTNode* left;
    struct BSTNode* right;
    struct BSTNode* parent;
}Node,*BSTree;

Node * create_bstree_node(Type key,Node * parent,Node * left,Node * right);
void preorder_bstree(BSTree tree);
void inorder_bstree(BSTree tree);
void postorder_bstree(BSTree tree);
Node * bstree_search(BSTree x,Type key);
Node * bstree_max(BSTree tree);
Node * bstree_min(BSTree tree);
Node * bstree_predecessor(Node * x);
Node * bstree_successor(Node * x);
Node * bstree_add(BSTree tree,Type key);
Node * bstree_delete(BSTree tree,Type key);
void bstree_print(BSTree tree,Type key,int direction);
void bstree_destroy(BSTree tree);
#endif  