video_seg_sel equ (3<<3) ;视频显示缓冲区的段选择子
[bits 32]
;字符串显示例程
global print
print: ;显示0终止的字符串并移动光标
    ;输入：DS:EBX=串地址
    push ebp
    mov ebp,esp
    push ecx
    mov ebx,[ebp+8]
.getc:
    mov cl,[ebx]
    or cl,cl
    jz .exit
    call put_char
    inc ebx
    jmp .getc

.exit:
    pop ecx
    leave
    ret 

;-------------------------------------------------------------------------------
put_char: ;在当前光标处显示一个字符,并推进
;光标。仅用于段内调用
;输入：CL=字符ASCII码
    pushad
    push gs
    mov eax,video_seg_sel 
    mov gs,eax

    ;以下取当前光标位置
    mov dx,0x3d4
    mov al,0x0e
    out dx,al
    mov dx,0x3d5
    in al,dx ;高字
    mov ah,al

    mov dx,0x3d4
    mov al,0x0f
    out dx,al
    mov dx,0x3d5
    in al,dx ;低字
    mov bx,ax ;BX=代表光标位置的16位数



    cmp cl,0x0d ;回车符？
    jnz .put_0a
    ;------这样子写会导致除0异常
    ; mov ax,bx
    ; mov bl,80
    ; div bl
    ; mul bl
    ; mov bx,ax
    xor dx, dx				  ; 被除数的高16位,清0.
    mov ax, bx				  ; 被除数的低16位.
    mov si, 80				  
    div si		
    sub bx, dx		
    jmp .set_cursor

.put_0a:
    cmp cl,0x0a ;换行符？
    jnz .put_08
    add bx,80
    jmp .roll_screen

.put_08:
    cmp cl,0x08 ;退格符？
    jnz .put_other
    dec bx
    mov cl,0x20
    shl bx,1
    mov [gs:bx],cl
    shr bx,1
    jmp .set_cursor


.put_other: ;正常显示字符
    shl bx,1
    mov [gs:bx],cl

    ;以下将光标位置推进一个字符
    shr bx,1
    inc bx
    
 .roll_screen:
    cmp bx,2000 ;光标超出屏幕？滚屏
    jl .set_cursor

    cld
    ; mov esi, 0xb80a0 ;小心！32位模式下movsb/w/d
    ; mov edi, 0xb8000 ;使用的是esi/edi/ecx
    ;这里esi，和edi要使用高端地址，否则在用户进程中打开shell
    ;需要滚屏的时候会报缺页异常
    mov esi, 0xc00b80a0	; 第1行行首
    mov edi, 0xc00b8000	; 第0行行首
    mov ecx,960
    rep movsd
    mov bx,3840 ;清除屏幕最底一行
    mov ecx,80 ;32位程序应该使用ECX

 .cls:
    mov word [gs:bx],0x0720
    add bx,2
    loop .cls
    mov bx,1920

 .set_cursor:
    mov dx,0x3d4
    mov al,0x0e
    out dx,al
    mov dx,0x3d5
    mov al,bh
    out dx,al
    mov dx,0x3d4
    mov al,0x0f
    out dx,al
    mov dx,0x3d5
    mov al,bl
    out dx,al

    pop eax
    mov gs,eax
    popad
    ret
end:
    jmp $
;-------------------------------------------------------------------------------
;清屏
global clear_screen
clear_screen:
    pushad

    ;屏幕上所有的字符都赋值为空格字符
    mov ecx,2000
    mov ebx,0 
    push es
    mov eax,video_seg_sel 
    mov es,eax
.set_space:
    mov word[es:bx],0x0720
    add bx,2
    loop .set_space
    pop eax
    mov es,eax

    ;光标回到0位置
    mov ebx,0 
    mov dx,0x3d4
    mov al,0x0e
    out dx,al
    inc dx ;0x3d5
    mov al,bh
    out dx,al
    dec dx ;0x3d4
    mov al,0x0f
    out dx,al
    inc dx ;0x3d5
    mov al,bl
    out dx,al

    popad 
    mov eax,10 ;返回值，用来做实验的，可以删除
    ret
