#include <rbtree.h>
#include <types.h>
#include <printk.h>
#include <fs.h>

//创建节点
Node * create_bstree_node(Type key,Node * parent,Node * left,Node * right)
{
    Node * p;
    if ((p=(Node*)malloc_sys(sizeof(Node)))==NULL)
    {
        return NULL;
    }

    p->key=key;
    p->left=left;
    p->right=right;
    p->parent=parent;

    return p;
}


//前序遍历
void preorder_bstree(BSTree tree)
{
    if (tree !=NULL)
    {
        printf("%d ",tree->key);

        preorder_bstree(tree->left);

        preorder_bstree(tree->right);
    }
}

//中序遍历
void inorder_bstree(BSTree tree)
{
    if (tree != NULL)
    {
        inorder_bstree(tree->left);

        printf("%d ",tree->key);

        inorder_bstree(tree->right);
    }
}

//后序遍历
void postorder_bstree(BSTree tree)
{
    if (tree !=NULL)
    {
        postorder_bstree(tree->left);

        postorder_bstree(tree->right);

        printf("%d ",tree->key);
    }
}

//查找
Node * bstree_search(BSTree x,Type key)
{
    if (x==NULL || x->key==key)
    {
        return x;
    }

    if (key< x->key)
    {
        return bstree_search(x->left,key);
    }else{
        return bstree_search(x->right,key);
    }
}

//查找最大值
Node * bstree_max(BSTree tree)
{
    if (tree==NULL)
        return NULL;

    while(tree->right != NULL)
        tree = tree->right;

    return tree;
}

//查找最小值
Node * bstree_min(BSTree tree)
{
    if (tree==NULL)
        return NULL;

    while(tree->left != NULL)
        tree=tree->left;

    return tree;
}

//查找节点的前驱节点。一个节点的前驱节点，也就是中序遍历中该节点的前一个节点,即左子树中的最大值
//树的中序遍历顺序为：1，2，4，5，7，8，9，11（因为是二叉搜索树，所以中序遍历的顺序是递增）。那么节点7的前驱是5，节点8的前驱是7
Node * bstree_predecessor(Node * x)
{
    // 如果x存在左孩子，则"x的前驱结点"为 "以其左孩子为根的子树的最大结点"
    if (x->left != NULL)
    {
        return bstree_max(x->left);
    }

    //当一个节点没有左子树，此时他的前驱节点就可能是父节点（祖父节点…）
    //所以前驱节点的寻找方法是，不断地找父节点，直到当前节点是父节点的右节点
    while(x->parent != NULL && x->parent->left==x)
    {
        x=x->parent;
    }

    //不管x->parent=NULL还是x->parent->right=x，return即可
    return x->parent;
}

//查找节点的后继节点.后继节点显然是中序遍历中当前节点的下一个节点，即右子树的最小值
Node * bstree_successor(Node * x)
{
    // 如果x存在右孩子，则"x的后继结点"为 "以其右孩子为根的子树的最小结点"
    if (x->right != NULL)
    {
        return bstree_min(x->right);
    }

    //在没有右子树的情况下，后驱节点是其父节点（祖父…），通过不断迭代查找，且必须满足当前迭代到的节点是其父节点的左节点
    while(x->parent != NULL && x->parent->right==x)
    {
        x=x->parent;
    }

    //不管x->parent=NULL还是x->parent->right=x，return即可
    return x->parent;
}

//内部函数,将结点new插入到二叉树(tree)中，并返回插入节点后的根节点,不允许插入相同键值的节点
static Node * bstree_insert(BSTree tree,Node *new_node)
{
    Node * pos=NULL;
    Node * cur=tree;

    //查找新节点的插入位置
    while(cur != NULL)
    {
        pos=cur;
        if (new_node->key < cur->key)
        {
            cur=cur->left;    
        }else if(new_node->key > cur->key){
            cur=cur->right;
        }else{//不允许插入相同键值的节点
            free_sys(new_node);
            return tree;
        }
    }

    new_node->parent=pos;
    if (pos==NULL)
    {
        tree=new_node;//作为根节点
    }else if(new_node->key < pos->key){
        pos->left = new_node;
    }else{
        pos->right = new_node ;
    }

    return tree;
}

//在树中新增节点，key是节点的值；并返回插入节点后的根节点
Node * bstree_add(BSTree tree,Type key)
{
    Node * new_node;
    if ((new_node=create_bstree_node(key,NULL,NULL,NULL))==NULL)
    {
        return tree;// 如果新建结点失败，则返回
    }

    return bstree_insert(tree,new_node);
}

//内部函数，它的作用是：移除二叉树(tree)中的节点(remove_node)，并返回移除节点后的根节点
//第一，被删除的节点是叶子节点，我们只需断开被删除的节点和其父节点的关联即可
//第二，被删除的节点只有一个孩子节点，我们可以用被删除的节点的孩子节点来替代被删除的节点
//第三，被删除的节点有两个孩子节点，相当于删除一个子树的根节点，为了保持二叉搜索树的性质，
// 我们可以使用左子树中的最大值或右子树的最小值来替代被删除的根节点，不过在实现时，考虑到实现的简便，
//对于第三种情况会通过直接修改当前节点的值来替代修改节点的指针指向，问题就转换为删除替代的节点
static Node * bstree_remove(BSTree tree,Node * remove_node)
{
    Node * real_remove = NULL;//真正移除的节点
    Node * child_node= NULL;//待删除节点的子节点

    if(remove_node->left != NULL && remove_node->right != NULL) //如果有两个子节点
    {
        real_remove = bstree_successor(remove_node);//真正要删除的是后继节点，即右子树的最小值
    }else{//没有子节点或者只有一个子节点
        real_remove =remove_node;//真正要删除的就是自身
    }

    //到这一步，真正待移除节点有一个子节点或者没有子节点（如果待删除节点是后继节点，最多也只可能有个右节点）
    if (real_remove->left != NULL)
    {
        child_node=real_remove->left;
    }else{
        child_node=real_remove->right;
    }

    //重新设置其子节点的父节点指向
    if (child_node != NULL)
    {
        child_node->parent=real_remove->parent;
    }

    //重新设置父节点的子节点指向
    if (real_remove->parent == NULL)//说明删的是根节点
    {
        tree = child_node;
    }else if(real_remove==real_remove->parent->left){
        real_remove->parent->left = child_node;
    }else if(real_remove == real_remove->parent->right){
        real_remove->parent->right =child_node;
    }

    //待删除节点有两个子节点的情况,直接修改当前节点的值来替代修改节点的指针指向
    if (real_remove != remove_node)
    {
        remove_node->key = real_remove->key;
    }

    //删除节点
    if (real_remove != NULL)
    {
        free_sys(real_remove);
    }

    return tree;
}

//在树中查找键值为key的节点，找到的话就删除该节点；并返回删除节点后的根节点
Node * bstree_delete(BSTree tree,Type key)
{
    Node * remove_node;
    if ((remove_node=bstree_search(tree,key)) == NULL)
        return tree;

    return bstree_remove(tree,remove_node);
}

//打印二叉树
// tree       -- 二叉树的节点
// key        -- 节点的键值 
// direction  --  0，表示该节点是根节点;
//               -1，表示该节点是它的父结点的左孩子;
//                1，表示该节点是它的父结点的右孩子。
void bstree_print(BSTree tree,Type key,int direction)
{
    if(tree!= NULL)
    {
        if (direction ==0 )
        {
            printk("%d is root \r\n",tree->key);
        }else{
            printk("%d is %d's %s child\r\n",tree->key,key,direction==1?"right":"left");
        }
        bstree_print(tree->left,tree->key,-1);
        bstree_print(tree->right,tree->key,1);
    }
}


void bstree_destroy(BSTree tree)
{
     if (tree==NULL)
         return ;

     if (tree->left != NULL)
         bstree_destroy(tree->left);
     if (tree->right != NULL)
         bstree_destroy(tree->right);
 
    free(tree);
}
