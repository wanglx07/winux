#ifndef _BITMAP_H_
#define _BITMAP_H_
#include <types.h>

typedef struct bitmap
{
    uint32_t length; //指针ptr每次按字节大小查看位图的8个位，一次可看8个页，以判断内存的使用情况，所以length=内存所占的页数/8
    uint8_t * ptr; 
}bitmap; 

void bitmap_init(bitmap *bmap);
void bitmap_set(bitmap *bmap,uint32_t position ,uint8_t value);
int bitmap_test(bitmap *bmap,uint32_t position);
int bitmap_find(bitmap *bmap,uint32_t count);

#endif 