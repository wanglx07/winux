#ifndef _PRINT_H_
#define _PRINT_H_
#include <types.h>
void print(char* str);
uint32_t clear_screen(void);
#endif