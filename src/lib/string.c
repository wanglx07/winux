#include <string.h>
#include <assert.h>

//获取字符串长度,不包括最后的'\0'
int strlen (const char *s)
{
  const char *p;
  p=s;
  while(*p != '\0')
  {
      p++;
  }
  return p-s;
}

// 将一个字符串(src)拷贝到另一个字符串(dest)，直到遇到0字符后停止
char * strcpy (char *dest, const char *src)
{
    assert(dest!=NULL && src!=NULL);
    char * ret=dest;
    while (*src != '\0')         
    {                            
        *dest++ = *src++;        
    }   
    *dest = '\0';                         
    return ret;
}

// 比较两个字符串,若csrc中的字符大于ct中的字符返回1,相等时返回0,否则返回-1
int strcmp (const char *csrc, const char *ct)
{
    assert(csrc!=NULL && ct!=NULL);
    while(*csrc!='\0' && *csrc == *ct )
    {
        csrc++;
        ct++;
    }
    return *csrc<*ct ? -1 : *csrc > *ct;
}

//从后往前查找字符串str中首次出现字符ch的地址(不是下标,是地址) 
char * strchr (const char *str, char c)
{
    char * pstr=(char*)str;
    while(*pstr != '\0'&& *pstr !=c)
    {
        pstr++;
    }
    return *pstr=='\0'? NULL : pstr;
}

//拼接
char * strcat (char *dest, const char *src)
{
    assert(dest!=NULL&& src!=NULL);
    while(*dest !=0)
        dest++;
    while(*src != '\0')
    {
        *dest=*src;
        dest++;
        src++;
    }
    *dest='\0';
    return dest;
}


// 用字符填写指定长度内存块
void * memset (void *dest, char c, size_t size)
{
    assert(dest!=NULL && size>=0);
    char *pdest=(char*)dest;
    while(size-->0)
    {
        *pdest++=c;
    }
    return dest;
}

// 内存块复制。从源地址src 处开始复制n 个字节到目的地址dest 处
void * memcpy (void *dest, const void *src, size_t size)
{
    assert(dest!=NULL && src!=NULL);
    char* pdest = (char*)dest; 
    char* psrc = (char*)src; 
    while(size-- > 0)
    {
        *pdest++=*psrc++;
    }
    return dest;
}

// 比较n 个字节的两块内存（两个字符串），即使遇上NULL 字节也不停止比较。
int memcmp (const void *csrc, const void *ct, size_t size)
{
    assert(csrc!=NULL && ct!=NULL);
    char* pcsrc = (char*)csrc; 
    char* pct = (char*)ct; 
    while(size-- > 0)
    {
        if (*pcsrc!=*pct)
        {
            return *pcsrc>*pct ? 1 : -1 ;
        }
        pcsrc++;
        pct++;
    }
    return 0;
}

// 在n 字节大小的内存块(字符串)中寻找指定字符
void * memchr (const void *csrc, char c, size_t size)
{
    assert(csrc!=NULL);
    while(size && (*(char *)csrc != c))
    {
        csrc = (char *)csrc + 1;
        size--;
    }
    return (size ? (void *)csrc : NULL);
}

//从后往前查找字符串str中首次出现字符ch的地址(不是下标,是地址)
char * strrchr(const char * str,char c)
{
   char* last = NULL;
   //从头到尾遍历一次,若存在ch字符,last_char总是该字符最后一次出现在串中的地址(不是下标,是地址)
   while (*str != 0) {
      if (*str == c) {
	    last = (char *)str;
      }
      str++;
   }
   return last;
}