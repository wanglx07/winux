#include <syscall.h>
#include <unistd.h>


//没有参数的系统调用，也就是只需要个功能号就行，而功能号由__NR_##name在unistd.h中找
#define _syscall0(name) ({\
    int ret;            \
    asm volatile(       \
        "int $0x80"     \
        :"=a"(ret)      \
        :"0"(__NR_##name));\
    ret; \
})

//一个参数的系统调用
#define _syscall1(name,arg1) ({ \
    int ret ; \
    asm volatile( \
        "int $0x80" \
        :"=a"(ret) \
        :"a"(__NR_##name),"b"(arg1));\
    ret; \
})

#define _syscall2(name,arg1,arg2) ({ \
    int ret ; \
    asm volatile( \
        "int $0x80"\
        :"=a"(ret) \
        :"a"(__NR_##name),"b"(arg1),"c"(arg2));\
    ret; \
})

#define _syscall3(name,arg1,arg2,arg3) ({ \
    int ret ; \
    asm volatile( \
        "int $0x80"\
        :"=a"(ret) \
        :"a"(__NR_##name),"b"(arg1),"c"(arg2),"d"(arg3));\
    ret; \
})

pid_t getpid()
{
    return _syscall0(getpid);//参数是系统调用的名称
}

int32_t write(int32_t fd ,const char * buf,uint32_t count )
{
    return _syscall3(write,fd,buf,count);
}


void * malloc(uint32_t size)
{
    return (void*)_syscall1(malloc,size);
}

void free(void * vaddr)
{
    return _syscall1(free,vaddr);
}

pid_t fork()
{
    return _syscall0(fork);
}

int32_t read(int32_t fd ,const char * buf,uint32_t count )
{
    return _syscall3(read,fd,buf,count);
}

void putchar(char c)
{
    return _syscall1(putchar,c);
}

void clear()
{
    return _syscall0(clear);
}

// 获取当前工作目录 
char* getcwd(char* buf, uint32_t size) {
   return (char*)_syscall2(getcwd, buf, size);
}

//以flag方式打开文件pathname 
int32_t open(char* pathname, uint8_t flag) {
   return _syscall2(open, pathname, flag);
}

// 关闭文件fd 
int32_t close(int32_t fd) {
   return _syscall1(close, fd);
}

// 设置文件偏移量 
int32_t lseek(int32_t fd, int32_t offset, uint8_t whence) {
   return _syscall3(lseek, fd, offset, whence);
}

//删除文件pathname 
int32_t unlink(const char* pathname) {
   return _syscall1(unlink, pathname);
}

// 创建目录pathname 
int32_t mkdir(const char* pathname) {
   return _syscall1(mkdir, pathname);
}

// 打开目录name 
struct dir* opendir(const char* name) {
   return (struct dir*)_syscall1(opendir, name);
}

// 关闭目录dir 
int32_t closedir(struct dir* dir) {
   return _syscall1(closedir, dir);
}

// 删除目录pathname 
int32_t rmdir(const char* pathname) {
   return _syscall1(rmdir, pathname);
}

// 读取目录dir 
struct dir_entry* readdir(struct dir* dir) {
   return (struct dir_entry*)_syscall1(readdir, dir);
}

// 重置目录指针 
void rewinddir(struct dir* dir) {
   _syscall1(rewinddir, dir);
}

// 获取path属性到buf中 
int32_t stat(const char* path, struct stat* buf) {
   return _syscall2(stat, path, buf);
}

// 改变工作目录为path 
int32_t chdir(const char* path) {
   return _syscall1(chdir, path);
}

// 显示进程列表 
void ps(void) {
   _syscall0(ps);
}

int32_t execv(const char * path,const char ** argv){
    return _syscall2(execv,path,argv);
}

pid_t wait(uint32_t * status)
{
    return _syscall1(wait, status);
}

void exit(int32_t status)
{
    return _syscall1(exit, status);
}

int32_t pipe(int32_t pipefd[2])
{
    return _syscall1(pipe, pipefd);
}

void fd_redirect(uint32_t old_fd,uint32_t new_fd)
{
    return _syscall2(fd_redirect,old_fd,new_fd);
}