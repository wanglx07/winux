#include <unistd.h>
#include <thread.h>
#include <syscall_kernel.h>
#include <printk.h>
#include <string.h>
#include <fs.h>
#include <fork.h>
#include <print.h>
#include <exec.h>
#include <wait_exit.h>
#include <pipe.h>

#define SYS_CALL_SIZE 32

void * sys_call_table[SYS_CALL_SIZE];//系统调用表


//新增功能号对应的处理函数
uint32_t getpid_sys()
{
    struct task_struct * cur_task=get_current_thread();
    return cur_task->pid;
}

// uint32_t write_sys(char * str)
// {
//     printk("%s",str);
//     return strlen(str);
// }

void * malloc_sys(uint32_t size)
{
    void * vaddr=kmalloc(size);
    return vaddr;
}

void free_sys (void * vaddr)
{
    kfree(vaddr);
}

void sys_putchar(char c)
{
    char buf[2]= {0};
    buf[0] = c;
    printk(buf);
}

void sys_clear(void)
{
    clear_screen();
}

void syscall_init()
{
    sys_call_table[__NR_getpid]=getpid_sys;//将处理函数注册进系统调用表
    sys_call_table[__NR_write] =sys_write; //在fs.c中
    sys_call_table[__NR_malloc] =malloc_sys;
    sys_call_table[__NR_free] = free_sys;
    sys_call_table[__NR_fork] = sys_fork;//在fork.c中
    sys_call_table[__NR_read] = sys_read;//在fs.c中
    sys_call_table[__NR_putchar] = sys_putchar;
    sys_call_table[__NR_clear] = sys_clear;
    sys_call_table[__NR_getcwd] = sys_getcwd;//这个及以下在fs.c中
    sys_call_table[__NR_open]   = sys_open;
    sys_call_table[__NR_close]  = sys_close;
    sys_call_table[__NR_lseek]	= sys_lseek;
    sys_call_table[__NR_unlink]	= sys_unlink;
    sys_call_table[__NR_mkdir]	= sys_mkdir;
    sys_call_table[__NR_opendir] = sys_opendir;
    sys_call_table[__NR_closedir] = sys_closedir;
    sys_call_table[__NR_chdir] = sys_chdir;
    sys_call_table[__NR_rmdir]	= sys_rmdir;
    sys_call_table[__NR_readdir] = sys_readdir;
    sys_call_table[__NR_rewinddir] = sys_rewinddir;
    sys_call_table[__NR_stat] = sys_stat;
    sys_call_table[__NR_ps]	 = sys_ps;//在thread.c中
    sys_call_table[__NR_execv]	= sys_execv;//在exec.c中
    sys_call_table[__NR_wait] = sys_wait; //在wait_exit.c中
    sys_call_table[__NR_exit] = sys_exit;
    sys_call_table[__NR_pipe] = sys_pipe;//在pipe.c中
    sys_call_table[__NR_fd_redirect] = sys_fd_redirect;//在pipe.c中
}