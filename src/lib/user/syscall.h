#ifndef  _SYSCALL_H_
#define _SYSCALL_H_
#include <types.h>
#include <fs.h>
#include <wait_exit.h>

pid_t getpid(void);
int32_t write(int32_t fd ,const char * buf,uint32_t count );
void * malloc(uint32_t size);
void free(void * vaddr);
pid_t fork(void);
int32_t read(int32_t fd ,const char * buf,uint32_t count);
void putchar(char c);
void clear(void);
char* getcwd(char* buf, uint32_t size);
int32_t open(char* pathname, uint8_t flag);
int32_t close(int32_t fd);
int32_t lseek(int32_t fd, int32_t offset, uint8_t whence);
int32_t unlink(const char* pathname);
int32_t mkdir(const char* pathname);
struct dir* opendir(const char* name);
int32_t closedir(struct dir* dir);
int32_t rmdir(const char* pathname);
struct dir_entry* readdir(struct dir* dir);
void rewinddir(struct dir* dir);
int32_t stat(const char* path, struct stat* buf);
int32_t chdir(const char* path);
void ps(void);
int32_t execv(const char * path,const char ** argv);
pid_t wait(uint32_t * status);
void exit(int32_t status);
int32_t pipe(int32_t pipefd[2]);
void fd_redirect(uint32_t old_fd,uint32_t new_fd);
#endif 