#ifndef _UNISTD_H_
#define _UNISTD_H_

#define __NR_getpid 0
#define __NR_write 1 
#define __NR_malloc 2 
#define __NR_free 3
#define __NR_fork 4 
#define __NR_read 5
#define __NR_putchar 6
#define __NR_clear 7
#define __NR_getcwd 8
#define __NR_open 9 
#define __NR_close 10 
#define __NR_lseek 11
#define __NR_unlink 12 
#define __NR_mkdir 13
#define __NR_opendir 14
#define __NR_closedir 15
#define __NR_chdir 16
#define __NR_rmdir 17 
#define __NR_readdir 18 
#define __NR_rewinddir 19
#define __NR_stat 20 
#define __NR_ps 21
#define __NR_execv 22
#define __NR_wait 23
#define __NR_exit 24
#define __NR_pipe 25
#define __NR_fd_redirect 26
#endif 