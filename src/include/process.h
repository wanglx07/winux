#ifndef  _PROCESS_H_
#define _PROCESS_H_
#include <thread.h>

#define DEFAULT_PRIO 1
#define USER_VIR_ADDR_START 0x8048000

void process_start(char *process_name,void *process_entry);
void refresh_cr3_tss(struct task_struct* next_task);
uint32_t * pdt_create(void);

#endif 