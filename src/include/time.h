#ifndef _TIME_H_
#define _TIME_H_ 
#include <types.h>

#define TIMER_CTR_PORT 0x43 
#define TIMER0 0x40
#define TIMER0_FREQ 100 //100HZ

void time_init(void);
void timer_intr_handler(void);
void sleep_ms(uint32_t time);
#endif 