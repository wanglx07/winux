#ifndef _ASSERT_H_
#define _ASSERT_H_

void __panic(const char * filename,char * func,int line,char * e);

#define panic(...)    \
    __panic(__FILE__,__func__,__LINE__, __VA_ARGS__)

#ifdef NDEBUG
#define assert(e) ((void)0) //什么都不做
#else
#define assert(e)  \
    ((e) ? ((void)0) : panic(#e))
#endif

#endif 