#ifndef  __BUILDIN_H_
#define __BUILDIN_H_
#include <types.h>

void get_abs_path(char * src_path,char * abs_path );
void buildin_ls(uint32_t argc, char** argv);
int32_t buildin_cd(uint32_t argc, char** argv);
int32_t buildin_mkdir(uint32_t argc, char** argv);
int32_t buildin_rmdir(uint32_t argc, char** argv);
int32_t buildin_rm(uint32_t argc, char** argv);
void buildin_pwd(uint32_t argc, char** argv);
void buildin_ps(uint32_t argc, char** argv);
void buildin_clear(uint32_t argc, char** argv);

#endif 