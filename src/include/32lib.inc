;----------------------------------------------
;这个一定要放在[bits 32]下面，因为需要使用32位进行编译，之前放在print下面，用不了
read_disk: ;从硬盘读取一个逻辑扇区
    ;EAX=逻辑扇区号
    ;DS:EBX=目标缓冲区地址
    ;返回：EBX=EBX+512
    push eax
    push ecx
    push edx

    push eax

    mov dx,0x1f2
    mov al,1
    out dx,al ;读取的扇区数

    inc dx ;0x1f3
    pop eax
    out dx,al ;LBA地址7~0

    inc dx ;0x1f4
    mov cl,8
    shr eax,cl
    out dx,al ;LBA地址15~8

    inc dx ;0x1f5
    shr eax,cl
    out dx,al ;LBA地址23~16

    inc dx ;0x1f6
    shr eax,cl
    or al,0xe0 ;第一硬盘 LBA地址27~24
    out dx,al

    inc dx ;0x1f7
    mov al,0x20 ;读命令
    out dx,al

.wait:
    in al,dx
    and al,0x88
    cmp al,0x08
    jnz .wait ;不忙，且硬盘已准备好数据传输

    mov ecx,256 ;总共要读取的字数
    mov dx,0x1f0
.readw:
    in ax,dx
    mov [ebx],ax
    add ebx,2
    loop .readw

    pop edx
    pop ecx
    pop eax

    ret 

;---------------------------------------------------------
;输入:栈中三个参数(dst,src,size)
;输出:无
mem_cpy:		      
   push ebp
   mov ebp, esp
   push ecx		   ; rep指令用到了ecx，但ecx对于外层段的循环还有用，故先入栈备份
   mov edi, [ebp + 8]	   ; dst
   mov esi, [ebp + 12]	   ; src
   mov ecx, [ebp + 16]	   ; size
   cld
   rep movsb		   ; 逐字节拷贝

   ;恢复环境
   pop ecx		
   pop ebp
   ret 

