#ifndef _DISK_H_
#define _DISK_H_
#include <types.h>
#include <list.h>
#include <mutex.h>
#include <semaphore.h>
#include <bitmap.h>

//分区
struct partition{
    char name[8] ;//分区名称
    uint32_t start_sector ;//起始扇区
    uint32_t sector_count;//扇区数
    struct disk * to_which_disk;//属于哪块硬盘 //前面的部分在disk.c的partition_scan()中初始化，后面的部分在fs.c的partition_mount()初始化
    struct list_head entry;//通过它加入队列 
    struct super_block * super_b;//本分区的超级块
    struct bitmap block_bitmap;//空闲块位图
    struct bitmap inode_bitmap;//i结点位图
    struct list_head open_inodes_list;//本分区打开的i结点队列
};

//主盘还是从盘
enum disk_diff{
    MASTER,
    SLAVE
};

//硬盘
struct disk{
    char name [8];//硬盘名称
    struct ata_channel * to_which_channel;//属于那个ata通道
    enum disk_diff diff; //区分主盘还是从盘
    struct partition prim_parts[4]; //主分区最多4个
    struct partition logic_parts[8];//逻辑分区最多8个
};

//ata通道
struct ata_channel{
    char name [8] ;//通道名字
    uint16_t start_port;//起始端口号
    mutex lock; //通道锁,一个通道有两个硬盘，这两个硬盘共用一个中断，所以用锁控制这个通道每次只能使用一个硬盘
    struct disk disks[2];//该通道上的两个硬盘，一主一从
    //给硬盘发命令的时候设置为true，硬盘中断中设置为false
    //用来标志处理硬盘的读写，而不是其他情况，如硬盘自检出了问题，通过中断发出了某种警告，这种情况暂不处理
    bool want_intr; 
    struct semaphore disk_sema; //线程用这个信号量来阻塞自己，硬盘中断中将线程唤醒
};

//构建一个16字节大小的结构体,用来存MBR或者EBR中的分区表项 
struct table_item {
   uint8_t  bootable;		 // 是否可引导	
   uint8_t  start_head;		 // 起始磁头号
   uint8_t  start_sector;		 // 分区的绝对起始扇区号
   uint8_t  start_chs;		 // 起始柱面号
   uint8_t  fs_type;		 // 分区类型
   uint8_t  end_head;		 // 结束磁头号
   uint8_t  end_sec;		 // 结束扇区号
   uint8_t  end_chs;		 // 结束柱面号
   uint32_t offset_sector;		 // 本分区的扇区偏移量
   uint32_t sector_count;		 // 本分区的总扇区数
} __attribute__ ((packed));	 

// 引导扇区,MBR或者EBR
struct boot_sector {
   uint8_t  boot_code[446];		 // 引导代码
   struct   table_item partition_table[4];       // 分区表中有4项,共64字节
   uint16_t magic;		 // 启动扇区的结束标志是0x55,0xaa,
} __attribute__ ((packed));


void disk_read(struct disk* hd,uint32_t start_sector,void * buf, uint32_t sector_count);
void disk_write(struct disk* hd,uint32_t start_sector,void * buf, uint32_t sector_count);
void disk_init(void);
void partition_scan(struct disk* hd, uint32_t ext_start_sector) ;

#endif 