#ifndef _IO_H_
#define _IO_H_
#include <types.h>

//不加static的话会出现报错:inb()多次定义
 static inline u8 inb(u16 port)//读端口
{
    u8 data;
    asm volatile("inb %%dx,%%al":"=a"(data):"d"(port));
    return data;
}

 static inline u16 inw(u16 port)
{   
    u8 data;
    asm volatile("inw %%dx,%%ax":"=a"(data):"d"(port));
    return data;
}

static inline void  outb(u16 port ,u8 data) //写端口
{
    asm volatile("outb %%al,%%dx"::"d"(port),"a"(data));
}

static inline void  outw(u16 port ,u16 data)
{
    asm volatile("outw %%ax,%%dx"::"d"(port),"a"(data)); //写字符串才使用outsw
}

//注意这是insw，不是movsw。movsw是ds:esi->es：edi ,insw是 dx(也就是端口)->es:edi
static inline void  insw(u16 port ,void * addr,u32 repeat)
{
    asm volatile("cld;rep insw":"+D"(addr),"+c"(repeat):"d"(port)); //写字符串才使用outsw,"+"表示可读可写
}

//outsw是ds:esi->dx(也就是端口)
static inline void  outsw(u16 port ,void * addr,u32 repeat)
{
    asm volatile("cld;rep outsw":"+S"(addr), "+c" (repeat):"d"(port)); //写字符串才使用outsw
}

#endif 