#ifndef  _SEMAPHORE_H_
#define _SEMAPHORE_H_ 
#include <list.h>

//信号量
struct semaphore{
    int count; //信号量，为1，就是别人可以用；为0，别人没有使用的机会
    struct list_head waiters_head; //等待队列
};

void sema_init(struct semaphore* sema, int val); 

void down(struct semaphore* sema);

void up(struct semaphore* sema);

#endif 
