#ifndef _INTERRUPT_H_
#define _INTERRUPT_H_
#include <types.h>

#define IDT_SIZE 0X81 
#define PIC_M_CTRL 0x20	       // 主片的控制端口是0x20
#define PIC_M_DATA 0x21	       // 主片的数据端口是0x21
#define PIC_S_CTRL 0xa0	       // 从片的控制端口是0xa0
#define PIC_S_DATA 0xa1	       // 从片的数据端口是0xa1

void interrupt_init(void);

void register_handler(uint8_t vector,void * handler);

//中断的两种状态:
enum intr_status {		 
    INTR_OFF,			 // 中断关闭
    INTR_ON		         // 中断打开
};

enum intr_status intr_get_status(void);
void intr_restore_status (enum intr_status);
enum intr_status intr_enable (void);
enum intr_status intr_disable (void);
#endif