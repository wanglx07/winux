#ifndef  _FILE_H_ 
#define _FILE_H_
#include <disk.h>
#include <fs.h>

//文件结构
struct file{
    uint32_t fd_pos;//当前文件操作的偏移地址，0为开始，-1为文件大小
    uint32_t fd_flag;//文件操作标识，如 O_RDONLY,
    struct inode * fd_inode;//inode 指针，用来指向 inode队列（ part-> open_ inodes_list ）中的 inode
};


//位图类型
enum bmap_type{
    INODE_BMAP, //inode位图
    BLOCK_BMAP //空闲块位图
};

//打开文件的选项
enum open_flag{
    O_RDONLY, //只读
    O_WRONLY, //只写
    O_RDWR,//读写
    O_CREAT =4 //创建
};

struct path_record{
    char path_traveled[MAX_PATH_LENGTH];//查找过程中已走过的路径
    struct dir * parent_dir;//文件或者目录所在的直接父目录
    enum file_type f_type;//查找的文件所属的类型
};

//文件表
extern struct file file_table[MAX_FILE_OPEN];

int free_file_slot(void);
int fd_install(int f_idx);
int32_t inode_bmap_alloc(struct partition * part);
int32_t block_bmap_alloc(struct partition * part);
void bmap_sync(struct partition * part,uint32_t bit_idx,enum bmap_type type);
int32_t file_create(struct dir * parent_dir,char * filename,uint8_t flag);
int32_t file_open( uint32_t i_num, enum open_flag flags);
int32_t file_close(struct file * f);
int32_t file_write(struct file* file, const void* data_buf, uint32_t count);
int32_t file_read(struct file *file,void * buf,uint32_t count);

#endif 