#ifndef  _KFIFO_H_
#define _KFIFO_H_
#include <types.h>
#include <mutex.h>
#include <thread.h>

#define BUF_SIZE 2048 //之前用bufsize会和fs.c中的某个变量冲突,改成2048(后面管道申请的内存是一页大小，也可以写4096但是数组太大了，取个中间值)

typedef struct kfifo {
    char buff[BUF_SIZE];
    uint32_t in;    //in指向buffer中队头,数据从这里进
    uint32_t out;    //out指向buffer中的队尾，数据从这里出
    mutex lock;    /*  用于put和get过程中加锁防止并发*/
    struct task_stask *producer;//生产者，缓冲区满时，在此缓冲区上阻塞；不满则继续放数据
    struct task_stask * consumer;//消费者，缓冲区空时，在此缓冲区阻塞；非空则继续拿数据
} kfifo;


void kfifo_init(kfifo* fifo);
bool kfifo_empty(kfifo* fifo);
bool kfifo_full(kfifo* fifo);
uint32_t kfifo_len(kfifo* fifo);
void kfifo_putchar(kfifo* fifo, char c);
char kfifo_getchar(kfifo* fifo);

extern kfifo fifo;

#endif