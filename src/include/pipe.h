#ifndef  _PIPE_H_
#define _PIPE_H_
#include <types.h>

#define PIPE_FLAG 0xAAAA
int32_t sys_pipe(int32_t pipefd[2]);
uint32_t pipe_read(int32_t fd,void * buf,uint32_t count);
uint32_t pipe_write(int32_t fd,void * buf,uint32_t count);
bool is_pipe(uint32_t fd);
void sys_fd_redirect(uint32_t old_fd,uint32_t new_fd);

#endif 