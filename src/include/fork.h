#ifndef  __FORK_H__
#define __FORK_H__
#include <types.h>
pid_t sys_fork(void);

#endif 