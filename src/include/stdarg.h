#ifndef _STDARG_H_
#define _STDARG_H_
//返回类型 n 占用的字节数，并且返回值为 sizeof(int) 的整数倍。换句话说，设空间最小粒度为 sizeof(int)
#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1)) 

//存储参数的类型信息，32位和64位实现不一样
typedef char * va_list;

/*
参数：
ap: 可变参数列表地址 
v: 确定的参数
功能：初始化可变参数列表，会把v之后的参数放入ap中
*/
#define va_start(ap,v) ( ap = (va_list)&v + _INTSIZEOF(v) )
 

/*功能：返回下一个参数的值
将ap移动_INTSIZEOF(t)个字节后，然后再取出ap未移动前所指向位置对应的数据,注意，该宏的第二个参数为类型
*/
#define va_arg(ap,t) (*(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)))

//功能：将0强转为va_list类型,并赋值给ap,使其置空,完成清理工作
#define va_end(ap) ( ap = (va_list)0 )

#endif 
