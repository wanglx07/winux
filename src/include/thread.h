#ifndef _THREAD_H_
#define _THREAD_H_
#include <types.h>
#include <list.h>
#include <memory.h>

#define MAX_FILES_PER_PROC_OPEN 8 //一个进程最多可以打开多少个文件
#define MAX_TASK_NAME_LENGTH 32
enum task_status{
    TASK_RUNNING,
    TASK_READY,
    TASK_BLOCKED,
    TASK_WAITING,
    TASK_HANGING,
    TASK_DIED
};

//发生中断时需要保护上下文环境
struct interrupt_stack{
    uint32_t vector_num;
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t esp_manual;// 手动压入的
    uint32_t ebx; 
    uint32_t edx; 
    uint32_t ecx; 
    uint32_t eax; 
    uint32_t gs; 
    uint32_t fs; 
    uint32_t es; 
    uint32_t ds;
    uint32_t error_code;
    void (*eip) (void);
    uint32_t cs;
    uint32_t eflags;
    void * esp;//中断自动压入的
    uint32_t ss;
};

//系统调用用到的栈，参考handler.asm中的asm_syscall_handler
struct syscall_stack{
    uint32_t arg0;
    uint32_t arg1;
    uint32_t arg2;
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t esp_manual;// 手动压入的
    uint32_t ebx; 
    uint32_t edx; 
    uint32_t ecx; 
    uint32_t eax; 
    uint32_t gs; 
    uint32_t fs; 
    uint32_t es; 
    uint32_t ds;
    void (*eip) (void);
    uint32_t cs;
    uint32_t eflags;
    void * esp;//中断自动压入的
    uint32_t ss;
};

typedef void (* thread_func) (void * args) ;

//线程栈
struct thread_stack{
    uint32_t ebp;
    uint32_t ebx;
    uint32_t edi;
    uint32_t esi;

    void (*eip) (thread_func func,void * args);//是个函数指针

    void (* reserverd);

    thread_func func;
    void * args;
};

struct task_struct{
    uint32_t * sp; //线程栈的起始地址
    char name [MAX_TASK_NAME_LENGTH];
    pid_t pid; 
    uint32_t priority;
    enum task_status status;
    uint32_t ticks; //时间片
    uint32_t elapsed_ticks; //运行了多久
    struct list_head general_entry;//通过它加入一般链表
    struct list_head total_entry;//通过它加入总链表
    uint32_t *pdt_base_vir; //进程页目录表的虚拟地址，注意是虚拟地址 
    vir_pool vir_pool_user ; //用户进程虚拟内存池
    struct mem_block_type mem_type_table_u[TYPE_COUNT]; //用户进程内存块分配表
    int32_t fd_table[MAX_FILES_PER_PROC_OPEN];//文件描述符数组
    uint32_t cwd_i_num;//任务当前工作目录的inode编号，cwd=current work directory 当前工作目录
    uint32_t parent_pid;//父进程pid
    int8_t exit_status;//作为进程结束时exit的参数
    uint32_t magic ;//魔数，用来检测是否栈溢出，即压栈的数据是否破坏了task_struct的信息
};

extern struct list_head ready_list_head;//就绪队列
extern struct list_head total_list_head;//总队列

void thread_create(struct task_struct* task, thread_func func ,void * func_arg);
void thread_init(struct task_struct* task, char* name, int priority);
struct task_struct* thread_start(char * name,uint32_t priority,thread_func func,void * args);
void create_main_thread(void);
struct task_struct * get_current_thread(void);
void main_thread_init(void);
void schedule(void);
void thread_block(enum task_status status);
void thread_unblock(struct task_struct * task);
void thred_yield(void);
pid_t alloc_pid(void);
void sys_ps(void);

#endif 