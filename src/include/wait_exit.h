#ifndef  _WAIT_EXIT_H_
#define _WAIT_EXIT_H_
#include <types.h>

pid_t sys_wait(uint32_t * status);
void sys_exit(int32_t status);

#endif 