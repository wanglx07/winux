#ifndef _LIST_H_
#define _LIST_H_
#include <types.h>

#define offset_of(type,member) ((uint32_t)(&((type*)0)->member))

#define container_of(ptr,type,member) ({\
        typeof(((type *)0)->member) * mptr = (ptr) ;\
        (type *)((char*)mptr - offset_of(type,member));\
        })

#define list_for_each(pos, head) \
    for (pos = (head)->next; pos != (head); \
        pos = pos->next)
        
#define list_entry(ptr, type, member) \
	container_of(ptr, type, member)

#define list_first_entry(ptr, type, member) \
	list_entry((ptr)->next, type, member)

#define list_next_entry(pos, member) \
	list_entry((pos)->member.next, typeof(*(pos)), member)

//访问链表节点所在的宿主数据结构,pos是宿主的结构体变量指针，head是链表头指针，member是在宿主结构体中的成员名字，得到宿主结构保存在pos中
//for循环中第一行，pos相当于循环中返回的循环变量，这里就是返回一个结构体指针
//第二行，预取一下，这样可以提高速度，用于预取以提高遍历速度；
//第三行，用于逐项向后（next 方向）移动 pos
#define list_for_each_entry(pos,head,member) \
    for(pos = list_first_entry(head,typeof(*pos),member);\
        &pos->member!=(head); \
        pos = list_next_entry(pos, member))  
    
struct list_head {
    struct list_head * prev;
    struct list_head * next;
};

typedef int (*func_ptr)(struct list_head * ,void *);

#define LIST_HEAD_INIT(name) {&(name),&(name)}

//初始化头节点
#define LIST_HEAD(name) \
    struct list_head name=LIST_HEAD_INIT(name)

//如果已知是一个struct list_head变量，则使用如下函数初始化
void INIT_LIST_HEAD(struct list_head *list);


//在prev和next之间插入new
void list_insert(struct list_head *new,
                            struct list_head *prev,
                            struct list_head*next);


//添加元素到链表头部
void list_push(struct list_head* new ,struct list_head* head);

//添加元素到链表尾部
void list_append(struct list_head* new ,struct list_head* head);

//从链表中删除某个元素
void list_del(struct list_head * entry);


//弹出第一个节点
struct list_head * list_pop(struct list_head * head);


//在链表中查找是否存在某个节点,成功返回0，失败返回1
int list_exist(struct list_head *head,struct list_head *entry);


//为空则返回1，否则返回0
int list_empty(const struct list_head *head);


//把链表中的每个节点和 arg 传给回调函数 func,
//找到符合条件的返回节点指针，否则返回NULL
struct list_head * list_find(struct list_head *head, func_ptr func,void* arg );

//返回链表长度，不包括头节点
int list_len(const struct list_head *head);

#endif 