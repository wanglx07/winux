#ifndef  _DIR_H_
#define _DIR_H_
#include <file.h>
#include <disk.h>

//目录
struct dir{
    struct inode * inode;
    uint32_t dir_offset;//已读的目录项占的字节数 dir_read()的时候用到
    uint8_t dir_buf[512];//目录的数据缓存，如读取目录时，用来存储返回的目录项
};

//目录项
struct dir_entry{
    char filename[MAX_FILE_NAME_LENGTH];//普通文件或者目录名字  
    uint32_t i_num;//普通文件或者目录对应的inode编号
    enum file_type f_type;//文件类型
};

extern struct dir root_dir;  //根目录

void root_dir_open(struct partition * part);
struct dir * dir_open(struct partition * part,uint32_t i_num);
void dir_close(struct dir * d);
void dir_entry_init(char * filename,uint32_t i_num,enum file_type type,struct dir_entry * dir_e);
bool dir_entry_search(struct partition * part,struct dir * d,const char * name,struct dir_entry * dir_e);
bool dir_entry_sync(struct dir * parent_dir,struct dir_entry* dir_e ,void * io_buf);
bool dir_entry_delete(struct partition * part ,struct dir * dir ,uint32_t i_num ,void * io_buf);
struct dir_entry * dir_read(struct dir * dir);
int32_t dir_remove(struct dir * parent_dir,struct dir * child_dir);

#endif