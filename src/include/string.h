#ifndef _STRING_H_
#define _STRING_H_
#include <types.h>

int strlen (const char *s);
char * strcpy (char *dest, const char *src);
int strcmp (const char *csrc, const char *ct);
char * strchr (const char *str, char c);
char * strcat (char *dest, const char *src);

void * memset (void *dest, char c, size_t size);
void * memcpy (void *dest, const void *src, size_t size);
int memcmp (const void *csrc, const void *ct, size_t size);
void * memchr (const void *csrc, char c, size_t size);

#endif