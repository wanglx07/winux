#ifndef  _TSS_H_
#define _TSS_H_
#include <thread.h>

#define DATA_SEG_SELECTOR (2<<3)  //内核数据段选择子，定义在loader.asm里面
#define TSS_SELECTOR (4<<3) //TSS段选择子
#define USER_CODE_SEG_SELECTOR (5<<3 | 0b011) //用户进程代码段选择子
#define USER_DATA_SEG_SELECTOR (6<<3 | 0b011) //用户进程数据段选择子

void tss_init(void);
void refresh_tss(struct task_struct *next_task);

#endif 