#ifndef _MEMORY_H_
#define _MEMORY_H_
#include <types.h>
#include <bitmap.h>
#include <mutex.h>
#define PG_SIZE 4096
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

typedef struct vir_pool{
    uint32_t ptr; //起始地址
    bitmap bmap;  //位图 
    mutex lock;
}vir_pool;


typedef struct phy_pool{
    uint32_t ptr; //起始地址
    uint32_t size; //容量 单位字节
    bitmap bmap;  //位图 
    mutex lock; 
}phy_pool;

//内存块，通过block_entry连接到链表中，类似于task_struct中也有
struct mem_block{
    struct list_head block_entry; 
};

//内存块的类型，主要根据block_size分为16 32 64 128 256 512 1024 宇节这几种类型
struct mem_block_type{
    uint32_t block_size; //内存块的大小
    uint32_t blocks_per_arena;  //每一个arena应该拥有block_size大小的内存块的个数
    struct list_head free_list_head; //这种规格大小的内存块所在的链表 
};

#define TYPE_COUNT 7 //七种类型的内存块

//内存仓库
struct arena{
    struct mem_block_type * type; //arena里面的内存块属于哪种类型，为什么要有这个成员呢？后面将内存块回收的时候用到
    uint32_t block_count;//large为true时，表示物理页数，否则表示空闲的内存块的数量
    bool large;
};

enum phy_pool_flag{
    PHY_POOL_KERNEL,
    PHY_POOL_USER
};

void memory_init(void);
uint32_t get_mem_total_size();
void phy_pool_init(uint32_t mem_total_size);
void vir_pool_init();
void * vir_addr_get(enum phy_pool_flag pf,uint32_t pg_count);
void * phy_addr_get(phy_pool * pool);
uint32_t * pde_get(uint32_t vaddr);
uint32_t * pte_get(uint32_t vaddr);
void mapping(void * vir_addr , void * phy_addr);
void * malloc_page(enum phy_pool_flag pf,uint32_t pg_count);
void * apply_kernel_mem(uint32_t pg_count);
void * apply_a_page(enum phy_pool_flag pf,uint32_t vir_addr);
uint32_t to_phy_addr(uint32_t vir_addr);
void * kmalloc(uint32_t size);
void free_page(enum phy_pool_flag pf,void * vaddr,uint32_t pg_count);
void kfree(void * vaddr);
void * apply_a_page_no_bmap(enum phy_pool_flag pf,uint32_t vir_addr);

#endif 