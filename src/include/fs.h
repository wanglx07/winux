#ifndef  _FS_H_
#define _FS_H_
#include <types.h>
#include <list.h>

#define MAX_FILE_NAME_LENGTH 16
#define MAX_FILES_PER_PART 4096
#define BITS_PER_SECTOR (512*8)
#define SECTOR_SIZE 512


#define MAX_FILE_OPEN 32 //一个文件最多被同时打开多少次
#define MAX_PATH_LENGTH 512 //路径最大长度

enum open_flag;//前置声明

//超级块
struct super_block{
    uint32_t magic ;//文件系统类型
    uint32_t sector_count;//本分区的扇区数（本分区是指主分区或者逻辑分区）
    uint32_t inode_count;//本分区的inode数
    uint32_t start_sector;//本分区的起始扇区

    uint32_t free_block_bmap_start_sector;//空闲块位图绝对起始扇区
    uint32_t free_block_bmap_sector_count;//空闲块位图占用的扇区数

    uint32_t inode_bmap_start_sector;//i结点位图绝对起始扇区
    uint32_t inode_bmap_sector_count;//i结点位图占用的扇区数

    uint32_t inode_table_start_secotr;//i结点表绝对起始扇区
    uint32_t inode_table_sector_count;//i结点表占用的扇区数

    uint32_t data_start_sector;//空闲数据块绝对起始扇区号
    uint32_t root_inode_num;//根目录所在的i结点号，也就是inode 数组中我们留出第几个inode 给根目录
    uint32_t dir_entry_size;//目录项大小

    uint8_t left[460];//凑够一个扇区的大小

} __attribute__ ((packed));

//标准输入输出描述符
enum std_fd{
    STD_IN, //0,标准输入
    STD_OUT, //1,标准输出
    STD_ERROR //2,标准错误
};

enum whence { 
    SEEK_SET = 1, 
    SEEK_CUR, 
    SEEK_END 
};

enum file_type{
    FT_UNKNOWN,//不支持的文件类型
    FT_REGULAR,//普通文件
    FT_DIRECTORY//目录
};

//文件属性
struct stat{
    uint32_t i_num; //inode编号
    uint32_t f_size;//文件大小
    enum file_type f_type;//文件类型
};

extern struct partition * current_part;

void fs_init(void);
bool partition_mount(struct list_head * p,void * args );
int32_t sys_open(const char * pathname,enum open_flag flags);
int32_t sys_close(int32_t fd);
int32_t sys_write(int32_t fd,const void * buf, uint32_t count);
int32_t sys_read(int32_t fd , void * buf , uint32_t count);
int32_t sys_lseek(int32_t fd , int32_t offset ,enum whence w);
int32_t sys_unlink(const char * pathname);
int32_t sys_mkdir(const char * pathname);
struct dir * sys_opendir(const char * name);
int32_t sys_closedir(struct dir * pdir);
struct dir_entry * sys_readdir(struct dir * dir);
void sys_rewinddir(struct dir * dir);
int32_t sys_rmdir(const char * pathname);
char * sys_getcwd(char * buf,uint32_t size);
int32_t sys_chdir(const char * pathname);
int32_t sys_stat(const char * pathname ,struct stat * stat);
char * path_parse(char * pathname,char* name_buf);

#endif 