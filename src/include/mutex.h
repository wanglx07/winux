#ifndef _MUTEX_H_
#define _MUTEX_H_
#include <list.h>
#include <types.h>
// #include <thread.h>
#include <semaphore.h>

struct task_struct;//使用前置声明

//互斥体
typedef struct mutex{
    struct semaphore sema;
    struct task_struct * holder; //锁的拥有者
    uint32_t repeat; //重复申请的次数
}mutex;

void mutex_init(mutex *lock);

void mutex_lock( mutex *lock);

void mutex_unlock( mutex *lock);

#endif