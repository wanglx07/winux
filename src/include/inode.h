#ifndef _INODE_H_
#define _INODE_H_
#include <disk.h>

struct inode{
    uint32_t i_num; //inode编号
    uint32_t i_size;//inode表示文件时,i_size等于文件大小；inode表示目录时，i_size等于该目录所有目录项大小总和。单位字节
    
    uint32_t i_open_count;//被打开的次数
    bool write_deny;//不能同时写文件，写文件时检查这个标志

    uint32_t i_sectors[13];//0~11直接块，12一级间接块指针

    struct list_head inode_entry;//在该文件第1次被打开时就通过这个将其 inode 加入内存缓存中，即己打开的 inode 队列
};

void inode_init(uint32_t i_num,struct inode * new_node);
void inode_close(struct inode *node);
struct inode* inode_open(struct partition * part ,uint32_t i_num);
void inode_sync(struct partition * part ,struct inode * node,void * io_buf);
void inode_release(struct partition * part , uint32_t i_num);

#endif 