#include <disk.h>
#include <sprintf.h>
#include <interrupt.h>
#include <asm/io.h>
#include <time.h>
#include <assert.h>
#include <printk.h>
#include <string.h>
#include <list.h>

//16位数据端口
#define DATA_PORT 0x1f0

#define ERROR_PORT 0x1f1

//设置要读取的扇区数量，8位端口，因此每次只能读写255个扇区，如果为0表示读取256个扇区
#define SECTOR_COUNT_PORT 0x1f2

//起始LBA扇区号，因为是28位，故分四段分别写入8位的端口0x1f3~0x1f6
#define LBA_7_0_PORT  0x1f3
#define LBA_15_8_PORT 0x1f4
#define LBA_23_16_PORT 0x1f5
//31 29 位固定是1;30位0：CHS, 1:LBA ;28位0：主盘，1：从盘
#define LBA_31_24_PORT 0X1f6 

/* cmd:0x20 读硬盘 ，0x30写硬盘 ；
* status:第7位为1表示忙，硬盘准备就绪会自动将此位清0，同时将第3位置1，意思是准备好了，请求主机发送接收数据
*典型代码如下：
* mov dx,0x1f7
* .wait:
*  in al,dx
*  and al,0x88
*  cmp al,0x08
*  jnz .wait ;不忙，且硬盘已准备好数据传输
*/
#define CMD_STATUS_PORT 0x1f7 

#define CMD_READ 0x20 
#define CMD_WRITE 0X30
#define CMD_IDENTIFY 0xec	

struct ata_channel channel_ata0; //通道ata0 

uint32_t  total_ext_start_sector; //总扩展分区起始扇区号

struct list_head partition_list;	 // 分区队列

uint32_t prim_idx , logic_idx ;	 // 用来记录硬盘主分区和逻辑分区的下标


//等待硬盘 30 秒。硬盘是个低速设备，因此在其响应
//过程中，驱动程序可以让出 CPU 使用权使其他任务得到调度
bool ready()
{
    uint32_t wait_ms=30*1000;//最多等待30秒
    uint8_t status;
    uint32_t i=0;
    
    while(i<wait_ms)
    {
        status=inb(CMD_STATUS_PORT);
        if (status&0x08)//通过打印可以知道status为0x58，所以这里不能写status&0x88==0x08
        {
            return true;
        }else{
            sleep_ms(10);
        }
        i+=10;
    }
    return false;

}

//从硬盘hd的起始扇区start_sector处开始读取sector_count个扇区到 buf
void disk_read(struct disk* hd,uint32_t start_sector,void * buf, uint32_t sector_count)
{
    mutex_lock(&hd->to_which_channel->lock);
    uint32_t each=0;//每次操作的扇区数
    uint32_t done=0;//已完成的扇区数

    while (done<sector_count)
    {  
        //1、读取的扇区数
        if (done+256<sector_count)//如果剩余的扇区数大于256
        {
            each=0; //为0表示读取256个扇区
        }else{
            each=sector_count-done;
        }
        outb(SECTOR_COUNT_PORT,each);

        //2、设置LBA起始扇区号和操作的硬盘
        outb(LBA_7_0_PORT,start_sector); 
        outb(LBA_15_8_PORT,start_sector>>8); 
        outb(LBA_23_16_PORT,start_sector>>16);
        outb(LBA_31_24_PORT,(start_sector>>24)|(hd->diff==MASTER ? 0xe0 /*主盘*/: 0xf0/*从盘*/));
        
        //3、读命令
        channel_ata0.want_intr=true;//这个一定要放在命令的前面
        outb(CMD_STATUS_PORT,CMD_READ);
        down(&channel_ata0.disk_sema);

        //4、检测硬盘状态是否已经准备好
        if(!ready())
        {
            char error_info[64];
            sprintf(error_info,"error : %s read sector %d failed \r\n ",hd->name,start_sector);
            panic(error_info);
        }

        //5、开始读取数据到缓冲区
        uint32_t bytes ; //要读取的字节数
        if (each==0)
        {
            bytes=256*512;
        }else{
            bytes=each*512;
        }
        insw(DATA_PORT,buf,bytes/2);
        done+=each;
    }
    mutex_unlock(&hd->to_which_channel->lock);
}

//将buf写入到硬盘hd的起始扇区start_sector处
void disk_write(struct disk* hd,uint32_t start_sector,void * buf, uint32_t sector_count)
{
    mutex_lock(&hd->to_which_channel->lock);
    uint32_t each=0;//每次操作的扇区数
    uint32_t done=0;//已完成的扇区数
    
    while (done<sector_count)
    {  
        //1、写入的扇区数
        if (done+256<sector_count)//如果剩余的扇区数大于256
        {
            each=256; //写入256个扇区,注意这里不能写0
        }else{
            each=sector_count-done;
        }

        outb(SECTOR_COUNT_PORT,each);
        
        //2、设置LBA起始扇区号和操作的硬盘
        outb(LBA_7_0_PORT,start_sector); 
        outb(LBA_15_8_PORT,start_sector>>8); 
        outb(LBA_23_16_PORT,start_sector>>16);
        outb(LBA_31_24_PORT,(start_sector>>24)|(hd->diff==MASTER ? 0xe0 /*主盘*/: 0xf0/*从盘*/));

        //3、写命令
        channel_ata0.want_intr=true;//这个一定要放在命令的前面
        outb(CMD_STATUS_PORT,CMD_WRITE);

        //4、检测硬盘状态是否已经准备好
        if(!ready())
        {
            char error_info[64];
            sprintf(error_info,"error : %s write sector %d failed \r\n ",hd->name,start_sector);
            panic(error_info);
        }

        //5、开始将缓冲区的数据写入磁盘
        uint32_t bytes ; //要写入的字节数
        if (each==0)
        {
            bytes=256*512;
        }else{
            bytes=each*512;
        }
        outsw(DATA_PORT,buf,bytes/2);
        down(&channel_ata0.disk_sema);//这句不能放在outb(CMD_STATUS_PORT,CMD_WRITE);后面
        done+=each;
    }

    mutex_unlock(&hd->to_which_channel->lock);
}

// 将dst中len个相邻字节交换位置后存入buf
//硬盘参数信息是以字为单位的，包括偏移、长度的单位都是字，在这 16 位的字中，相邻字符的位置是互
//换的，所以通过此函数做转换。
void swap(const char* dst, char* buf, uint32_t len) {
   int i;
   for (i = 0; i < len; i += 2) {
      // buf中存储dst中两相邻元素交换位置后的字符串
      buf[i + 1] = *dst++;   
      buf[i]     = *dst++;   
   }
   buf[i] = '\0';
}

//获取磁盘信息
void disk_identify(struct disk* hd)
{
    char disk_infos[512];//存储向硬盘发送 identify 命令后返回的硬盘参数

    outb(LBA_31_24_PORT, hd->diff==MASTER ? 0xe0 : 0xf0 );//选择主盘还是从盘

    channel_ata0.want_intr=true;//一定要放在命令前面
    outb(CMD_STATUS_PORT,CMD_IDENTIFY);//发送命令
    down(&channel_ata0.disk_sema);

    //检测硬盘状态是否已经准备好
    if(!ready())
    {
        char error_info[64];
        sprintf("error : %s identify failed \r\n ",hd->name);
        panic(error_info);
    }

    insw(DATA_PORT,disk_infos,512/2);//直接从端口读进来，没有通过disk_read

    char buf[64];
    uint8_t serial_start = 10 * 2;//序列号起始字节地址
    uint8_t serial_len = 20;//序列号长度
    uint8_t model_start = 27 * 2;//型号起始字节地址
    uint8_t model_len = 40;//型号长度

    //序列号
    swap(&disk_infos[serial_start], buf, serial_len);
    printk("   disk %s info:\r\n      SN: %s\r\n", hd->name, buf);

    //型号
    memset(buf, 0, sizeof(buf));
    swap(&disk_infos[model_start], buf, model_len);
    printk("      MODULE: %s\r\n", buf);

    //可供用户使用的扇区数，长度为2的整型
    uint32_t sectors = *(uint32_t*)&disk_infos[60 * 2];
    printk("      SECTORS: %d\r\n", sectors);
    printk("      CAPACITY: %dMB\r\n", sectors * 512 / 1024 / 1024);

}

//扫描分区表,ext_start_sector 扩展分区绝对起始扇区（也就是相对第1个扇区的距离）
void partition_scan(struct disk* hd, uint32_t ext_start_sector) {
    struct boot_sector* bs = malloc_sys(sizeof(struct boot_sector));
    disk_read(hd, ext_start_sector, bs, 1);//得到MBR或者EBR
    struct table_item* item = bs->partition_table;//取出分区表
    uint8_t i = 0;
   // 遍历分区表4个分区表项
    while (i++ < 4) {
        if (item->fs_type == 0x5) {	 // 分区类型若为扩展分区
            if (total_ext_start_sector != 0) { 
                //子扩展分区的绝对起始扇区=主引导扇区中的总扩展分区起始扇区号+偏移扇区号 
                partition_scan(hd,  total_ext_start_sector+item->offset_sector );
            } else { 
                //注意这里要写offset_sector，即是总扩展分区或者逻辑分区的偏移扇区，不是start_sector,start_sector是MBR、EBR的绝对起始扇区号
                total_ext_start_sector = item->offset_sector;
                partition_scan(hd, item->offset_sector);
            }

        } else if (item->fs_type != 0) { // 若是有效的分区类型，像MBR分区表中一主一扩展区的时候，有些项是空的
            if (ext_start_sector == 0) {	 // 此时是主分区
                hd->prim_parts[prim_idx].start_sector = item->offset_sector;
                hd->prim_parts[prim_idx].sector_count = item->sector_count;
                hd->prim_parts[prim_idx].to_which_disk = hd;
                sprintf(hd->prim_parts[prim_idx].name, "%s%d", hd->name, prim_idx + 1);
                list_append(&hd->prim_parts[prim_idx].entry,&partition_list );
                prim_idx++;
            } else {
                hd->logic_parts[logic_idx].start_sector = ext_start_sector + item->offset_sector;
                hd->logic_parts[logic_idx].sector_count = item->sector_count;
                hd->logic_parts[logic_idx].to_which_disk = hd;
                sprintf(hd->logic_parts[logic_idx].name, "%s%d", hd->name, logic_idx + 5);	 // 逻辑分区数字是从5开始,主分区是1～4.
                list_append(&hd->logic_parts[logic_idx].entry, &partition_list);
                logic_idx++;
                if (logic_idx >= 8)    // 只支持8个逻辑分区,避免数组越界
                return;
            }
      }
      item++;
   }
   free_sys(bs);
}


// 打印分区信息 
bool partition_info(struct list_head* entry,int arg ) {
   struct partition* part = container_of(entry,struct partition,entry);
   printk("   %s start_lba:0x%x, sec_cnt:0x%x \r\n",part->name, part->start_sector, part->sector_count);

//在此处return false与函数本身功能无关,只是为了让主调函数list_traversal继续向下遍历元素
   return false;
}

void disk_handler()
{
   if(channel_ata0.want_intr==true )
   {
      channel_ata0.want_intr=false;
      up(&channel_ata0.disk_sema);
   }
   inb(CMD_STATUS_PORT);
}

void disk_init()
{
    sprintf(channel_ata0.name,"ata%d",0);
    channel_ata0.start_port=0x1f0;
    mutex_init(&channel_ata0.lock);
    INIT_LIST_HEAD(&partition_list);
   sema_init(&channel_ata0.disk_sema,0);
   channel_ata0.want_intr=false;
   register_handler(0x2e,disk_handler);

    //为了代码的简便，我们这里的代码只获取ata0通道的主盘和从盘的信息，并且只扫描从盘的分区情况，没涉及到其它通道其它盘
    //主盘
    struct disk* hd_master = &channel_ata0.disks[0];
    hd_master->to_which_channel = &channel_ata0;
    hd_master->diff = MASTER;
    sprintf(hd_master->name, "sd%c", 'a');
    disk_identify(hd_master);	 // 获取硬盘参数

    //从盘
    struct disk* hd_slave = &channel_ata0.disks[1];
    hd_slave->to_which_channel = &channel_ata0;
    hd_slave->diff = SLAVE;
    sprintf(hd_slave->name, "sd%c", 'b');
    disk_identify(hd_slave);	 // 获取硬盘参数
    prim_idx=0;//全局变量要在这里初始化，不懂为什么在定义的时候初始化为0不行
    logic_idx=0;
    total_ext_start_sector=0;
	partition_scan(hd_slave, 0);  // 扫描硬盘的分区
    // 打印从盘所有分区信息
    printk("\r\n  the ata0-slave partition info\r\n");
    list_find(&partition_list, partition_info, (int)NULL);
}
