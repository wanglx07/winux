#include <keyboard.h>
#include <asm/io.h>
#include <printk.h>
#include <types.h>
#include <kfifo.h>

#define KEYBOARD_DATA 0X60  //输入输出缓冲区的端口

static bool shift_status; //shift键是否被按下
static bool ctrl_status;
static bool capslock_status;
static bool extend_status; //表示通码是否以e0开头
static bool alt_status;

//定义控制键的通码
#define shift_l_make	0x2a
#define shift_r_make 	0x36 
#define alt_l_make   	0x38
#define alt_r_make   	0xe038
#define ctrl_l_make  	0x1d
#define ctrl_r_make  	0xe01d
#define caps_lock_make 	0x3a

kfifo fifo; 

char keymap[][2] = {
/* 扫描码   没按下shift键的选择  按下shift键的选择*/
/* ---------------------------------- */
/* 0x00 */	{0,	0},		
/* 0x01 */	{'\x1b','\x1b'}, //esc键		
/* 0x02 */	{'1',	'!'},		
/* 0x03 */	{'2',	'@'},		
/* 0x04 */	{'3',	'#'},		
/* 0x05 */	{'4',	'$'},		
/* 0x06 */	{'5',	'%'},		
/* 0x07 */	{'6',	'^'},		
/* 0x08 */	{'7',	'&'},		
/* 0x09 */	{'8',	'*'},		
/* 0x0A */	{'9',	'('},		
/* 0x0B */	{'0',	')'},		
/* 0x0C */	{'-',	'_'},		
/* 0x0D */	{'=',	'+'},		
/* 0x0E */	{'\b', '\b'},   //backspace键
/* 0x0F */	{'\t',	'\t'},  //tab键		
/* 0x10 */	{'q',	'Q'},		
/* 0x11 */	{'w',	'W'},		
/* 0x12 */	{'e',	'E'},		
/* 0x13 */	{'r',	'R'},		
/* 0x14 */	{'t',	'T'},		
/* 0x15 */	{'y',	'Y'},		
/* 0x16 */	{'u',	'U'},		
/* 0x17 */	{'i',	'I'},		
/* 0x18 */	{'o',	'O'},		
/* 0x19 */	{'p',	'P'},		
/* 0x1A */	{'[',	'{'},		
/* 0x1B */	{']',	'}'},		
/* 0x1C */	{'\r',  '\r'}, //回车键
/* 0x1D */	{0,       0},  //左边ctrl键，没有具体的ASCII码，用0代替
/* 0x1E */	{'a',	'A'},		
/* 0x1F */	{'s',	'S'},		
/* 0x20 */	{'d',	'D'},		
/* 0x21 */	{'f',	'F'},		
/* 0x22 */	{'g',	'G'},		
/* 0x23 */	{'h',	'H'},		
/* 0x24 */	{'j',	'J'},		
/* 0x25 */	{'k',	'K'},		
/* 0x26 */	{'l',	'L'},		
/* 0x27 */	{';',	':'},		
/* 0x28 */	{'\'',	'"'},		
/* 0x29 */	{'`',	'~'},		
/* 0x2A */	{0,       0}, //左边shift键	
/* 0x2B */	{'\\',	'|'},		
/* 0x2C */	{'z',	'Z'},		
/* 0x2D */	{'x',	'X'},		
/* 0x2E */	{'c',	'C'},		
/* 0x2F */	{'v',	'V'},		
/* 0x30 */	{'b',	'B'},		
/* 0x31 */	{'n',	'N'},		
/* 0x32 */	{'m',	'M'},		
/* 0x33 */	{',',	'<'},		
/* 0x34 */	{'.',	'>'},		
/* 0x35 */	{'/',	'?'},
/* 0x36	*/	{0,       0}, //右边shift键	
/* 0x37 */	{'*',	'*'},    	
/* 0x38 */	{0,       0}, //左边alt键
/* 0x39 */	{' ',	' '},		
/* 0x3A */	{0,       0}  //大写键capslock
};

void keyboard_handler()
{
    uint32_t scancode=inb(KEYBOARD_DATA);

    //若扫描码是e0开头的,表示此键的按下将产生多个扫描码,所以马上结束此次中断处理函数,等待下一个扫描码进来
    if (scancode == 0xe0) 
    {
        extend_status=true;
        return;
    }

    // 如果上次是以0xe0开头,将扫描码合并
   if (extend_status) 
   {
      scancode = ((0xe000) | scancode);
      extend_status = false;   
   } 

    //判断是通码还是断码
    if(scancode & 0x0080)
    {   
        //断码
        uint32_t make_code= scancode & ~(0x0080);//得到断码对应的通码

        if (make_code == shift_l_make || make_code ==shift_r_make)
        {
            shift_status=false;
        }else if(make_code ==alt_r_make || make_code== alt_l_make)
        {
            alt_status=false;
        }else if(make_code ==ctrl_l_make || make_code ==ctrl_r_make)
        {
            ctrl_status=false;
        }

        return;
        
        //在我们处理范围内的通码,像alt_r_make不在我们上面定义的数组中,所以后面要用 ||
    }else if((scancode>=0x01 && scancode<=0x3a) || scancode == shift_r_make ||
            scancode == alt_r_make || scancode == ctrl_r_make  ){
            bool display_up =false;//true则显示上面的字符(或者大写)，false则显示下面的字符(或者小写)
            
                //那些有两个字符且不是字母键的键
            if ((scancode < 0x0e) || (scancode == 0x27) || (scancode == 0x28) ||(scancode == 0x29) ||
             (scancode == 0x1a) || (scancode == 0x1b) || (scancode == 0x2b) ||  (scancode == 0x33) || 
            (scancode == 0x34) || (scancode == 0x35)) { 
                if (shift_status)
                {
                    display_up=true;
                }

                //字母键或者空格、回车、退格
            }else if((scancode>=0x0e && scancode <=0x19)|| scancode==0x1c ||(scancode>=0x1e && scancode <=0x26)||
            (scancode>=0x2c&&scancode<=0x32) || scancode == 0x39){
                
                if(shift_status && capslock_status) //如果都按下了
                {
                    display_up = false;//小写
                }else if(shift_status || capslock_status){//任意一个被按下
                    display_up = true;
                }
                // printk("haha up=%d",display_up);
                //控制键，如ctrl,shift...
            }else{
                if (scancode == ctrl_l_make || scancode == ctrl_r_make) {
                    ctrl_status = true;
                } else if (scancode == shift_l_make || scancode == shift_r_make) {
                    shift_status = true;
                } else if (scancode == alt_l_make || scancode == alt_r_make) {
                    alt_status = true;
                } else if (scancode == caps_lock_make) {
                    /* 不管之前是否有按下caps_lock键,当再次按下时则状态取反,
                    * 即:已经开启时,再按下同样的键是关闭。关闭时按下表示开启。*/
	                capslock_status = !capslock_status;
                }
            }
     
            uint32_t key_index = scancode & 0x00ff;//得到具体的通码
            // printk("up= %d \r\n",display_up);
            char c= keymap[key_index][display_up];//得到具体的字符

            if (c!=0)//如果不是控制键
            {
                kfifo_putchar(&fifo,c);
                // if (c=='\r')
                // {
                //     printk("%c\n",c);//不加这个的话只会回到开头，不会换行
                // }
                // else{ 
                //     printk("%c",c);//要屏蔽掉这个，不然shell的实现那里，按一次键打印两次
                // }
                return;
            }
        }
        
}


void keyboard_init()
{
    shift_status=false; //一定要初始化不然可能出现异常
    ctrl_status=false;
    capslock_status=false;
    extend_status=false; 
    alt_status=false;
    
    kfifo_init(&fifo);

    register_handler(0x21,keyboard_handler);
    
}