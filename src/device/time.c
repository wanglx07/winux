#include <time.h>
#include <asm/io.h>
#include <types.h>
#include <thread.h>
#include <interrupt.h>
#include <assert.h>

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

uint32_t ticks;

void timer_8253_init()
{   
    //往控制字寄存器端口 0x43 中写入控制字
    outb(TIMER_CTR_PORT,0b00110100);//定时器0；先读写低字节，后读写高字节；方式2；二进制
    //在所指定使用的计数器端口中写入计数初值
    outb(TIMER0,(u32)(1193180/TIMER0_FREQ));
    outb(TIMER0,(u32)(1193180/TIMER0_FREQ) >>8);
    ticks=0;
}

void time_init()
{
    timer_8253_init();
    register_handler(0x20,timer_intr_handler);
}

//时钟中断处理函数
void timer_intr_handler(void)
{
    struct task_struct * current_thread =get_current_thread();
    assert(current_thread->magic == 0x20220727);

    current_thread->elapsed_ticks++;
    ticks++;

    if (current_thread->ticks==0)
    { 
        schedule();
    }else{
        current_thread->ticks--;
    }

}

void sleep(uint32_t sleep_ticks)
{
    uint32_t start_tick=ticks;
    //如果第一次让了之后，再从其它线程切回来的时候如果还是小于sleep_ticks继续让，如果大于则执行自己的，
    //yield是append到就绪队列后面的，而且其它线程的时间片也大小不一，所以延时时间都会大于或等于sleep_ticks
    while(ticks - start_tick< sleep_ticks)
    {
        thred_yield();
    }
}

//延时函数
void sleep_ms(uint32_t time)
{
    //TIMER0_FREQ=100,说明一秒钟中断100次，也就是每10ms中断一次
    uint32_t sleep_ticks=DIV_ROUND_UP(time,10);//那么time时间中中断 time/10 次,这么多次中断中，我们都让出cpu
    sleep(sleep_ticks);
}
