#include <kfifo.h>
#include <assert.h>

void kfifo_init(kfifo* fifo)
{
    mutex_init(&fifo->lock);
    fifo->in = 0;
    fifo->out = 0;
    fifo->producer=NULL;
    fifo->consumer= NULL;
}

//队列是否为空
bool kfifo_empty(kfifo* fifo)
{
    return fifo->in==fifo->out;
}

//队列是否已满
bool kfifo_full(kfifo* fifo)
{
    return (fifo->in+1)%BUF_SIZE==fifo->out; 
}

//队列中元素的个数，最长为 (BUF_SIZE -1 )
uint32_t kfifo_len(kfifo* fifo)
{
    return (fifo->in-fifo->out + BUF_SIZE) % BUF_SIZE;
}

void kfifo_putchar(kfifo* fifo, char c)
{
    while(kfifo_full(fifo)) //缓冲区满，不能继续往里放
    {
        mutex_lock(&fifo->lock);
        //消费者标记为当前线程，以便生产者放数据进缓冲区了知道唤醒谁
        fifo->producer=get_current_thread();
        thread_block(TASK_BLOCKED);
        mutex_unlock(&fifo->lock);
    }

    //放数据
    fifo->buff[fifo->in]=c;
    fifo->in= (fifo->in+1) % BUF_SIZE;

    if (fifo->consumer != NULL)
    {
        thread_unblock(fifo->consumer);
        fifo->consumer=NULL;        
    }
}

char kfifo_getchar(kfifo* fifo)
{
    while(kfifo_empty(fifo))
    {
        mutex_lock(&fifo->lock);
        //消费者标记为当前线程，以便生产者放数据进缓冲区了知道唤醒谁
        fifo->consumer=get_current_thread();
        thread_block(TASK_BLOCKED);
        mutex_unlock(&fifo->lock);
    }

    //拿数据
    char c;
    c = fifo->buff[fifo->out];
    fifo->out=(fifo->out+1) %BUF_SIZE;

    //若之前此缓冲区是满的，正好有生产者来添加数据，那个生产者一定会在此缓冲区上阻塞
    //（也就是在kfifo中producer上记录着），现在有空间了要唤醒生产者
    if (fifo->producer!= NULL)
    {
        thread_unblock(fifo->producer);
        fifo->producer=NULL;
    }
    return c;
}

